<?php

use App\Ukm;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// front

Route::name('adminUi.')->group(function () {
    Route::get('AdminFront', 'AdminUiController@Index');
});
Route::name('HomeUi.')->group(function () {
    Route::get('HomePagesUser', 'HomeUiController@Index');
    Route::get('LandingPages', 'HomeUiController@LandingPages');
    Route::get('Bantuan', 'HomeUiController@Bantuan');
    Route::get('ProfileMerchantsUser', 'HomeUiController@ProfileMerchantsUser');
});
Route::name('MerchantsUi.')->group(function () {
    Route::get('ProfileMerchants', 'MerchantsUiController@Index');
    Route::get('Regist', 'MerchantsUiController@Regist');
    Route::get('AlertRegist', 'MerchantsUiController@AlertRegist');

});
Route::name('Kategori.')->group(function () {
    Route::get('Makanan', 'KategoriController@Makanan');
    Route::get('Produsen', 'KategoriController@Produsen');
    Route::get('Store', 'KategoriController@Store');
    Route::get('Wisata', 'KategoriController@Wisata');
    Route::get('Barang', 'KategoriController@Barang');
    Route::get('Jasa', 'KategoriController@Jasa');
    Route::get('Properti', 'KategoriController@Properti');
    Route::get('Hasilbumi', 'KategoriController@Hasilbumi');


});


//  /front


Route::get('/api-test', 'ZController@index');

Auth::routes();

// Route::get('/test',function () {
//     return view('ukm.create-second-part');
// });

Route::get('/', 'GuestController@index');

Route::post('/reset-password-ukm', 'GuestController@resetPassword');


Route::group(['middleware' => ['auth', 'checkRole:admin']], function () {

    Route::get('/super-admin/reset-password-ukm', 'PrivasiController@index');

    Route::post('/super-admin/reset-password', 'PrivasiController@resetPassword');

    Route::get('/super-admin', 'UkmController@index');

    Route::resource('/super-admin/users', 'admin\UsersController');

    Route::get('/super-admin/detail-ukm/{id}', 'UkmController@show');

    Route::post('/super-admin/delete-ukm', 'UkmController@destroy');

    Route::get('/super-admin/buat-ukm', function () {
        return view('admin.buat-ukm');
    });

    Route::post('/registrasi-ukm', 'UkmController@store');

    Route::post('/update-ukm', 'UkmController@update');


});

Route::group(['middleware' => ['auth', 'checkRole:ukm']], function () {

    Route::get('/ukm', 'HomeController@indexUKM');

    Route::post('/ukm/rubah-tersedia/{id}', 'ProdukController@ubahTersedia');

    Route::post('/ukm/rubah-habis/{id}', 'ProdukController@ubahHabis');

    Route::get('/ukm/kasir', 'UkmController@kasir');

    Route::post('/ukm/kasir/keranjang', 'PenjualanController@store');

    Route::post('/ukm/kasir/hapus-data/{id}', 'PenjualanController@destroy');

    Route::post('/ukm/kasir/beli', 'PenjualanController@create');

    Route::get('/ukm/kasir/nota', 'UkmController@nota');

    Route::get('ukm/kasir/nota/detail/{id}', 'PenjualanController@show');

    Route::get('ukm/kasir/nota/detail/{id}/cetak', 'PenjualanController@createPDF');

    Route::get('/ukm/product', 'ProdukController@index');

    Route::get('/ukm/product-create', 'ProdukController@create');

    Route::post('/ukm/product/create', 'ProdukController@store');

    Route::get('/ukm/product/{id}', 'ProdukController@show');

    Route::get('/ukm/edit-product/{id}', 'ProdukController@edit');

    Route::post('/ukm/edit-product/update', 'ProdukController@update');

    Route::post('/ukm/product/destroy', 'ProdukController@destroy');

    Route::post('/ukm/product/update', 'ProdukController@update');

    Route::get('/ukm/edit-product/hapus-foto/{id}', 'ProdukController@destroyFoto');

    Route::get('/ukm/product-create/{id}/select-profile/', 'FotoProdukController@showFotoById');

    Route::post('/ukm/product-create/select-profile-mine/', 'FotoProdukController@setThumbnail');

    Route::post('/ukm/product-create/unselect-profile-mine/', 'FotoProdukController@unsetThumbnail');

    Route::post('/ukm/product-create/update-profile-mine/', 'FotoProdukController@updateThumbnail');

    Route::get('/ukm/profile/', 'UkmController@pengaturan');

    Route::get('/ukm/profile/edit', 'UkmController@edit');

    Route::post('/ukm/profile/edit/update-data', 'UkmController@updateData');

    Route::get('/ukm/profile/edit-nomor', 'PrivasiController@editNomor');

    Route::post('/ukm/profile/edit/permission-nomor', 'PrivasiController@permissionNomor');

    Route::post('/ukm/profile/edit/update-nohp', 'PrivasiController@updateNohp');

    Route::get('/ukm/profile/edit-password', 'PrivasiController@editPassword');

    Route::post('/ukm/profile/edit/update-password', 'PrivasiController@updatePassword');

    Route::post('/ukm/profile/edit/permission-password', 'PrivasiController@permissionPassword');

    Route::get('/json-harga/', 'PenjualanController@cariHarga');

});

Route::group(['middleware' => ['auth', 'checkRole:admin,ukm']], function () {

    Route::get('/home', 'HomeController@index');
    Route::get('/home', 'HomeController@index');

});

Route::get('/{slug}', 'GuestController@show');

Route::get('/{slug}/{slug_produk}', 'GuestController@product');




// Route::get('/home', 'HomeController@index')->name('home');
