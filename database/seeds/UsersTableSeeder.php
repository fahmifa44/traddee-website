<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'role' => 1,
            'name' => 'admin',
            'username' => '081359651571',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('adminadmin'),
        ]);
        User::create([
            'role' => 2,
            'name' => 'suherman',
            'username' => '1234567890',
            'email' => 'ukm@gmail.com',
            'password' => bcrypt('userukm'),
        ]);
    }
}
