<?php

use Illuminate\Database\Seeder;
use App\Ukm;

class UkmTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Ukm::create([
            'user_id'=>2,
            'nama_pemilik' => 'jamu gendong',
            'slug' => 'jamu_gendong',
            'alamat' => 'jl tikus got',
            'iframe' => 'ccc',
            'deskripsi' => 'Belilah odading mang oleh kadiek',
            'foto_profil' => 'dblajsh',
        ]);
    }
}
