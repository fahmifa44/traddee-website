<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdukTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produk', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('ukm_id');
            $table->string('nama_produk');
            $table->string('slug_produk');
            $table->string('deskripsi');
            $table->integer('category_id')->nullable();
            $table->string('berat');
            $table->string('rasa');
            $table->integer('harga');
            $table->boolean('status');
            $table->boolean('tersedia');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produk');
    }
}
