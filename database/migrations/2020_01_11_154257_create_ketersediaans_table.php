<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKetersediaansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ketersediaans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kode_penjualan');
            $table->unsignedBigInteger('produk_id');
            $table->unsignedBigInteger('ukm_id');
            $table->integer('banyak');
            $table->integer('total');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ketersediaans');
    }
}
