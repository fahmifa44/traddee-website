@extends('layouts.guestNavbar')

@section('content')
<main>
    <!-- breadcrumb area start -->
    <div class="breadcrumb-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb-wrap">
                        <nav aria-label="breadcrumb">
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url('/')}}"><i class="fa fa-home"></i></a></li>
                                <li class="breadcrumb-item active" aria-current="page">UKM {{$profil->user->name}}</li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumb area end -->

    <!-- about us area start -->
    <section class="about-us section-padding">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-5">
                    <div class="about-thumb">
                      @if($profil->foto_profil != NULL)
                        <img src="foto_profil/{{$profil->foto_profil}}" alt="about thumb" style="object-fit: cover;height: 420px;">
                      @else
                        <img src="assets/img/logo/logo.png" alt="about thumb">
                      @endif
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="about-content">
                        <h2 class="about-title"><strong style="color:black">{{$profil->user->name}}</strong></h2>
                        <h5 class="about-sub-title">
                            Tentang UKM Kami :
                        </h5>
                        <p>{{$profil->deskripsi}}</p>
                        <p>Pemilik : {{$profil->nama_pemilik}}</p>
                        <p>Alamat : {{$profil->alamat}}</p>
                        <p>No. Wa : {{$profil->user->username}}</p>

                          <a href="https://api.whatsapp.com/send?phone={{$profil->user->username}}&text=Hallo UKM {{$profil->user->name}} saya melihat UKM anda pada Website Dolly Bisa dan ingin menanyakan : "><button class="btn btn-hero" style="background-color:#5cb85c" type="button" name="button"><i class="fa fa-whatsapp"></i> Hubungi Kami</button></a>
                          <a href="#peta"><button class="btn btn-hero" style="background-color:#428bca" type="button" name="button"><i class="fa fa-map-marker" aria-hidden="true"></i> Temukan Kami</button></a>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- about us area end -->
    <div class="container" style="padding-top: 100px; margin-top: -100px;" id="peta">
      <div class="row">
          <div class="col-12">
              <!-- section title start -->
              <div class="section-title text-center">
                  <h2 class="title">Lokasi UKM Kami</h2>
                  <!-- <p class="sub-title">Sector Makanan Kering dan Basah</p> -->
              </div>
              <!-- section title start -->
          </div>
      </div>
    </div>
    <div class="container">
      <div class="align-item-center">
        <div class="col-12">
          {!! $profil->iframe !!}
          <style>
              iframe{
                width: 100%;
              }
          </style>
        </div>

      </div>
    </div>

    <!-- product area start -->
        <section class="product-area section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <!-- section title start -->
                        <div class="section-title text-center">
                            <h2 class="title">Produk UKM Kami</h2>
                            <!-- <p class="sub-title">Sector Makanan Kering dan Basah</p> -->
                        </div>
                        <!-- section title start -->
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="product-container">

                            <!-- product tab content start -->
                            <div class="tab-content">
                                <div class="tab-pane fade show active" id="tab1">
                                    <!-- <div class="product-carousel-4 slick-row-10 slick-arrow-style"> -->
                                    <div class="row">

                                      @if($profil->produk->where('status', 1)->count() != 0)
                                      @foreach($profil->produk->where('status', 1)->sortByDesc('created_at') as $produks)
                                        <div class="col-md-3" style="margin-bottom:40px">
                                        <!-- product item start -->
                                        <div class="product-item">
                                            <figure class="product-thumb" style="width: 100%; height: 0; padding-bottom: 100%; position: relative;">
                                                <a href="{{url('/')}}/{{$produks->ukm->slug}}/{{$produks->slug_produk}}">
                                                  @if($produks->foto_produk->count() == 1)
                                                    @php $i=0 @endphp
                                                    @foreach($produks->foto_produk->take(1) as $zzz)
                                                      @php $i++ @endphp
                                                      <img class="pri-img" src="{{asset('/foto_produk/'.$zzz->file)}}" alt="product" style="max-height: 100%;max-width: 100%; width: auto; height: auto; position: absolute; top: 0; bottom: 0; left: 0;right: 0; margin: auto;">
                                                      <img class="sec-img" src="{{asset('/foto_produk/'.$zzz->file)}}" alt="product" style="max-height: 100%;max-width: 100%; width: auto; height: auto; position: absolute; top: 0; bottom: 0; left: 0;right: 0; margin: auto;">
                                                    @endforeach
                                                  @else
                                                    @php $i=0 @endphp
                                                    @foreach($produks->foto_produk->take(2) as $zzz)
                                                    @php $i++ @endphp
                                                    @if($i==1)
                                                    <img class="pri-img" src="{{asset('/foto_produk/'.$zzz->file)}}" alt="product" style="max-height: 100%;max-width: 100%; width: auto; height: auto; position: absolute; top: 0; bottom: 0; left: 0;right: 0; margin: auto;">
                                                    @endif
                                                    @if($i!=1)
                                                    <img class="sec-img" src="{{asset('/foto_produk/'.$zzz->file)}}" alt="product" style="max-height: 100%;max-width: 100%; width: auto; height: auto; position: absolute; top: 0; bottom: 0; left: 0;right: 0; margin: auto;">
                                                    @endif
                                                    @endforeach
                                                    @endif
                                                </a>
                                            </figure>
                                            <div class="product-caption text-center">
                                                <h6 class="product-name">
                                                    <a href="product-details.html">{{$produks->nama_produk. " ". $produks->rasa. " - ". $produks->berat. "gr"}}</a>
                                                </h6>
                                                <div class="price-box">
                                                    <span class="price-regular">Rp. {{$produks->harga}}</span>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                        <!-- product item end -->
                                        @endforeach
                                        @else
                                          <div class="col-md-4">

                                          </div>
                                          <div class="col-md-4" style="text-align:center">
                                            <h5>UKM ini Belum memiliki Produk</h5>
                                            <p>Sabar ya, UKM ini sedang bersiap agar pengalaman belanjamu menyenangkan</p>
                                          </div>
                                          <div class="col-md-4">

                                          </div>
                                        @endif
                                        </div>
                                    <!-- </div> -->
                                </div>
                            </div>
                            <!-- product tab content end -->
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- product area end -->
</main>
<style media="screen">
  html {
     scroll-behavior: smooth;
  }
</style>
@endsection
