@extends('layouts.navbar-ukm')
@section('menu-reset-password','custom-active')
@section('style')
    <link rel="stylesheet" href="{{ asset('admin/vendors/ti-icons/css/themify-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/vendors/css/vendor.bundle.base.css') }}">
    <!-- endinject -->
    <!-- Plugin css for this page -->
    <link rel="stylesheet" href="{{ asset('admin/vendors/select2/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/vendors/select2-bootstrap-theme/select2-bootstrap.min.css') }}">
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="{{ asset('admin/css/vertical-layout-dark/style.css') }}">
    <!-- endinject -->
    <link rel="shortcut icon" href="{{ asset('admin/images/favicon.png') }}"/>
@endsection
@section('js')
<script src="{{ asset('admin/vendors/select2/select2.min.js') }}"></script>
<script src="{{asset('admin/vendors/js/vendor.bundle.base.js')}}"></script>
<!-- endinject -->
<!-- Plugin js for this page -->
<script src="{{asset('admin/vendors/typeahead.js/typeahead.bundle.min.js')}}"></script>
<script src="{{asset('admin/vendors/select2/select2.min.js')}}"></script>
<!-- End plugin js for this page -->
<!-- inject:js -->
<script src="{{asset('admin/js/off-canvas.js')}}"></script>
<script src="{{asset('admin/js/hoverable-collapse.js')}}"></script>
<script src="{{asset('admin/js/template.js')}}"></script>
<script src="{{asset('admin/js/settings.js')}}"></script>
<script src="{{asset('admin/js/todolist.js')}}"></script>
<!-- endinject -->
<!-- Custom js for this page-->
<script src="{{asset('admin/js/file-upload.js')}}"></script>
<script src="{{asset('admin/js/typeahead.js')}}"></script>
<script src="{{asset('admin/js/select2.js')}}"></script>
@stop
@section('content')

    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">
          <div class="container">
              <div class="row justify-content-center">
                  <div class="col-md-12">

                      <div class="card">


                          <div class="card-header">{{ __('Reset Password UKM') }}</div>

                          <div class="card-body">

                              <form method="POST" action="{{ url('/super-admin/reset-password') }}">

                                  @csrf

                                  <div class="form-group row">
                                      <label for="name" class="col-md-3 col-form-label text-md-right">{{ __('Nama UKM') }}</label>

                                      <div class="col-md-6">
                                        <div class="form-group">
                                            <select style="width: 100%" id="ukm" class="js-example-basic-single width100" name="ukm_id">
                                                @foreach($ukm as $ukms)
                                                    <option value="{{ $ukms->user->id }}" name="ukm_id">
                                                        {{ $ukms->user->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                  </div>
                                  </div>


                                  <div class="form-group row mb-0">
                                      <div class="col-md-6 offset-md-4">
                                          <button type="submit" class="btn btn-primary">
                                              {{ __('Reset Password') }}
                                          </button>
                                      </div>
                                  </div>
                              </form>
                          </div>
                      </div>
                  </div>
              </div>
          </div>

        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->

        <!-- partial -->
    </div>
    <!-- main-panel ends -->
</div>

@endsection
