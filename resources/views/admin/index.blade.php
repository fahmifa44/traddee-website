@extends('layouts.navbar-ukm')
@section('menu-beranda-admin','custom-active')
@section('js')
<script src="{{asset('admin/vendors/js/vendor.bundle.base.js')}}"></script>
<!-- endinject -->
<!-- Plugin js for this page -->
<script src="{{asset('admin/vendors/typeahead.js/typeahead.bundle.min.js')}}"></script>
<script src="{{asset('admin/vendors/select2/select2.min.js')}}"></script>
<!-- End plugin js for this page -->
<!-- inject:js -->
<script src="{{asset('admin/js/off-canvas.js')}}"></script>
<script src="{{asset('admin/js/hoverable-collapse.js')}}"></script>
<script src="{{asset('admin/js/template.js')}}"></script>
<script src="{{asset('admin/js/settings.js')}}"></script>
<script src="{{asset('admin/js/todolist.js')}}"></script>
<!-- endinject -->
<!-- Custom js for this page-->
<script src="{{asset('admin/js/file-upload.js')}}"></script>
<script src="{{asset('admin/js/typeahead.js')}}"></script>
<script src="{{asset('admin/js/select2.js')}}"></script>
@stop


@section('title','Beranda')

@section('content')

        <!-- partial -->
        <div class="main-panel">
            <div class="content-wrapper">
              <div class="row">
                @forelse($ukm as $ukm)

                    <div class="col-md-4 stretch-card mt-4">
                        <div class="card">
                            <div class="card-body">
                                <p class="card-title">UKM <strong>{{$ukm->user->name}}</strong></p>
                                <p>Nama Pemilik : <strong>{{$ukm->nama_pemilik}}</strong></p>
                                <p>Alamat : <strong>{{$ukm->alamat}}</strong></p>
                                <p>Username : <strong>{{$ukm->user->username}}</strong></p>
                                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#ukm{{$ukm->id}}">
                                  <i class="ti-pencil menu-icon"></i>
                                   <!-- <span class="menu-title">Edit UKM</span> -->
                                </button>
                                <a href="{{ url($ukm->slug) }}" class="btn btn-success btn-sm">
                                  <i class="ti-eye menu-icon"></i>
                                   <!-- <span class="menu-title">Detail UKM</span> -->
                                </a>
                                <button type="button" class="btn btn-danger btn-sm float-right" data-toggle="modal" data-target="#delete{{$ukm->id}}">
                                  <i class="ti-trash menu-icon"></i>
                                   <!-- <span class="menu-title">Edit UKM</span> -->
                                </button>

                                <!-- MODAL DELETE -->

                                <div class="modal fade" id="delete{{$ukm->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                  <div class="modal-dialog modal-dialog" role="document">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLongTitle">Peringatan</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body">
                                        Anda yakin ingin menghapus UKM <strong>{{$ukm->user->name}}</strong> ?
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                        <form action="{{ url('/super-admin/delete-ukm') }}" method="post">
                                        @csrf
                                          <input type="hidden" name="id_ukm" value="{{$ukm->id}}">
                                          <input type="hidden" name="nama_ukm" value="{{$ukm->user->name}}">
                                          <input type="hidden" name="id_user" value="{{$ukm->user->id}}">
                                          <button type="submit" class="btn btn-success">Konfirmasi</button>
                                      </form>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <!-- END MODAL DELETE -->

                              <!-- Modal -->
                              <div class="modal fade" id="ukm{{$ukm->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h5 class="modal-title" id="exampleModalLongTitle">{{$ukm->user->name}}</h5>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <div class="modal-body">
                                      <form method="POST" action="{{ url('/update-ukm') }}">
                                          @csrf
                                          <input type="hidden" name="id_ukm" value="{{$ukm->id}}">
                                          <input type="hidden" name="id_user" value="{{$ukm->user->id}}">
                                          <div class="form-group row">
                                              <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nama UKM') }}</label>

                                              <div class="col-md-6">
                                                  <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $ukm->user->name }}" required autocomplete="name" autofocus>
                                              </div>
                                          </div>

                                          <div class="form-group row">
                                              <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nama Pemilik') }}</label>

                                              <div class="col-md-6">
                                                  <input id="name" type="text" class="form-control" name="nama_pemilik" value="{{ $ukm->nama_pemilik }}" required autocomplete="name" autofocus>
                                              </div>
                                          </div>

                                          <div class="form-group row">
                                              <label for="username" class="col-md-4 col-form-label text-md-right">{{ __('Nomor WA') }}</label>
                                              <div class="col-md-6">
                                                  <input id="username" type="number" class="form-control" name="username" value="{{ $ukm->user->username }}" required autocomplete="username">
                                                  <input type="hidden" name="last_username" value="{{ $ukm->user->username }}">
                                              </div>
                                          </div>

                                          <div class="form-group row">
                                              <label for="alamat" class="col-md-4 col-form-label text-md-right">{{ __('Alamat') }}</label>
                                              <div class="col-md-6">
                                                  <input id="alamat" type="text" class="form-control" name="alamat" value="{{ $ukm->alamat }}" required autocomplete="alamat">
                                              </div>
                                          </div>

                                          <div class="form-group row">
                                              <label for="iframe" class="col-md-4 col-form-label text-md-right">{{ __('Link Iframe') }}</label>
                                              <div class="col-md-6">
                                                <textarea class="form-control" name="iframe" rows="8" cols="80">{{$ukm->iframe}}</textarea>
                                                  <!-- <input id="iframe" value="" type="text" class="form-control" name="iframe" required autocomplete="iframe"> -->
                                              </div>
                                          </div>


                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
                                      <button type="submit" class="btn btn-primary btn-sm">Save changes</button>
                                    </div>
                                  </div>
                                  </form>
                                </div>
                              </div>
                            </div>
                        </div>
                    </div>

                    @empty
                    <p>Kosong</p>
                @endforelse
              </div>
            <br>

            </div>

        </div>
        <!-- main-panel ends -->
    </div>
@endsection
