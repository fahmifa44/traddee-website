@extends('layouts.navbar-ukm')
@section('menu-user-admin','custom-active')
@section('js')
    <script src="{{asset('admin/vendors/js/vendor.bundle.base.js')}}"></script>
    <!-- endinject -->
    <!-- Plugin js for this page -->
    <script src="{{asset('admin/vendors/typeahead.js/typeahead.bundle.min.js')}}"></script>
    <script src="{{asset('admin/vendors/select2/select2.min.js')}}"></script>
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src="{{asset('admin/js/off-canvas.js')}}"></script>
    <script src="{{asset('admin/js/hoverable-collapse.js')}}"></script>
    <script src="{{asset('admin/js/template.js')}}"></script>
    <script src="{{asset('admin/js/settings.js')}}"></script>
    <script src="{{asset('admin/js/todolist.js')}}"></script>
    <!-- endinject -->
    <!-- Custom js for this page-->
    <script src="{{asset('admin/js/file-upload.js')}}"></script>
    <script src="{{asset('admin/js/typeahead.js')}}"></script>
    <script src="{{asset('admin/js/select2.js')}}"></script>
@stop


@section('title','Beranda')

@section('content')

    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="col-12">
                <div class="table-responsive">
                    <div id="order-listing_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">

                        <div class="row">
                            <div class="col-sm-12">
                                <table id="order-listing" class="table dataTable no-footer" role="grid"
                                       aria-describedby="order-listing_info">
                                    <thead>
                                    <tr role="row">
                                        <th class="sorting_asc" tabindex="0" aria-controls="order-listing" rowspan="1"
                                            colspan="1" style="width: 86.15px;color: white;" aria-sort="ascending"
                                            aria-label="Order #: activate to sort column descending">
                                            No. #
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="order-listing" rowspan="1"
                                            colspan="1" style="width: 146.483px;color: white;"
                                            aria-label="Purchased On: activate to sort column ascending">
                                            Nama User
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="order-listing" rowspan="1"
                                            colspan="1" style="width: 105.833px;color: white;"
                                            aria-label="Customer: activate to sort column ascending">
                                            No. HP
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="order-listing" rowspan="1"
                                            colspan="1" style="width: 79.6px;color: white;"
                                            aria-label="Ship to: activate to sort column ascending">
                                            Alamat
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="order-listing" rowspan="1"
                                            colspan="1" style="width: 115.017px;color: white;"
                                            aria-label="Base Price: activate to sort column ascending">
                                            Nama Toko
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="order-listing" rowspan="1"
                                            colspan="1" style="width: 84.85px;color: white;"
                                            aria-label="Actions: activate to sort column ascending">
                                            Actions
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($user as $index=>$users)
                                        @if($users->ukm->active == 0)
                                            <tr>
                                                <td style="color: white">{{ $index+1 }}</td>
                                                <td style="color: white">{{ $users->name }}</td>
                                                <td style="color: white">{{ $users->username }}</td>
                                                <td style="color: white">{{ $users->ukm->alamat }}</td>
                                                <td style="color: white">{{ $users->ukm->nama_pemilik }}</td>
                                                <td>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <button class="btn btn-primary">Detail</button>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <button class="btn btn-success" data-toggle="modal" data-target="#acc{{$users->id}}">ACC</button>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <button class="btn btn-danger">Cancel</button>
                                                        </div>
                                                    </div>
                                                </td>
{{--                                                modal acc --}}
{{--                                                <div class="modal fade" id="acc{{$user->ukm->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">--}}
{{--                                                    <div class="modal-dialog modal-dialog" role="document">--}}
{{--                                                        <div class="modal-content">--}}
{{--                                                            <div class="modal-header">--}}
{{--                                                                <h5 class="modal-title" id="exampleModalLongTitle">Peringatan</h5>--}}
{{--                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
{{--                                                                    <span aria-hidden="true">&times;</span>--}}
{{--                                                                </button>--}}
{{--                                                            </div>--}}
{{--                                                            <div class="modal-body">--}}
{{--                                                                Anda yakin ingin ACC UKM <strong>{{$ukm->user->name}}</strong> ?--}}
{{--                                                            </div>--}}
{{--                                                            <div class="modal-footer">--}}
{{--                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>--}}
{{--                                                                <form action="{{ url('/super-admin/update') }}" method="post">--}}
{{--                                                                    @csrf--}}
{{--                                                                    <input type="hidden" name="id_ukm" value="{{$users->ukm->id}}">--}}
{{--                                                                    <button type="submit" class="btn btn-success">Konfirmasi</button>--}}
{{--                                                                </form>--}}
{{--                                                            </div>--}}
{{--                                                        </div>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
                                            </tr>
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>

                </div>
            </div>

        </div>

    </div>
    <!-- main-panel ends -->
@endsection
