@extends('layouts.navbar-ukm')
@section('menu-buat-ukm','custom-active')
@section('js')
<script src="{{asset('admin/vendors/js/vendor.bundle.base.js')}}"></script>
<!-- endinject -->
<!-- Plugin js for this page -->
<script src="{{asset('admin/vendors/typeahead.js/typeahead.bundle.min.js')}}"></script>
<script src="{{asset('admin/vendors/select2/select2.min.js')}}"></script>
<!-- End plugin js for this page -->
<!-- inject:js -->
<script src="{{asset('admin/js/off-canvas.js')}}"></script>
<script src="{{asset('admin/js/hoverable-collapse.js')}}"></script>
<script src="{{asset('admin/js/template.js')}}"></script>
<script src="{{asset('admin/js/settings.js')}}"></script>
<script src="{{asset('admin/js/todolist.js')}}"></script>
<!-- endinject -->
<!-- Custom js for this page-->
<script src="{{asset('admin/js/file-upload.js')}}"></script>
<script src="{{asset('admin/js/typeahead.js')}}"></script>
<script src="{{asset('admin/js/select2.js')}}"></script>
@stop
@section('content')

    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">
          <div class="container">
              <div class="row justify-content-center">
                  <div class="col-md-12">

                      <div class="card">


                          <div class="card-header">{{ __('Buat UKM') }}</div>

                          <div class="card-body">

                              <form method="POST" action="{{ url('/registrasi-ukm') }}">

                                  @csrf

                                  <div class="form-group row">
                                      <label for="name" class="col-md-3 col-form-label text-md-right">{{ __('Nama UKM') }}</label>

                                      <div class="col-md-6">
                                          <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus required>
                                      </div>
                                  </div>

                                  <div class="form-group row">
                                      <label for="name" class="col-md-3 col-form-label text-md-right">{{ __('Nama Pemilik') }}</label>

                                      <div class="col-md-6">
                                          <input id="name" type="text" class="form-control" name="nama_pemilik" required autocomplete="name" autofocus>
                                      </div>
                                  </div>

                                  <div class="form-group row">
                                      <label for="username" class="col-md-3 col-form-label text-md-right">{{ __('Nomor WA') }}</label>
                                      <div class="col-md-6">
                                          <input id="username" type="number" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" required autocomplete="username">
                                      </div>
                                  </div>

                                  <div class="form-group row">
                                      <label for="alamat" class="col-md-3 col-form-label text-md-right">{{ __('Alamat') }}</label>
                                      <div class="col-md-6">
                                          <input id="alamat" type="text" class="form-control" name="alamat" value="{{ old('alamat') }}" required autocomplete="alamat">
                                      </div>
                                  </div>

                                  <div class="form-group row">
                                      <label for="iframe" class="col-md-3 col-form-label text-md-right">{{ __('Link Iframe') }}</label>
                                      <div class="col-md-6">
                                          <input id="iframe" type="text" class="form-control" name="iframe" required autocomplete="iframe">
                                      </div>
                                      <div class="col-md-3">
                                        <a class="btn btn-primary" href="https://www.google.co.id/maps" target="_blank">
                                              {{ __('Lihat Maps') }}
                                        </a>
                                      </div>
                                  </div>

                                  <div class="form-group row">
                                      <label for="password" class="col-md-3 col-form-label text-md-right">{{ __('Password') }}</label>

                                      <div class="col-md-6">
                                          <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                          @error('password')
                                              <span class="invalid-feedback" role="alert">
                                                  <strong>{{ $message }}</strong>
                                              </span>
                                          @enderror
                                      </div>
                                  </div>

                                  <div class="form-group row">
                                      <label for="password-confirm" class="col-md-3 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                                      <div class="col-md-6">
                                          <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                      </div>
                                  </div>

                                  <div class="form-group row mb-0">
                                      <div class="col-md-6 offset-md-4">
                                          <button type="submit" class="btn btn-primary">
                                              {{ __('Buat UKM') }}
                                          </button>
                                      </div>
                                  </div>
                              </form>
                          </div>
                      </div>
                  </div>
              </div>
          </div>

        </div>

        <!-- partial -->
    </div>
    <!-- main-panel ends -->
</div>

@endsection
