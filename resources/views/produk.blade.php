@extends('layouts.guestNavbar')

@section('content')
<main>
    <!-- breadcrumb area start -->
    <div class="breadcrumb-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb-wrap">
                        <nav aria-label="breadcrumb">
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url('/')}}"><i class="fa fa-home"></i></a></li>
                                <li class="breadcrumb-item"><a href="{{url('/')}}/{{$barang->ukm->slug}}">UKM {{$barang->ukm->user->name}}</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Detail Produk</li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumb area end -->

    <!-- page main wrapper start -->
    <div class="shop-main-wrapper section-padding pb-0">
        <div class="container">
            <div class="row">
                <!-- product details wrapper start -->
                <div class="col-lg-12 order-1 order-lg-2">
                    <!-- product details inner end -->
                    <div class="product-details-inner">
                        <div class="row">
                            <div class="col-lg-5">
                                <div class="product-large-slider">
                                  @foreach($barang->foto_produk as $foto_produks)
                                    <div class="pro-large-img img-zoom">
                                        <img src="{{url('/')}}/foto_produk/{{$foto_produks->file}}" alt="product-details" style="object-fit: cover;height: 350px;" />
                                    </div>
                                  @endforeach
                                </div>
                                <div class="pro-nav slick-row-10 slick-arrow-style">
                                  @foreach($barang->foto_produk as $foto_produks)
                                    <div class="pro-nav-thumb">
                                        <img src="{{url('/')}}/foto_produk/{{$foto_produks->file}}" alt="product-details" style="object-fit: cover;height: 100px;width:300px"/>
                                    </div>
                                  @endforeach
                                </div>
                            </div>
                            <div class="col-lg-7">
                                <div class="product-details-des">
                                    <div class="manufacturer-name">
                                        <a href="{{url('/')}}/{{$barang->ukm->slug}}">Produk UKM {{$barang->ukm->user->name}}</a>
                                    </div>
                                    <h3 class="product-name">{{$barang->nama_produk. " ". $barang->rasa. " - ". $barang->berat. "gr"}}</h3>
                                    <div class="price-box">
                                        <span class="price-regular">Rp. {{$barang->harga}}</span>
                                    </div>
                                    <p class="pro-desc">{{$barang->deskripsi}}
                                    </p>

                                    <div class="quantity-cart-box d-flex align-items-center">
                                        <h6 class="option-title">Rasa : </h6>
                                        <div class="quantity">
                                            {{$barang->rasa}}
                                        </div>

                                    </div>
                                    <div class="quantity-cart-box d-flex align-items-center">
                                        <h6 class="option-title">Berat : </h6>
                                        <div class="quantity">
                                            {{$barang->berat . "gr"}}
                                        </div>

                                    </div>
                                    <div class="action_link">
                                        <a class="btn btn-cart2" href="https://api.whatsapp.com/send?phone={{$barang->ukm->user->username}}&text=Hallo UKM {{$barang->ukm->user->name}} saya Ingin Menanyakan Mengenai Produk Anda {{$barang->nama_produk. " ". $barang->rasa. " - ". $barang->berat. "gr"}} Apakah Tersedia ? " style="background-color:#5cb85c"><i class="fa fa-whatsapp"></i> PESAN !</a>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- product details inner end -->

                </div>
                <!-- product details wrapper end -->
            </div>
        </div>
    </div>
    <!-- page main wrapper end -->

    <!-- related products area start -->
    <section class="related-products section-padding">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <!-- section title start -->
                    <div class="section-title text-center">
                        <h2 class="title">Produk Makanan Terbaru Lainnya</h2>
                        <p class="sub-title">Add related products to weekly lineup</p>
                    </div>
                    <!-- section title start -->
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="product-carousel-4 slick-row-10 slick-arrow-style">
                      @foreach($produk->where('status',1)->sortByDesc('created_at') as $produks)
                      @if($produks->id != $barang->id)
                        <!-- product item start -->
                        <div class="product-item">
                            <figure class="product-thumb" style="width: 100%; height: 0; padding-bottom: 100%; position: relative;">
                                <a href="{{url('/')}}/{{$produks->ukm->slug}}/{{$produks->slug_produk}}">
                                @if($produks->foto_produk->count() == 1)
                                  @php $i=0 @endphp
                                  @foreach($produks->foto_produk->take(1) as $zzz)
                                    @php $i++ @endphp
                                    <img class="pri-img" src="{{asset('/foto_produk/'.$zzz->file)}}" alt="product" style="max-height: 100%;max-width: 100%; width: auto; height: auto; position: absolute; top: 0; bottom: 0; left: 0;right: 0; margin: auto;">
                                    <img class="sec-img" src="{{asset('/foto_produk/'.$zzz->file)}}" alt="product" style="max-height: 100%;max-width: 100%; width: auto; height: auto; position: absolute; top: 0; bottom: 0; left: 0;right: 0; margin: auto;">
                                  @endforeach
                                @else
                                  @php $i=0 @endphp
                                  @foreach($produks->foto_produk->take(2) as $zzz)
                                  @php $i++ @endphp
                                  @if($i==1)
                                  <img class="pri-img" src="{{asset('/foto_produk/'.$zzz->file)}}" alt="product" style="max-height: 100%;max-width: 100%; width: auto; height: auto; position: absolute; top: 0; bottom: 0; left: 0;right: 0; margin: auto;">
                                  @endif
                                  @if($i!=1)
                                  <img class="sec-img" src="{{asset('/foto_produk/'.$zzz->file)}}" alt="product" style="max-height: 100%;max-width: 100%; width: auto; height: auto; position: absolute; top: 0; bottom: 0; left: 0;right: 0; margin: auto;">
                                  @endif
                                  @endforeach
                                  @endif
                                </a>
                            </figure>
                            <div class="product-caption text-center">
                                <div class="product-identity">
                                    <p class="manufacturer-name"><a href="{{url('/')}}/{{$produks->ukm->slug}}">UKM {{$produks->ukm->user->name}}</a></p>
                                </div>
                                <h6 class="product-name">
                                    <a href="{{url('/')}}/{{$produks->ukm->slug}}/{{$produks->slug_produk}}">{{$produks->nama_produk. " " . $produks->rasa . " - " . $produks->berat ."gr"}}</a>
                                </h6>
                                <div class="price-box">
                                    <span class="price-regular">Rp. {{$produks->harga}}</span>
                                </div>
                            </div>
                        </div>
                        <!-- product item end -->
                        @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- related products area end -->
</main>

@endsection
