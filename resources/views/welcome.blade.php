@extends('layouts.guestNavbar')

@section('content')

    <main>
        <!-- hero slider area start -->
        <section class="slider-area">
            <div class="hero-slider-active slick-arrow-style slick-arrow-style_hero slick-dot-style">
                <!-- single slider item start -->
                <div class="hero-single-slide hero-overlay">
                    <div class="hero-slider-item bg-img" data-bg="{{url('/')}}/assets/img/slider/1.jpg">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="hero-slider-content slide-1">
                                        <h2 class="slide-title" style="color:white">GANG DOLLY<span>Collection</span></h2>
                                        <h4 class="slide-desc" style="color:white">udah Jadi UKM Sekarang</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- single slider item end -->

                <!-- single slider item start -->
                <div class="hero-single-slide hero-overlay">
                    <div class="hero-slider-item bg-img" data-bg="{{url('/')}}/assets/img/slider/2.jpg">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="hero-slider-content slide-2">
                                        <h2 class="slide-title" style="color:white">GANG DOLLY<span>Collection</span></h2>
                                        <h4 class="slide-desc" style="color:white">udah Jadi UKM Sekarang</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- single slider item start -->

                <!-- single slider item start -->
                <div class="hero-single-slide hero-overlay">
                    <div class="hero-slider-item bg-img" data-bg="{{url('/')}}/assets/img/slider/3.jpg">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="hero-slider-content slide-3">
                                        <h2 class="slide-title" style="color:white">GANG DOLLY<span>Collection</span></h2>
                                        <h4 class="slide-desc" style="color:white">udah Jadi UKM Sekarang</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- single slider item start -->
            </div>
        </section>
        <!-- hero slider area end -->
        <section class="product-area section-padding" style="padding-bottom:0">
          <div class="section-title text-center">
            <h2 class="title">UKM Dolly</h2>
            <p class="sub-title">Sector Makanan Kering dan Basah</p>
          </div>
        </section>
        <!-- product banner statistics area start -->
        <section class="product-banner-statistics">
            <div class="container-fluid">
                <div class="row">
                  @foreach($list as $lists)
                    <div class="col-md-3" style="margin-top:20px" >
                        <!-- <div class="product-banner-carousel slick-row-10"> -->
                            <!-- banner single slide start -->
                            <div class="banner-slide-item" >
                                <figure class="banner-statistics" style="width: 100%; height: 0; padding-bottom: 100%; position: relative;">
                                    <a href="{{url('/')}}/{{$lists->slug}}">
                                      @if($lists->foto_profil != NULL)
                                        <img src="foto_profil/{{$lists->foto_profil}}" alt="product banner" style="max-height: 100%;max-width: 100%; width: auto; height: auto; position: absolute; top: 0; bottom: 0; left: 0;right: 0; margin: auto;">
                                      @else
                                        <img src="/logo-dolly.png" alt="product banner" style="max-height: 100%;max-width: 100%; width: auto; height: auto; position: absolute; top: 0; bottom: 0; left: 0;right: 0; margin: auto;">
                                      @endif
                                    </a>
                                    <div class="banner-content banner-content_style2">
                                        <h5 class="banner-text3" style="background-color:rgba(255,255,255,0.7); padding:2px; border-radius:4px; text-align:center"><a href="{{url('/')}}/{{$lists->slug}}">UKM {{$lists->user->name}}</a></h5>
                                    </div>
                                </figure>
                            </div>
                            <!-- banner single slide start -->


                        <!-- </div> -->
                    </div>
                    @endforeach
                </div>
            </div>
        </section>
        <!-- product banner statistics area end -->

    </main>
@endsection
