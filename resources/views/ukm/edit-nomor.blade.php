@extends('layouts.navbar-ukm')
@section('menu-pengaturan','custom-active')
@section('js')
<script src="{{asset('admin/vendors/js/vendor.bundle.base.js')}}"></script>
<!-- endinject -->
<!-- Plugin js for this page -->
<script src="{{asset('admin/vendors/typeahead.js/typeahead.bundle.min.js')}}"></script>
<script src="{{asset('admin/vendors/select2/select2.min.js')}}"></script>
<!-- End plugin js for this page -->
<!-- inject:js -->
<script src="{{asset('admin/js/off-canvas.js')}}"></script>
<script src="{{asset('admin/js/hoverable-collapse.js')}}"></script>
<script src="{{asset('admin/js/template.js')}}"></script>
<script src="{{asset('admin/js/settings.js')}}"></script>
<script src="{{asset('admin/js/todolist.js')}}"></script>
<!-- endinject -->
<!-- Custom js for this page-->
<script src="{{asset('admin/js/file-upload.js')}}"></script>
<script src="{{asset('admin/js/typeahead.js')}}"></script>
<script src="{{asset('admin/js/select2.js')}}"></script>
@stop
@section('title','edit Profil UKM')

@section('content')

        <!-- partial -->
        <div class="main-panel">
            <div class="content-wrapper">
                <div class="row">
                    <div class="col-12">
                        {{ session('status') }}
                        <div class="card">
                            <div class="card-body">
                                <div class="row">

                                    <div class="col-lg-12">
                                      <h3 class="pb-4">Data Privasi</h3>
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="border-bottom py-4 form-sample">
                                                    <form action="{{ url('/ukm/profile/edit/permission-nomor') }}"
                                                          method="post">
                                                        @csrf
                                                        @if(session('changeNohp'))
                                                        <div class="form-group text-white alert alert-success">
                                                          Password dan Nomor Hp yang anda masukkan sesuai, silahkan masukkan nomor terbaru
                                                        </div>
                                                        @endif
                                                        <div class="form-group">
                                                            <label>Nomor Hp / Username </label>
                                                            <input class="form-control" name="username"
                                                                   type="text"
                                                                   @if(session('changeNohp'))
                                                                   value="{{Auth::user()->username}}"
                                                                   @endif>
                                                        </div>
                                                        @if(!session('changeNohp'))
                                                        <div class="form-group">
                                                            <label>Password </label>
                                                            <input class="form-control" name="password_lama"
                                                                   type="password">
                                                        </div>
                                                        <button class="btn btn-primary" type="submit">Simpan</button>
                                                        @endif
                                                    </form>
                                                </div>
                                            </div>
                                            @if(session('changeNohp'))
                                            <div class="col-8">
                                                <div class="border-bottom py-4 form-sample">
                                                    <form action="{{ url('/ukm/profile/edit/update-nohp') }}"
                                                          method="post">
                                                        @csrf
                                                        <!-- <h3 class="pb-4">Data password</h3> -->

                                                        <div class="form-group col-md-8">
                                                            <label>Nomor Hp / Username Baru </label>
                                                            <input class="form-control" name="nohp_baru"
                                                                   type="text" required>
                                                        </div>
<!--
                                                        <div class="form-group col-md-8">
                                                            <label>Ulangi Password Baru </label>
                                                            <input class="form-control" name="ulangi_password_baru"
                                                                   type="password">
                                                        </div> -->
                                                        <div class="form-group col-md-8">
                                                          <a class="btn btn-danger" href="{{url('/ukm/profile/edit-nomor')}}">Batal</a>
                                                        <button class="btn btn-primary" type="submit">Simpan</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                            @endif
                                        </div>

                                        <!-- CONTENT -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

@endsection
