@extends('layouts.navbar-ukm')

@section('title','Beranda')
@section('menu-produk','custom-active')
@section('content')
<!-- partial -->
<style>
    .w100 {
        width: 100%;
    }
</style>
<div class="main-panel">
    <div class="content-wrapper">
        <div class="mb-3">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-custom">
                    <li class="breadcrumb-item"><a href=" {{ url('/') }}">Halaman Utama</a></li>
                    <li class="breadcrumb-item"><a href=" {{ url('/ukm') }}">Beranda</a></li>
                    <li class="breadcrumb-item active" aria-current="page"><span>Produk</span></li>
                </ol>
            </nav>
        </div>
        <div class="row">
            <!-- <div class="card col-md-12"> -->
                <div class="card-body">
                    <!-- <div class="template-demo"> -->
                        <!-- <div class="row"> -->
                            <!-- <div class="col-12"> -->
                                <a class="btn btn-primary col-12" id="aaaa" href="{{ url('/ukm/product-create') }}">
                                    Tambah Produk Baru
                                </a>
                            <!-- </div> -->


                        <!-- </div> -->

                    <!-- </div> -->
                </div>
            <!-- </div> -->
              <div class="col-md-12">
                <div class="row">
            @foreach($produk as $up)
            <div class=" col-md-4">

                    <div class="card mt-4">
                        <div class="card-body">
                          @foreach($up->foto_produk->where('status',1) as $foto)
                          <img src="{{asset('/foto_produk/'.$foto->file)}}" style="height: 250px;width: 100%;object-fit: cover;">
                          @endforeach
                            <div class="float-left mt-4">
                                <p class="card-title">Nama Produk :</p>
                                <h4><strong>{{$up->nama_produk}}</strong></h4>
                            </div>
                            <div class="float-right mt-4">
                                <a href="{{ url('/ukm/edit-product') }}/{{$up->slug_produk}}" class="btn btn-outline-secondary btn-icon-text btn-sm">
                                    <i class=" ti-pencil"></i>
                                </a>
                            </div>
                            <div class="float-right mt-4">
                                <a href="{{ url('/'.Auth::user()->ukm->slug.'/'.$up->slug_produk) }}" class="mr-2 btn btn-outline-secondary btn-icon-text btn-sm">
                                    <i class=" ti-eye"></i>
                                </a>
                            </div>
                            <div class="float-right mt-4">
                              <form id="productForm" action="{{ url('/ukm/product/destroy') }}" method="post">
                                @csrf
                                <input type="hidden" name="id_produk" value="{{$up->id}}">
                                <button class="dlt mr-2 btn btn-outline-secondary btn-icon-text btn-sm">
                                    <i class=" ti-trash"></i>
                                </button>
                              </form>
                            </div>


                        </div>
                    </div>
                  </div>
            @endforeach
          </div>
          {!! $produk->render() !!}
          </div>

            <br>
        </div>


        @stop

        @section('js')
            <script src="{{asset('admin/vendors/js/vendor.bundle.base.js')}}"></script>
            <!-- endinject -->
            <!-- Plugin js for this page -->
            <script src="{{asset('admin/vendors/jquery-validation/jquery.validate.min.js')}}"></script>
            <script src="{{asset('admin/vendors/bootstrap-maxlength/bootstrap-maxlength.min.js')}}"></script>
            <!-- End plugin js for this page -->
            <!-- inject:js -->
            <script src="{{asset('admin/js/off-canvas.js')}}"></script>
            <script src="{{asset('admin/js/hoverable-collapse.js')}}"></script>
            <script src="{{asset('admin/js/template.js')}}"></script>
            <script src="{{asset('admin/js/settings.js')}}"></script>
            <script src="{{asset('admin/js/todolist.js')}}"></script>
            <!-- endinject -->
            <!-- Custom js for this page-->
            <script src="{{asset('admin/js/form-validation.js')}}"></script>
            <script src="{{asset('admin/js/bt-maxLength.js')}}"></script>
            <script src="{{asset('admin/vendors/jquery.repeater/jquery.repeater.min.js')}}"></script>
            <script src="{{asset('admin/js/form-repeater.js')}}"></script>
@stop
