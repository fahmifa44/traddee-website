@extends('layouts.navbar-ukm')
@section('menu-pengaturan','custom-active')
@section('title','Beranda')

@section('content')

    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">


          <div class="card">
              <div class="card-body">
                @if($produk->foto_produk->where('status','1')->count() == 0)
                  <h2 class="card-title">Pilih Thumbnail Foto</h2>
                @else
                <h2 class="card-title">Pilih Thumbnail Foto</h2>
                @endif
                  @if($produk->foto_produk->count() == 0)
                  <p>Gaada</p>
                  @else
                <div class="row portfolio-grid">
                  <!-- YG THUMBNAIL -->
                  @if($produk->foto_produk->where('status','1')->count() == 1)
                  @foreach($produk->foto_produk->where('status','1') as $foto)
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
                      <figure class="effect-text-in">
                        <img src="{{asset('/foto_produk/'.$foto->file)}}" alt="image"/ style="height: 125px;object-fit: cover;">
                        <figcaption>

                        </figcaption>
                      </figure>
                    </div>
                  @endforeach
                  <!-- YG BUKAN THUMBNAIL -->
                  @foreach($produk->foto_produk->where('status','!=','1') as $foto)
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
                      <figure class="effect-text-in">
                        <img src="{{asset('/foto_produk/'.$foto->file)}}" alt="image"/>
                        <figcaption>
                          <!-- FORM -->
                            <form action="{{ url('/ukm/product-create/update-profile-mine/') }}" method="post">
                            <p class="row offset-4">
                                @csrf

                              @foreach($produk->foto_produk->where('status','1') as $fotoZ)
                              <input type="hidden" name="id_foto_lawas" value="{{$fotoZ->id}}">
                              @endforeach
                              <input type="hidden" name="id_foto" value="{{$foto->id}}">
                              <button class="btn btn-success btn-sm">
                                <i class="ti-star"></i>
                              </button>
                              <a class="btn btn-danger btn-sm" href="{{ url('/ukm/edit-product/hapus-foto/') }}/{{$foto->id}}">
                                <i class="ti-trash"></i>
                              </a>
                            </p>
                          </form>
                          <!-- END FORM -->
                        </figcaption>
                      </figure>
                    </div>
                  @endforeach
                  @elseif($produk->foto_produk->where('status','1')->count() == 0)
                  @foreach($produk->foto_produk as $foto)
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
                      <figure class="effect-text-in">
                        <img src="{{asset('/foto_produk/'.$foto->file)}}" alt="image"/>
                        <figcaption>
                          <!-- FORM -->
                            <form action="{{ url('/ukm/product-create/select-profile-mine/') }}" method="post">
                            <p class="row offset-4">
                                @csrf
                              <input type="hidden" name="id_foto" value="{{$foto->id}}">
                              <button class="btn btn-success btn-sm">
                                <i class="ti-star"></i>
                              </button>
                              <a class="btn btn-danger btn-sm" href="{{ url('/ukm/edit-product/hapus-foto/') }}/{{$foto->id}}">
                                <i class="ti-trash"></i>
                              </a>
                            </p>
                          </form>
                          <!-- END FORM -->
                        </figcaption>
                      </figure>
                    </div>
                  @endforeach
                  @endif
                </div>
                @endif
                @if($produk->foto_produk->where('status','1')->count() == 0)
                <button type="button" class="btn btn-info text-white">Lain Kali</button>
                @endif
                @if(session('baruBuat'))
                  <a href="{{url('/ukm/product')}}" class="btn btn-info text-white">Lain Kali</a>
                @endif
                @if(!session('baruBuat'))
                  <a href="{{url('/ukm/product')}}" class="btn btn-info text-white">Kembali</a>
                @endif
              </div>
            </div>
          </div>

          
@stop

@section('js')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script src="{{asset('admin/vendors/js/vendor.bundle.base.js')}}"></script>
  <!-- endinject -->
  <!-- Plugin js for this page -->
  <script src="{{asset('admin/vendors/jquery-validation/jquery.validate.min.js')}}"></script>
  <script src="{{asset('admin/vendors/bootstrap-maxlength/bootstrap-maxlength.min.js')}}"></script>
  <!-- End plugin js for this page -->
  <!-- inject:js -->
  <script src="{{asset('admin/js/off-canvas.js')}}"></script>
  <script src="{{asset('admin/js/hoverable-collapse.js')}}"></script>
  <script src="{{asset('admin/js/template.js')}}"></script>
  <script src="{{asset('admin/js/settings.js')}}"></script>
  <script src="{{asset('admin/js/todolist.js')}}"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="{{asset('admin/js/form-validation.js')}}"></script>
  <script src="{{asset('admin/js/bt-maxLength.js')}}"></script>
  <script src="{{asset('admin/vendors/jquery.repeater/jquery.repeater.min.js')}}"></script>
  <script src="{{asset('admin/js/form-repeater.js')}}"></script>
@stop
