@extends('layouts.navbar-ukm')
@section('menu-pengaturan','custom-active')
@section('js')
<script src="{{asset('admin/vendors/js/vendor.bundle.base.js')}}"></script>
<!-- endinject -->
<!-- Plugin js for this page -->
<script src="{{asset('admin/vendors/typeahead.js/typeahead.bundle.min.js')}}"></script>
<script src="{{asset('admin/vendors/select2/select2.min.js')}}"></script>
<!-- End plugin js for this page -->
<!-- inject:js -->
<script src="{{asset('admin/js/off-canvas.js')}}"></script>
<script src="{{asset('admin/js/hoverable-collapse.js')}}"></script>
<script src="{{asset('admin/js/template.js')}}"></script>
<script src="{{asset('admin/js/settings.js')}}"></script>
<script src="{{asset('admin/js/todolist.js')}}"></script>
<!-- endinject -->
<!-- Custom js for this page-->
<script src="{{asset('admin/js/file-upload.js')}}"></script>
<script src="{{asset('admin/js/typeahead.js')}}"></script>
<script src="{{asset('admin/js/select2.js')}}"></script>
@stop
@section('title','Profile UKM')

@section('content')
<div class="main-panel">
    <div class="content-wrapper">
        <div class="row">
            <div class="col-12">
                {{ session('status') }}
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="border-bottom text-center pb-4">
                                  @if(empty($ukm->foto_profil))
                                    <img src="{{asset('admin/images/faces/default.png')}}"
                                         class="img-lg rounded-circle mb-3"/>
                                  @elseif(!empty($ukm->foto_profil))
                                  <img src="{{asset('foto_profil/'.$ukm->foto_profil)}}"
                                       class="img-lg rounded-circle mb-3"/>
                                  @endif
                                    <div class="mb-3">
                                        <h3>{{$ukm->user->name}} ({{$ukm->user->role}})</h3>
                                    </div>
                                <!-- <p class="w-75 mx-auto mb-3">Role : <strong>{{$ukm->user->role}}</strong></p> -->
                                    <p class="w-75 mx-auto mb-3">Nomor WA :
                                        <strong>{{$ukm->user->username}}</strong></p>
                                    <p class="w-75 mx-auto mb-3">Pemilik :
                                        <strong>{{$ukm->nama_pemilik}}</strong></p>
                                    <p class="w-75 mx-auto mb-3">Alamat : <strong>{{$ukm->alamat}}</strong></p>
                                    <p class="w-75 mx-auto mb-3">Deskripsi : <br><strong>{{$ukm->deskripsi}}</strong></p>

                                    <div class="d-flex justify-content-center">
                                        {{--                                                <button class="btn btn-success mr-1">Hire Me</button>--}}
                                        <a href="{{ url('/ukm/profile/edit') }}"><button class="btn btn-success">Edit profil</button></a>
                                    </div>
                                </div>
                                <div class="border-bottom py-4">
                                    <p>Kategori UKM</p>
                                    <div>
                                        <label class="badge badge-outline-light">Makanan</label>
                                    </div>
                                </div>
                                <div class="border-bottom py-4">
                                    <p>Tanggal Bergabung</p>
                                    <div>
                                        <label
                                            class="badge badge-light">{{date('d M Y', strtotime($ukm->created_at))}}</label>
                                    </div>
                                </div>
                                <div class="border-bottom py-4">
                                    <p>Ingin keluar? klik tombol keluar dibawah ini</p>
                                    <div class="d-flex justify-content-center">
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST">
                                            @csrf
                                            <button type="submit" name="button"
                                                    class="btn btn-rounded btn-danger btn-lg"><i
                                                    class="ti-power-off text-white mr-2"></i>Logout
                                            </button>
                                        </form>
                                    </div>
                                </div>

                            </div>
                            <div class="col-lg-8">
                                <div class="d-flex justify-content-between">
                                    <h3 class="text-center">Lokasi</h3>
                                </div>

                                <!-- CONTENT -->
                                {!! $ukm->iframe !!}
                                <style>
                                    iframe {
                                        height: 450px;
                                        width: 100%;
                                    }
                                </style>

                            <!-- CONTENT -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
