<!DOCTYPE html>
<html>
<head>
	<title>Membuat Laporan PDF Dengan DOMPDF Laravel</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
    <h3 class="text-right my-5">Kode Pembayaran : #{{ $penjualan->kode_penjualan }}</h3>
    <hr>
	</center>
  <div class="col-md-12">
  <div class="row">
      <div class="col-md-6">
        <p class=""><b>Toko, {{ $user->name }}</b></p>
        <p>{{ $user->Ukm->nama_pemilik }}<br>{{ $user->Ukm->alamat }}.</p>
      </div>
      <div class="col-md-6 offset-6">
        <p class="mb-0 mt-5"> </p>
        <p>Tanggal transaksi : {{ date('D M Y', strtotime($penjualan->created_at)) }}</p>
        <p>Waktu transaksi   : {{ date('H:i', strtotime($penjualan->created_at)) }}</p>
      </div>
    </div>
  </div>
	<table class='table table-bordered'>
		<thead>
			<tr>
        <th>#</th>
        <th>Nama Produk</th>
        <th>Banyak</th>
        <th>Harga satuan</th>
        <th>Total</th>
			</tr>
		</thead>
		<tbody>
			@php $i=1 @endphp
      @foreach($nota as $i=>$nota)
          <tr class="text-right">
              <td class="text-left">{{ $i+1 }}</td>
              <td class="text-left">{{ $nota->produk->nama_produk }}</td>
              <td class="text-left">{{ $nota->banyak }}</td>
              <td class="text-left">Rp.{{ $nota->produk->harga }}</td>
              <td class="text-left">Rp.{{ $nota->total }}</td>
          </tr>
      @endforeach
		</tbody>
	</table>

</body>
</html>










public function cetak_pdf()
{
	$pegawai = Pegawai::all();

	$pdf = PDF::loadview('pegawai_pdf',['pegawai'=>$pegawai]);
	return $pdf->stream();
}
