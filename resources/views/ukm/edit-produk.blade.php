@extends('layouts.navbar-ukm')
@section('menu-produk','custom-active')
@section('title','Beranda')

@section('content')


    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">


          <div class="card">
              <div class="card-body">
                <form action="{{ url('/ukm/product/update') }}" method="post" enctype="multipart/form-data" class="form repeater">
                @csrf
                <input type="hidden" name="produk_id" value="{{ $produk->id }}">
                <h3 class="card-title">Edit Produk <strong>{{ $produk->nama_produk }}</strong></h3>
                <hr>
                <style>
                  hr{
                    background-color: white;
                  }
                </style>
                <div class="form-group row">
                  <div class="col-lg-2">
                    <label class="col-form-label">Nama Produk</label>
                  </div>
                  <div class="col-lg-8">
                    <input name="nama_produk" value="{{$produk->nama_produk}}" class="form-control" maxlength="200" type="text" placeholder="Masukkan nama produk">
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-lg-2">
                    <label class="col-form-label">Berat</label>
                  </div>
                  <div class="col-lg-8">
                    <input name="berat_produk" value="{{$produk->berat}}" class="form-control" maxlength="10" max="1000000000"  id="defaultconfig-2" type="number" placeholder="Masukkan berat produk dalam gram">
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-lg-2">
                    <label class="col-form-label">Rasa</label>
                  </div>
                  <div class="col-lg-8">
                    <input name="rasa_produk" value="{{$produk->rasa}}" class="form-control" maxlength="20" name="" id="defaultconfig-1" type="text" placeholder="Masukkan rasa">
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-lg-2">
                    <label class="col-form-label">Harga</label>
                  </div>
                  <div class="col-lg-8">
                    <input name="harga_produk" value="{{$produk->harga}}" class="form-control" maxlength="10" max="9999999999" name="" id="defaultconfig" type="number" placeholder="Masukkan berat produk dalam gram">
                  </div>
                </div>

                <!-- UPLOAD FOTO -->
                <div class="row portfolio-grid">
                  <!-- THUMBNAIL -->
                  @if($produk->foto_produk->where('status','1')->count() == 1)
                    @foreach($produk->foto_produk->where('status','1') as $foto)
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
                      <figure class="effect-text-in">
                        <img src="{{asset('/foto_produk/'.$foto->file)}}" alt="image"/>
                        <figcaption>
                        </figcaption>
                      </figure>
                    </div>
                    @endforeach
                  @foreach($produk->foto_produk->where('status','!=','1') as $foto)
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
                      <figure class="effect-text-in">
                        <img src="{{asset('/foto_produk/'.$foto->file)}}" alt="image"/>
                        <figcaption>
                          <!-- FORM -->
                            <p class="row offset-4">
                              <a class="btn btn-success btn-sm" href="{{ url('/ukm/product-create/') }}/{{$produk->id}}/select-profile/">
                                <i class="ti-star"></i>
                              </a>
                              <a class="btn btn-danger btn-sm" href="{{ url('/ukm/edit-product/hapus-foto/') }}/{{$foto->id}}">
                                <i class="ti-trash"></i>
                              </a>
                            </p>
                          <!-- END FORM -->
                        </figcaption>
                      </figure>
                    </div>
                  @endforeach
                  @endif
                </div>
                <div class="form-group row">
                  <div class="col-lg-2">
                    <label class="col-form-label">UPLOAD</label>
                  </div>
                  <div class="col-lg-8 field_wrapper">
                     <div class="row">
                       <div class="col-md-10">
                         <input type="file" class="form-control form-control-sm" name="field_name[]">
                       </div>
                       <div class="col-md-2">
                         <a href="javascript:void(0);" class="add_button btn btn-primary" title="Add field">tambah</a>
                       </div>
                     </div>
                     <br>
                  </div>
                </div>
                <!-- END UPLOAD FOTO -->

                <div class="form-group row">
                  <div class="col-lg-2">
                    <label class="col-form-label">Deskripsi</label>
                  </div>
                  <div class="col-lg-8">
                    <textarea name="deskripsi_produk" id="maxlength-textarea" class="form-control" maxlength="255" rows="4" placeholder="This textarea has a limit of 100 chars.">{{$produk->deskripsi}}
                    </textarea>
                  </div>
                </div>
                <div class="form-group row">
                  <button type="submit" class="btn btn-primary">Update Produk</button>
                </div>
                </form>
              </div>

            </div>
          </div>


@stop

@section('js')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
      <script type="text/javascript">
          $(document).ready(function(){
          var maxField = 10; //Input fields increment limitation
          var addButton = $('.add_button'); //Add button selector
          var wrapper = $('.field_wrapper'); //Input field wrapper
          var fieldHTML = '<div class="tumbal"><div class="row"><div class="col-md-10"><input type="file" class="form-control form-control-sm" name="field_name[]"></div><div class="col-md-2"><a href="javascript:void(0);" class="remove_button btn btn-danger">Hapus</a></div></div></div><br>'; //New input field html
          var x = 1; //Initial field counter is 1

          //Once add button is clicked
          $(addButton).click(function(){
          //Check maximum number of input fields
          if(x < maxField){
          x++; //Increment field counter
          $(wrapper).append(fieldHTML); //Add field html
          }
          });

          //Once remove button is clicked
          $(wrapper).on('click', '.remove_button', function(e){
          e.preventDefault();
          $('.tumbal').remove(); //Remove field html
          x--; //Decrement field counter
          });
          });
      </script>
<script src="{{asset('admin/vendors/js/vendor.bundle.base.js')}}"></script>
  <!-- endinject -->
  <!-- Plugin js for this page -->
  <script src="{{asset('admin/vendors/jquery-validation/jquery.validate.min.js')}}"></script>
  <script src="{{asset('admin/vendors/bootstrap-maxlength/bootstrap-maxlength.min.js')}}"></script>
  <!-- End plugin js for this page -->
  <!-- inject:js -->
  <script src="{{asset('admin/js/off-canvas.js')}}"></script>
  <script src="{{asset('admin/js/hoverable-collapse.js')}}"></script>
  <script src="{{asset('admin/js/template.js')}}"></script>
  <script src="{{asset('admin/js/settings.js')}}"></script>
  <script src="{{asset('admin/js/todolist.js')}}"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="{{asset('admin/js/form-validation.js')}}"></script>
  <script src="{{asset('admin/js/bt-maxLength.js')}}"></script>
  <script src="{{asset('admin/vendors/jquery.repeater/jquery.repeater.min.js')}}"></script>
  <script src="{{asset('admin/js/form-repeater.js')}}"></script>
@stop
