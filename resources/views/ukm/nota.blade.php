@extends('layouts.navbar-ukm')
@section('title','Nota Toko')
@section('menu-kasir','custom-active')

@section('content')
<div class="main-panel">
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-12 grid-margin">
                <div class="mb-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb breadcrumb-custom">
                            <li class="breadcrumb-item"><a href=" {{ url('/') }}">Halaman Utama</a></li>
                            <li class="breadcrumb-item"><a href=" {{ url('/ukm') }}">Beranda</a></li>
                            <li class="breadcrumb-item"><a href=" {{ url('/ukm/kasir') }}">Kasir</a></li>
                            <li class="breadcrumb-item active" aria-current="page"><span>Nota</span></li>
                        </ol>
                    </nav>
                </div>
                <div class="row">
                    {{ session('status') }}
                    <div class="col-12 col-xl-5 mb-4 mb-xl-0">
                        <h4 class="font-weight-bold">NOTA</h4>
                        <h4 class="font-weight-normal mb-0">catatan penjualan sistem kasir</h4>
                    </div>
                    <div class="col-12 mt-4">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Data table</h4>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="table-responsive">
                                            <div id="order-listing_wrapper"
                                                 class="dataTables_wrapper dt-bootstrap4 no-footer">
                                                <div class="row">

                                                    <div class="col-sm-12 col-md-6">
                                                        <div id="order-listing_filter" class="dataTables_filter">
                                                            <label><input type="search" class="form-control"
                                                                          placeholder="Search"
                                                                          aria-controls="order-listing"></label></div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12">
                                                        <table id="order-listing" class="table dataTable no-footer"
                                                               role="grid" aria-describedby="order-listing_info">
                                                            <thead>
                                                            <tr role="row">
                                                                <th class="sorting_asc text-white" tabindex="0"
                                                                    aria-controls="order-listing" rowspan="1" colspan="1"
                                                                    style="width: 68.1833px;" aria-sort="ascending"
                                                                    aria-label="Order #: activate to sort column descending">
                                                                    No #
                                                                </th>
                                                                <th class="sorting text-white" tabindex="0"
                                                                    aria-controls="order-listing" rowspan="1" colspan="1"
                                                                    style="width: 120.233px;"
                                                                    aria-label="Purchased On: activate to sort column ascending">
                                                                    Tanggal Penjualan
                                                                </th>
                                                                <th class="sorting text-white" tabindex="0"
                                                                    aria-controls="order-listing" rowspan="1" colspan="1"
                                                                    style="width: 85.15px;"
                                                                    aria-label="Customer: activate to sort column ascending">
                                                                    Kode Penjualan
                                                                </th>
                                                                <th class="sorting text-white" tabindex="0"
                                                                    aria-controls="order-listing" rowspan="1" colspan="1"
                                                                    style="width: 63.65px;"
                                                                    aria-label="Ship to: activate to sort column ascending">
                                                                    Total Bayar
                                                                </th>
                                                                <th class="sorting text-white" tabindex="0"
                                                                    aria-controls="order-listing" rowspan="1" colspan="1"
                                                                    style="width: 68.1667px;"
                                                                    aria-label="Status: activate to sort column ascending">
                                                                    Status
                                                                </th>
                                                                <th class="sorting text-white" tabindex="0"
                                                                    aria-controls="order-listing" rowspan="1" colspan="1"
                                                                    style="width: 67.05px;"
                                                                    aria-label="Actions: activate to sort column ascending">
                                                                    Actions
                                                                </th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>

                                                            @foreach($nota as $i =>$nota)
                                                            <tr role="row" class="odd">
                                                                <td class="sorting_1 text-white"> {{ $i+1 }}</td>
                                                                <td class="text-white">{{ $nota->created_at }}</td>
                                                                <td class="text-white">{{ $nota->kode_penjualan }}</td>
                                                                <td class="text-white">{{ $nota->total_bayar }}</td>
                                                                <td>
                                                                    <label class="badge badge-info">Sukses</label>
                                                                </td>
                                                                <td>
                                                                  <a href="{{url('/ukm/kasir/nota/detail')}}/{{$nota->id}}" class="btn btn-outline-primary">View</a>
                                                                </td>
                                                            </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12 col-md-5">
                                                        <div class="dataTables_info" id="order-listing_info" role="status"
                                                             aria-live="polite">Showing 1 to 10 of 10 entries
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12 col-md-7">
                                                        <div class="dataTables_paginate paging_simple_numbers"
                                                             id="order-listing_paginate">
                                                            <ul class="pagination">
                                                                <li class="paginate_button page-item previous disabled"
                                                                    id="order-listing_previous"><a href="#"
                                                                                                   aria-controls="order-listing"
                                                                                                   data-dt-idx="0"
                                                                                                   tabindex="0"
                                                                                                   class="page-link">Previous</a>
                                                                </li>
                                                                <li class="paginate_button page-item active"><a href="#"
                                                                                                                aria-controls="order-listing"
                                                                                                                data-dt-idx="1"
                                                                                                                tabindex="0"
                                                                                                                class="page-link">1</a>
                                                                </li>
                                                                <li class="paginate_button page-item next disabled"
                                                                    id="order-listing_next"><a href="#"
                                                                                               aria-controls="order-listing"
                                                                                               data-dt-idx="2" tabindex="0"
                                                                                               class="page-link">Next</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

@endsection
