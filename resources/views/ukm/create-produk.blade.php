@extends('layouts.navbar-ukm')
@section('menu-produk','custom-active')
@section('title','Beranda')

@section('content')
<div class="main-panel">
    <div class="content-wrapper">


      <div class="card">
          <div class="card-body">
            <form action="{{ url('/ukm/product/create') }}" method="post" enctype="multipart/form-data" class="form repeater">
            @csrf
            <h3 class="card-title">Buat Produk</h3>
            <hr>
            <style>
              hr{
                background-color: white;
              }
            </style>
            <div class="form-group row">
              <div class="col-lg-2">
                <label class="col-form-label">Nama Produk</label>
              </div>
              <div class="col-lg-8">
                <input name="nama_produk" class="form-control" maxlength="200" type="text" placeholder="Masukkan nama produk" required>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-lg-2">
                <label class="col-form-label">Berat</label>
              </div>
              <div class="col-lg-8">
                <input name="berat_produk" class="form-control" maxlength="10" max="1000000000"  id="defaultconfig-2" type="number" placeholder="Masukkan berat produk dalam gram" required>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-lg-2">
                <label class="col-form-label">Rasa</label>
              </div>
              <div class="col-lg-8">
                <input name="rasa_produk" class="form-control" maxlength="20" name="" id="defaultconfig-1" type="text" placeholder="Masukkan rasa required">
              </div>
            </div>
            <div class="form-group row">
              <div class="col-lg-2">
                <label class="col-form-label">Harga</label>
              </div>
              <div class="col-lg-8">
                <input name="harga_produk" class="form-control" maxlength="10" max="9999999999" name="" id="defaultconfig" type="number" placeholder="Masukkan berat produk dalam gram" required>
              </div>
            </div>

            <!-- UPLOAD FOTO -->
            <div class="form-group row">
              <div class="col-lg-2">
                <label class="col-form-label">UPLOAD</label>
              </div>
              <div class="col-lg-8 field_wrapper">
                 <div class="row">
                   <div class="col-md-10">
                     <input type="file" class="form-control form-control-sm" name="field_name[]" required>
                   </div>
                   <div class="col-md-2">
                     <a href="javascript:void(0);" class="add_button btn btn-primary" title="Add field">tambah</a>
                   </div>
                 </div>
                 <br>
              </div>
            </div>
            <!-- END UPLOAD FOTO -->

            <div class="form-group row">
              <div class="col-lg-2">
                <label class="col-form-label">Deskripsi</label>
              </div>
              <div class="col-lg-8">
                <textarea required name="deskripsi_produk" id="maxlength-textarea" class="form-control" maxlength="255" rows="4" placeholder="This textarea has a limit of 100 chars."></textarea>
              </div>
            </div>





            <div class="form-group row">
              <button type="submit" class="btn btn-primary">Buat Produk</button>
            </div>
            </form>
          </div>
        </div>
      </div>

@stop

@section('js')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
      <script type="text/javascript">
          $(document).ready(function(){
          var maxField = 10; //Input fields increment limitation
          var addButton = $('.add_button'); //Add button selector
          var wrapper = $('.field_wrapper'); //Input field wrapper
          var fieldHTML = '<div class="tumbal"><div class="row"><div class="col-md-10"><input type="file" class="form-control form-control-sm" name="field_name[]"></div><div class="col-md-2"><a href="javascript:void(0);" class="remove_button btn btn-danger">Hapus</a></div></div></div><br>'; //New input field html
          var x = 1; //Initial field counter is 1

          //Once add button is clicked
          $(addButton).click(function(){
          //Check maximum number of input fields
          if(x < maxField){
          x++; //Increment field counter
          $(wrapper).append(fieldHTML); //Add field html
          }
          });

          //Once remove button is clicked
          $(wrapper).on('click', '.remove_button', function(e){
          e.preventDefault();
          $('.tumbal').remove(); //Remove field html
          x--; //Decrement field counter
          });
          });
      </script>
<script src="{{asset('admin/vendors/js/vendor.bundle.base.js')}}"></script>
  <!-- endinject -->
  <!-- Plugin js for this page -->
  <script src="{{asset('admin/vendors/jquery-validation/jquery.validate.min.js')}}"></script>
  <script src="{{asset('admin/vendors/bootstrap-maxlength/bootstrap-maxlength.min.js')}}"></script>
  <!-- End plugin js for this page -->
  <!-- inject:js -->
  <script src="{{asset('admin/js/off-canvas.js')}}"></script>
  <script src="{{asset('admin/js/hoverable-collapse.js')}}"></script>
  <script src="{{asset('admin/js/template.js')}}"></script>
  <script src="{{asset('admin/js/settings.js')}}"></script>
  <script src="{{asset('admin/js/todolist.js')}}"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="{{asset('admin/js/form-validation.js')}}"></script>
  <script src="{{asset('admin/js/bt-maxLength.js')}}"></script>
  <script src="{{asset('admin/vendors/jquery.repeater/jquery.repeater.min.js')}}"></script>
  <script src="{{asset('admin/js/form-repeater.js')}}"></script>
@stop
