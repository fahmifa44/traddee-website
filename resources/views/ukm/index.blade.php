@extends('layouts.navbar-ukm')
@section('title','Beranda')
@section('menu-beranda','custom-active')

@section('content')
<div class="main-panel">
    <div class="content-wrapper">
        <div class="mb-3">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-custom">
                    <li class="breadcrumb-item"><a href=" {{ url('/') }}">Halaman Utama</a></li>
                    <li class="breadcrumb-item active" aria-current="page"><span>Beranda</span></li>
                </ol>
            </nav>
        </div>
        <div class="row">
            <div class="col-md-12 grid-margin">
                <div class="row">
                    <div class="col-12 col-xl-5 mb-4 mb-xl-0">
                        <h4 class="font-weight-bold">Hai, {{ $user->ukm->nama_pemilik }}</h4>
                        <h4 class="font-weight-normal mb-0">Selamat datang di halaman Penjual</h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <p class="card-title text-md-center text-xl-left">Lama Menggunakan</p>
                        @if(empty($tahun))
                            <div
                                class="d-flex flex-wrap justify-content-between justify-content-md-center justify-content-xl-between align-items-center">
                                <h3 class="mb-0 mb-md-2 mb-xl-0 order-md-1 order-xl-0">{{ $lamapenggunaan }}
                                    <span
                                        class="text-body ml-1"><small>Hari</small></span></h3>
                                <i class="ti-calendar icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>
                            </div>
                        @else
                            <div
                                class="d-flex flex-wrap justify-content-between justify-content-md-center justify-content-xl-between align-items-center">
                                <h3 class="mb-0 mb-md-2 mb-xl-0 order-md-1 order-xl-0">{{ $tahun }}<span
                                        class="text-body ml-1"><small>Tahun</small></span></h3>
                                <i class="ti-calendar icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-3 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <p class="card-title text-md-center text-xl-left">Terbeli sebanyak</p>
                        <div
                            class="d-flex flex-wrap justify-content-between justify-content-md-center justify-content-xl-between align-items-center">
                            <h3 class="mb-0 mb-md-2 mb-xl-0 order-md-1 order-xl-0">{{ Auth::user()->ukm->keranjang->sum('banyak') }}
                                <span
                                    class="text-body ml-1"><small>Pcs</small></span></h3>
                            <i class="ti-user icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <p class="card-title text-md-center text-xl-left">Total Terjual Hari Ini</p>
                        <div
                            class="d-flex flex-wrap justify-content-between justify-content-md-center justify-content-xl-between align-items-center">
                            <h3 class="mb-0 mb-md-2 mb-xl-0 order-md-1 order-xl-0">

                                {{ $total }}<span><small>Pcs</small></span></h3>
                            <i class="ti-agenda icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <p class="card-title text-md-center text-xl-left">Pendapatan Hari Ini</p>
                        <div
                            class="d-flex flex-wrap justify-content-between justify-content-md-center justify-content-xl-between align-items-center">
                            <h3 class="mb-0 mb-md-2 mb-xl-0 order-md-1 order-xl-0">Rp.{{ $income }}</h3>
                            <i class="ti-layers-alt icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-8 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <p class="card-title mb-0">Ketersediaan Produk</p>
                        <div class="table-responsive">
                            <table class="table table-striped table-borderless">
                                <thead>
                                <tr>
                                    <th class="text-white"><strong>Nama produk</strong></th>
                                    <th class="text-white"><strong>status saat ini</strong></th>
                                    <th class="text-white"><strong>Rubah status</strong></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($produk->where('status',1) as $produks)
                                    <tr>
                                        <td class="text-white">{{ $produks->nama_produk }}</td>

                                        @if($produks->tersedia == 0)
                                            <td class="font-weight-medium text-danger">Stok Habis</td>
                                        @else
                                            <td class="font-weight-medium text-success">Stok tersedia</td>
                                        @endif

                                        @if($produks->tersedia == 0)

                                            <td>
                                                <form action="/ukm/rubah-tersedia/{{ $produks->id }}" method="post">
                                                    @csrf
                                                    <button class="btn btn-primary" style="width: 80%;">Ubah tersedia</button>
                                                </form>
                                            </td>
                                        @else
                                            <td>
                                                <form action="/ukm/rubah-habis/{{ $produks->id }}" method="post">
                                                    @csrf
                                                    <button class="btn btn-danger" style="width: 80%;">Ubah Habis</button>
                                                </form>

                                            </td>
                                        @endif

                                    </tr>

                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 stretch-card">
                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <p class="card-title">Paling diminati</p>
                                <div class="charts-data">
                                    @foreach($produk as $produks)
                                        <div class="mt-3">
                                            <p class="text-muted mb-0">{{ $produks->nama_produk }}
                                                || terjual :{{ $produks->keranjang->sum('banyak') }}</p>
                                            <div class="d-flex justify-content-between align-items-center">
                                                @if($produks->keranjang->sum('banyak') != 0 && $totalbanyak != 0)
                                                    <div class="progress progress-md flex-grow-1 mr-4">


                                                        <div class="progress-bar bg-success" role="progressbar"
                                                             style="width: {{ ($produks->keranjang->sum('banyak')/$totalbanyak)*100 }}%"
                                                             aria-valuenow="{{ ($produks->keranjang->sum('banyak')/$totalbanyak)*100 }}"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100">
                                                        </div>

                                                    </div>
                                                    <p class="text-muted mb-0">{{ round(($produks->keranjang->sum('banyak')/$totalbanyak)*100) }}
                                                        %</p>
                                                @endif
                                            </div>
                                        </div>
                                    @endforeach

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 stretch-card grid-margin grid-margin-md-0">
                        <div class="card data-icon-card-primary">
                            <div class="card-body">
                                <p class="card-title text-white">Number of Meetings</p>
                                <div class="row">
                                    <div class="col-8 text-white">
                                        <h3>3404</h3>
                                        <p class="text-white font-weight-light mb-0">The total number of
                                            sessions within the date range. It is the period time</p>
                                    </div>
                                    <div class="col-4 background-icon">
                                        <i class="ti-calendar"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @section('js')
            <script src="{{asset('admin/vendors/js/vendor.bundle.base.js')}}"></script>
            <!-- endinject -->
            <!-- Plugin js for this page -->
            <script src="{{asset('admin/vendors/typeahead.js/typeahead.bundle.min.js')}}"></script>
            <script src="{{asset('admin/vendors/select2/select2.min.js')}}"></script>
            <!-- End plugin js for this page -->
            <!-- inject:js -->
            <script src="{{asset('admin/js/off-canvas.js')}}"></script>
            <script src="{{asset('admin/js/hoverable-collapse.js')}}"></script>
            <script src="{{asset('admin/js/template.js')}}"></script>
            <script src="{{asset('admin/js/settings.js')}}"></script>
            <script src="{{asset('admin/js/todolist.js')}}"></script>
            <!-- endinject -->
            <!-- Custom js for this page-->
            <script src="{{asset('admin/js/file-upload.js')}}"></script>
            <script src="{{asset('admin/js/typeahead.js')}}"></script>
            <script src="{{asset('admin/js/select2.js')}}"></script>

        @stop
@endsection
