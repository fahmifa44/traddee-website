@extends('layouts.navbar-ukm')

@section('title','Sistem Kasir UMKM')
@section('menu-kasir','custom-active')
@section('style')
    <link rel="stylesheet" href="{{ asset('admin/vendors/ti-icons/css/themify-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/vendors/css/vendor.bundle.base.css') }}">
    <!-- endinject -->
    <!-- Plugin css for this page -->
    <link rel="stylesheet" href="{{ asset('admin/vendors/select2/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/vendors/select2-bootstrap-theme/select2-bootstrap.min.css') }}">
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="{{ asset('admin/css/vertical-layout-dark/style.css') }}">
    <!-- endinject -->
    <link rel="shortcut icon" href="{{ asset('admin/images/favicon.png') }}"/>
@endsection

@section('js')
    <!-- plugins:js -->
    <script src="{{ asset('admin/vendors/js/vendor.bundle.base.js') }}"></script>
    <!-- endinject -->
    <!-- Plugin js for this page -->
    <script src="{{ asset('admin/vendors/typeahead.js/typeahead.bundle.min.js') }}"></script>
    <script src="{{ asset('admin/vendors/select2/select2.min.js') }}"></script>
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src="{{ asset('admin/js/off-canvas.js') }}"></script>
    <script src="{{ asset('admin/js/hoverable-collapse.js') }}"></script>
    <script src="{{ asset('admin/js/template.js') }}"></script>
    <script src="{{ asset('admin/js/settings.js') }}"></script>
    <script src="{{ asset('admin/js/todolist.js') }}"></script>
    <!-- endinject -->
    <!-- Custom js for this page-->
    <script src="{{ asset('admin/js/file-upload.js') }}"></script>
    <script src="{{ asset('admin/js/typeahead.js') }}"></script>
    <script src="{{ asset('admin/js/select2.js') }}"></script>
    <!-- End custom js for this page-->
    <!-- Ajax DROPDOWN VALUE -->

@endsection

@section('content')
<div class="main-panel">
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-12 grid-margin">
                <div class="mb-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb breadcrumb-custom">
                            <li class="breadcrumb-item"><a href=" {{ url('/') }}">Halaman Utama</a></li>
                            <li class="breadcrumb-item"><a href=" {{ url('/ukm') }}">Beranda</a></li>
                            <li class="breadcrumb-item active" aria-current="page"><span>Kasir</span></li>
                        </ol>
                    </nav>
                </div>
                <div class="row">
                    {{ session('status') }}
                    <div class="col-12 col-xl-5 mb-4 mb-xl-0">
                        <h4 class="font-weight-bold">KASIR</h4>
                        <h4 class="font-weight-normal mb-0">catat penjualanmu disini</h4>
                        <small>Tambahkan barang ke keranjang terlebih dahulu, jika dirasa sudah benar anda dapat
                            klik bayar.</small>
                    </div>
                    <div class="col-xl-2">

                    </div>
                    <a href="{{ url('ukm/kasir/nota') }}">
                    <div class=" col-12 col-xl-5 mb-xl-0">
                        <button class="btn btn-lg btn-warning" style="float: right;">
                            <i class="settings-close  ti-files mr-3"></i>NOTA
                        </button>
                        </a>
                    </div>
                </div>
            </div>

            <style>
                .width100 {
                    width: 100%;
                }
            </style>
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <form action="{{ url('ukm/kasir/keranjang') }}" method="post">
                        @csrf
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <h4 class="card-title">Nama Barang</h4>
                                    <div class="form-group">
                                        <select id="produk" class="js-example-basic-single width100" name="produk_id">
                                            @foreach($ukm->produk->where('status',1)->where('tersedia',1) as $produk)
                                                <option value="{{ $produk->id }}" name="produk_id">
                                                    {{ $produk->nama_produk }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <h4 class="card-title">Banyak</h4>
                                    <div class="form-group">
                                        <input type="number" name="banyak" class="form-control banyak" required min="1">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <h4 class="card-title">Harga <small>( *Otomatis )</small></h4>
                                    <div class="form-group width100">
                                        <div class="input-group" id="uang">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Rp.</span>
                                            </div>
                                              <input class="form-control" id="hargaSatu">
                                        </div>
                                    </div>
                                </div>
                                <!-- <script type="text/javascript">
                                  function myFunction(e){
                                    document.getElementById('myText').value = e.target.value
                                  }
                                </script> -->
                                <div class="col-md-3">
                                    <h4 class="card-title">Total<small>( *Otomatis )</small></h4>
                                    <div class="form-group width100">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Rp.</span>
                                            </div>
                                            <div class="form-control"> 00
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <h4 class="card-title"></h4>
                                    <div class="form-group">
                                        <input type="submit" class="form-control btn btn-primary mt-2"
                                               value="Tambahhkan">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>


            <script>
                $(document).ready(function () {
                    $.ajaxSetup({cache: false}); // This part addresses an IE bug.  without it, IE will only load the first number and will never refresh
                    setInterval(function () {
                        $('#uang').load('{{ url('/ukm/kasir') }}');
                    }, 3000); // the "3000"
                });

                // $(document).on("keyup", ".banyak", function() {
                //     var sum = 0;
                //     $(".qty1").each(function(){
                //         sum += +$(this).val();
                //     });
                //     $(".total").val(sum);
                // });
            </script>


            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h2 class="card-title">Keranjang Belanja</h2>
                        <div class="row">
                            <div class="col-12">
                                <div class="table-responsive">
                                    <div id="order-listing_wrapper"
                                         class="dataTables_wrapper dt-bootstrap4 no-footer">

                                        <div class="row">
                                            <div class="col-sm-12">
                                                <table id="order-listing" class="table dataTable no-footer"
                                                       role="grid" aria-describedby="order-listing_info">
                                                    <thead>
                                                    <tr role="row">
                                                        <th class="sorting_asc" tabindex="0"
                                                            aria-controls="order-listing" rowspan="1"
                                                            colspan="1" style="width: 86.15px;color: white;"
                                                            aria-sort="ascending"
                                                            aria-label="Order #: activate to sort column descending">
                                                            No. #
                                                        </th>
                                                        <th class="sorting" tabindex="0"
                                                            aria-controls="order-listing" rowspan="1"
                                                            colspan="1" style="width: 146.483px;color: white;"
                                                            aria-label="Purchased On: activate to sort column ascending">
                                                            Nama Barang
                                                        </th>
                                                        <th class="sorting" tabindex="0"
                                                            aria-controls="order-listing" rowspan="1"
                                                            colspan="1" style="width: 105.833px;color: white;"
                                                            aria-label="Customer: activate to sort column ascending">
                                                            Banyak barang
                                                        </th>
                                                        <th class="sorting" tabindex="0"
                                                            aria-controls="order-listing" rowspan="1"
                                                            colspan="1" style="width: 79.6px;color: white;"
                                                            aria-label="Ship to: activate to sort column ascending">
                                                            Harga Barang
                                                        </th>
                                                        <th class="sorting" tabindex="0"
                                                            aria-controls="order-listing" rowspan="1"
                                                            colspan="1" style="width: 115.017px;color: white;"
                                                            aria-label="Base Price: activate to sort column ascending">
                                                            Jumlah
                                                        </th>
                                                        <th class="sorting" tabindex="0"
                                                            aria-controls="order-listing" rowspan="1"
                                                            colspan="1" style="width: 84.85px;color: white;"
                                                            aria-label="Actions: activate to sort column ascending">
                                                            Actions
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($keranjang as $i => $keranjang)
                                                        <tr role="row" class="odd">
                                                            <td class="sorting_1"
                                                                style="color: white;">{{ $i+1 }}</td>
                                                            <td style="color: white;">{{ $keranjang->produk->nama_produk }}</td>
                                                            <td style="color: white;">{{ $keranjang->banyak }}</td>
                                                            <td style="color: white;">{{ $keranjang->produk->harga }}</td>
                                                            <td style="color: white;">{{ $keranjang->total }}</td>
                                                            <td>
                                                                <form method="post" action="/ukm/kasir/hapus-data/{{ $keranjang->id }}">
                                                                    @csrf
                                                                    <button type="submit" class="btn btn-outline-danger">Hapus
                                                                    </button>
                                                                </form>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-5">

                            </div>
                            <div class="col-7">
                                <div class="row">
                                    <div class="col-md-5">

                                    </div>
                                    <div class="col-md-3" style="float: right;">
                                        <h3>TOTAL :</h3>
                                    </div>
                                    <div class="col-md-4 mt-1" style="float:left;">
                                        <h4 class="text-warning">Rp.{{ $total }}</h4>
                                    </div>
                                </div>
                                </div>
                              </div>
                                <div class="form-group">
                                 <form action="{{ url('/ukm/kasir/beli') }}" method="post">
                                   @csrf
                                   <button type="submit" class="form-control btn btn-primary mt-2">Bayar</button>
                                 </form>
                                </div>
                      </div>
                  </div>
@endsection
