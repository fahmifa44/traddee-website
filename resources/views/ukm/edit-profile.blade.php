@extends('layouts.navbar-ukm')
@section('menu-pengaturan','custom-active')
@section('js')
<script src="{{asset('admin/vendors/js/vendor.bundle.base.js')}}"></script>
<!-- endinject -->
<!-- Plugin js for this page -->
<script src="{{asset('admin/vendors/typeahead.js/typeahead.bundle.min.js')}}"></script>
<script src="{{asset('admin/vendors/select2/select2.min.js')}}"></script>
<!-- End plugin js for this page -->
<!-- inject:js -->
<script src="{{asset('admin/js/off-canvas.js')}}"></script>
<script src="{{asset('admin/js/hoverable-collapse.js')}}"></script>
<script src="{{asset('admin/js/template.js')}}"></script>
<script src="{{asset('admin/js/settings.js')}}"></script>
<script src="{{asset('admin/js/todolist.js')}}"></script>
<!-- endinject -->
<!-- Custom js for this page-->
<script src="{{asset('admin/js/file-upload.js')}}"></script>
<script src="{{asset('admin/js/typeahead.js')}}"></script>
<script src="{{asset('admin/js/select2.js')}}"></script>
@stop
@section('title','edit Profil UKM')

@section('content')

        <!-- partial -->
        <div class="main-panel">
            <div class="content-wrapper">
                <div class="row">
                    <div class="col-12">
                        {{ session('status') }}
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="border-bottom text-center pb-4">
                                          @if(empty($ukm->foto_profil))
                                            <img src="{{asset('admin/images/faces/default.png')}}"
                                                 class="img-lg rounded-circle mb-3"/>
                                          @else
                                          <img src="{{asset('/foto_profil/'.$ukm->foto_profil)}}"
                                               class="img-lg rounded-circle mb-3"/>
                                          @endif
                                            <div class="mb-3">
                                                <h3>{{$ukm->user->name}} ({{$ukm->user->role}})</h3>
                                            </div>
                                            <a href="{{ url('/ukm/profile/edit-password') }}">
                                                <button class="btn btn-primary btn-rounded">Ganti password</button>
                                            </a>
                                            <a href="{{ url('/ukm/profile/edit-nomor') }}">
                                                <button class="btn btn-warning btn-rounded">Ganti nomor</button>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="border-bottom py-4 form-sample">
                                                    <form action="{{ url('/ukm/profile/edit/update-data') }}"
                                                          method="post" enctype="multipart/form-data">
                                                        @csrf
                                                        <h3 class="pb-4">Data Pribadi</h3>
                                                        <div class="form-group">
                                                          <label>Foto Profil</label>
                                                          <input type="file" name="foto_profil" class="file-upload-default">
                                                          <div class="input-group col-xs-12">
                                                            <input type="text" class="form-control file-upload-info" disabled placeholder="Unggah Foto Profil UKM">
                                                            <span class="input-group-append">
                                                              <button class="file-upload-browse btn btn-primary" type="button">Unggah</button>
                                                            </span>
                                                          </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Nama Pemilik :</label>
                                                            <input class="form-control" name="nama_pemilik"
                                                                   type="text"
                                                                   value="{{$ukm->nama_pemilik}}">
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Alamat :</label>
                                                            <input class="form-control" name="alamat"
                                                                   type="text"
                                                                   value="{{$ukm->alamat}}">
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Deskripsi UKM :</label>
                                                            <textarea class="form-control" name="deskripsi"
                                                                   type="text" >{{$ukm->deskripsi}}</textarea>
                                                        </div>

                                                        <button class="btn btn-primary" type="submit">Simpan</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- CONTENT -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

@endsection
