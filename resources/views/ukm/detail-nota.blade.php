@extends('layouts.navbar')

@section('title','Nota Toko')

@section('content')
    <div class="container-scroller">
        <!-- partial:partials/_settings-panel.html -->

        <!-- partial -->
        <!-- partial:partials/_sidebar.html -->
        <!-- <nav class="sidebar sidebar-offcanvas" id="sidebar">

        </nav> -->
        <!-- partial -->
        <div class="container-fluid page-body-wrapper full-page-wrappe">
            <div class="content-wrapper">
                <div class="row">
                    <div class="col-md-12 grid-margin">
                        <div class="mb-3">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb breadcrumb-custom">
                                    <li class="breadcrumb-item"><a href=" {{ url('/') }}">Halaman Utama</a></li>
                                    <li class="breadcrumb-item"><a href=" {{ url('/ukm') }}">Beranda</a></li>
                                    <li class="breadcrumb-item"><a href=" {{ url('/ukm') }}">Kasir</a></li>
                                    <li class="breadcrumb-item"><a href=" {{ url('/ukm') }}">Nota</a></li>
                                    <li class="breadcrumb-item active" aria-current="page"><span>Detail nota</span></li>
                                </ol>
                            </nav>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card px-2">
                                    <div class="card-body">
                                        <div class="container-fluid">
                                            <h3 class="text-right my-5">Kode Pembayaran
                                                &nbsp;&nbsp;#{{ $penjualan->kode_penjualan }}</h3>
                                            <hr>
                                        </div>
                                        <div class="container-fluid d-flex justify-content-between">
                                            <div class="col-lg-3 pl-0">
                                                <p class="mt-5 mb-2"><b>Toko, {{ $user->name }}</b></p>
                                                <p>{{ $user->Ukm->nama_pemilik }}<br>{{ $user->Ukm->alamat }}.</p>
                                            </div>
                                            <div class="col-lg-3 pr-0">
                                                <p class="mb-0 mt-5"></p>
                                                <p>Tanggal transaksi
                                                    : {{ date('D M Y', strtotime($penjualan->created_at)) }}</p>
                                                <p>Waktu transaksi
                                                    : {{ date('H:i', strtotime($penjualan->created_at)) }}</p>
                                            </div>
                                        </div>
                                        <div class="container-fluid d-flex justify-content-between">
                                            <div class="col-lg-3 pl-0">

                                            </div>
                                        </div>
                                        <div class="container-fluid mt-5 d-flex justify-content-center w-100">
                                            <div class="table-responsive w-100">
                                                <table class="table">
                                                    <thead>
                                                    <tr class="bg-dark text-white">
                                                        <th>#</th>
                                                        <th>Nama Produk</th>
                                                        <th class="text-right">Banyak</th>
                                                        <th class="text-right">Harga satuan</th>
                                                        <th class="text-right">Total</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($nota as $i=>$nota)
                                                        <tr class="text-right">
                                                            <td class="text-left text-white">{{ $i+1 }}</td>
                                                            <td class="text-left text-white">{{ $nota->produk->nama_produk }}</td>
                                                            <td class="text-white">{{ $nota->banyak }}</td>
                                                            <td class="text-white">Rp.{{ $nota->produk->harga }}</td>
                                                            <td class="text-white">Rp.{{ $nota->total }}</td>
                                                        </tr>
                                                    @endforeach

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="container-fluid mt-5 w-100">
                                            <h4 class="text-right mb-5">Total yang harus dibayar:
                                                Rp.{{ $penjualan->total_bayar }}</h4>
                                            <hr>
                                        </div>
                                        <div class="container-fluid w-100">
                                            <a href="/ukm/kasir/nota/detail/{{ $penjualan->id }}/cetak"
                                               class="btn btn-primary float-right mt-4 ml-2"><i
                                                    class="ti-printer mr-1"></i>Unduh</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">

                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
