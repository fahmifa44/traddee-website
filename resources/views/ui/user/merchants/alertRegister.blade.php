<!DOCTYPE html>
<html lang="en">
<head>
  <title>TRADEE.COM | MERCHANTS | AQILA</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
  <link rel="stylesheet" type="text/css" href="css/userStyle.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

</head>
<body>	

 <!-- modal edit product -->
 <div class="modal fade" id="editProduct">
    <div class="modal-dialog modal-xl">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <div class="modal-body">
          <div class="container-fluid">
            <div class="row">

              <div class="col-5 ">
<!-- slideshow detail produk -->
                <div id="demo" class="carousel slide" data-ride="carousel">
                    <ul class="carousel-indicators">
                      <li data-target="#demo" data-slide-to="0" class="active"></li>
                      <li data-target="#demo" data-slide-to="1"></li>
                      <li data-target="#demo" data-slide-to="2"></li>
                    </ul>

                    <div class="carousel-inner">
                      <div class="carousel-item active">
                        <img src = 'assets/images/iklan1.jpg' width="100%" height="250px"class="img-thumbnail">
                      </div>
                      <div class="carousel-item">
                        <img src = 'assets/images/iklan2.jpg' width="100%" height="250px" class="img-thumbnail">
                      </div>
                      <div class="carousel-item">
                        <img src = 'assets/images/iklan1.jpg' width="100%" height="250px" class="img-thumbnail">
                      </div>
                    </div>

                    <a class="carousel-control-prev" href="#demo" data-slide="prev">
                      <span class="carousel-control-prev-icon"></span>
                    </a>
                    <a class="carousel-control-next" href="#demo" data-slide="next">
                      <span class="carousel-control-next-icon"></span>
                    </a>
                </div>
<!-- /slideshow detail produk -->
              </div>
              
              <div class="col-7 ">
                <div class="card">
                  <div class="card-body">
                  <input type="text" class="form-control" placeholder="Nama Product "></br>
                  <input type="text" class="form-control" placeholder="Kategori"></br>
                  <input type="text" class="form-control" placeholder="Sub Kategori "></br>
                  <input type="text" class="form-control" placeholder="Harga "></br>
                  <hr/>

                  <p style="color:black;">Ganti Foto 1</p>
                  <input type="file" class="form-control-file border">
                  <p style="color:black;">Ganti Foto 2</p>
                  <input type="file" class="form-control-file border">
                  <p style="color:black;">Ganti Foto 3</p>
                  <input type="file" class="form-control-file border">
                  <hr/>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="modal-footer">
        <button type="button" class="btn btn-primary">
            <a href="/Profilemerchants">
            <center><font face="Tahoma" color="white" >Edit</font></center>
            </a>
            </button>
        </div>
        
      </div>
    </div>
  </div>
<!-- / Modal bedit product -->


<div class="boxHeaderProfileMerc2">
            
        <!-- iklan slide show -->
        <div id="demo" class="carousel slide" data-ride="carousel">

          <!-- The slideshow -->
          <div class="carousel-inner">
                <div class="carousel-item active">
                  <img src = 'assets/images/aqilabg.jpg' class="imgHeaderProfileMerc2">
                </div>
          </div>

        <!-- tutup iklan slide show -->

      <!-- txt Slide Show -->
     
      <div class="container-fluid">
       <div class="ProfileHeader2">
         <div class="row">
           <div class="col-3">
              <img src = 'assets/images/user.png' class="imgProfileMerc2">
           </div>

           <div class="col-9">
              <p class="txtProfileMerch11">TOKO BUSANAN AQILA</p>
              <p class="txtProfileMerch22">ID : 000011</p>
              
           </div>
         </div>
       </div>
     
      </div>
      <!-- / txt Slide Show -->
</div>

<div class="boxMainProfileMercAlert">
<div class="container-fluid">
  <div class="row">
    <div class="col-9">

<!-- iklan Slide Show -->
              <div id="demo" class="carousel slide" data-ride="carousel">


                  <ul class="carousel-indicators">
                    <li data-target="#demo" data-slide-to="0" class="active"></li>
                    <li data-target="#demo" data-slide-to="1"></li>
                    <li data-target="#demo" data-slide-to="2"></li>
                  </ul>

                  <div class="carousel-inner">
                  <div class="carousel-item active">
                    <img src="assets/images/iklanH1.jpg" class="ImgSlideUser">
                  </div>
                  <div class="carousel-item">
                    <img src="assets/images/iklanH2.jpg" class="ImgSlideUser">
                  </div>
                  <div class="carousel-item">
                    <img src="assets/images/iklanH3.jpg" class="ImgSlideUser">
                  </div>
                  </div>

                  <a class="carousel-control-prev" href="#demo" data-slide="prev"></a>
                  <a class="carousel-control-next" href="#demo" data-slide="next"></a>

              </div>
  </div>
<!-- iklan -->
  <div class="col-3">
    <div class="boxIklan">

    </div>
    </br> 
    <div class="boxIklan">

    </div>
  </div>
<!-- /iklan -->
<!-- /iklan slide show -->
          </br>
<!-- main -->
<div class="container-fluid">
</br>
<h1 class="dsMerch">Dashboard Merchants</h1>

                  
    <!-- nav -->
    <nav class="navbar navbar-expand-md bg-dark navbar-dark">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="collapsibleNavbar">
      <ul class="nav " role="tablist">
          <li class="nav-item">
          <a class="nav-link active" data-toggle="pill" href="#daftar">Daftar Barang / Jasa</a>
          </li>
          <li class="nav-item">
          <a class="nav-link" data-toggle="pill" href="#">Chat</a>
          </li>
          <li class="nav-item">
          <a class="nav-link" data-toggle="pill" href="#">Informasi Toko</a>
          </li>
          <li>
          <a class="nav-link" data-toggle="pill" href="#">Edit Profile</a>
          </li>
          <li>
          <a class="nav-link" data-toggle="pill" href="#">Edit Informasi Toko</a>
          </li>
          <li>
          <a class="nav-link mr-5" data-toggle="pill" href="#">Ajukan Pemasangan Iklan</a>
          </li>
          <li  class="ml-auto"><a href="/LandingPages">
          <button type="button" class="btn btn-danger" >Logout</button></a>
          </li>
      </ul>
    </div>
    </nav>
    <!-- /nav -->
<!-- konten -->
    <div class="tab-content">
      <div id="daftar" class="container tab-pane active"><br>
      <center><label> Verifikasi ID </label><center>
          <form action="#">
                      <div class="form-group">
                      <input type="text" class="form-control" id="IdVerifikasi" placeholder="Masukan Id yang sudah di kirim melalui WA">
                      </div>
          </form>
          <a href="ProfileMerchants"> <button type="button" class="btn bg-primary btn-block "><font color="white">SUBMIT</font></button> </a>
              <a href="#" class="txtLdNav">Kirim ulang Kode</a>
      </div>
    </div>
<!-- /konten -->

<!-- /main -->





</br>
</br>
<center><p id="footer">Kisah Kreatif | Copyright 2020 © All Rights Reserved</p></center>


</body>
</html>



