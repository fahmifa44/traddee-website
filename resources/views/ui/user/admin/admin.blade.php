<!DOCTYPE html>
<html lang="en">
<head>
  <title>TRADDEE.COM | Admin</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
  <link rel="stylesheet" type="text/css" href="css/userStyle.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

</head>
<body>
<nav class="navbar navbar-expand-sm bg-dark navbar-dark fixed-top">
  <img src="assets/images/admin.png" class="imgAdmin"> <font color="white" size="4"> | Admin</font>
    <div class="collapse navbar-collapse" id="collapsibleNavbar">
        <ul class="nav pills ml-auto">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="pill" href="#dashboard"><font color="white">Dashboard</font></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="pill" href="#toko"><font color="white">Toko</font></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="pill" href="#iklan"><font color="white">Iklan</font></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="pill" href="#pengaturan"><font color="white">Pengaturan Lainnya</font></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="LandingPages"><font color="red">Logout</font></a>
            </li>
        </ul>
    </div>
</nav>

<div class="tab-content">

                    <!-- panel dashboard -->
                    <div id="dashboard" class="tab-pane container-fluid active mt-5">
                    </br>
                        <h1> Dashboard</h1><hr/>
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-4">
                                            </br>
                                            </br>
                                            </br>
                                            <center>Jumlah Toko</center>
                                        </div>
                                        <div class="col-4">
                                            </br>
                                            </br>
                                            </br>
                                            <center>Pengajuan Iklan Berbayar</center>
                                        </div>
                                        <div class="col-4">
                                            </br>
                                            </br>
                                            </br>
                                            <center>Iklan berbayar yang aktif</center>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </br>
                            <div class="card">
                            <div class="container-fluid"><br/><h3>Iklan Slideshow di User</h3><hr/></div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-4">
                                            <img src="assets/images/iklanH1.jpg" width="100%" height="120px">
                                            <center><p>Iklan 1</p></center>
                                        </div>
                                        <div class="col-4">
                                            <img src="assets/images/iklanH2.jpg" width="100%" height="120px">
                                            <center><p>Iklan 2</p></center>
                                        </div>
                                        <div class="col-4">
                                            <img src="assets/images/iklanH3.jpg" width="100%" height="120px">
                                            <center><p>Iklan 3</p></center>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </br>
                            <div class="card">
                            <div class="container-fluid"><br/><h3>Iklan Slideshow di Admin</h3><hr/></div>
                                <div class="card-body">
                                    <div class="row">
                                    <div class="col-4">
                                            <img src="assets/images/iklanH1.jpg" width="100%" height="120px">
                                            <center><p>Iklan 1</p></center>
                                        </div>
                                        <div class="col-4">
                                            <img src="assets/images/iklanH2.jpg" width="100%" height="120px">
                                            <center><p>Iklan 2</p></center>
                                        </div>
                                        <div class="col-4">
                                            <img src="assets/images/iklanH3.jpg" width="100%" height="120px">
                                            <center><p>Iklan 3</p></center>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </br>
                            <div class="card">
                            <div class="container-fluid"><br/><h3>Iklan Khusus Anggota</h3><hr/></div>
                                <div class="card-body">
                                    <div class="row">
                                    <div class="col-4">
                                            <img src="assets/images/iklanH1.jpg" width="100%" height="120px">
                                            <center><p>Iklan 1</p></center>
                                        </div>
                                        <div class="col-4">
                                            <img src="assets/images/iklanH2.jpg" width="100%" height="120px">
                                            <center><p>Iklan 2</p></center>
                                        </div>
                                        <div class="col-4">
                                            <img src="assets/images/iklanH3.jpg" width="100%" height="120px">
                                            <center><p>Iklan 3</p></center>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </br>
                            <div class="card">
                            <div class="container-fluid"><br/><h3>Iklan untuk User</h3><hr/></div>
                                <div class="card-body">
                                    <div class="row">
                                    <div class="col-4">
                                            <img src="assets/images/iklanH1.jpg" width="100%" height="120px">
                                            <center><p>Iklan 1</p></center>
                                        </div>
                                        <div class="col-4">
                                            <img src="assets/images/iklanH2.jpg" width="100%" height="120px">
                                            <center><p>Iklan 2</p></center>
                                        </div>
                                        <div class="col-4">
                                            <img src="assets/images/iklanH3.jpg" width="100%" height="120px">
                                            <center><p>Iklan 3</p></center>
                                        </div>
                                    </div>
                                </div>
                            </div>

                    </div>
                    <!-- /panel dashboard -->

                     <!-- panel toko -->
                     <div id="toko" class="tab-pane container-fluid fade mt-5">
                     </br>
                        <h1>List Toko</h1><hr/>
                                <form class="form-inline" action="/action_page.php">
                                    <input class="form-control mr-sm-2" type="text" placeholder="Cari Toko">
                                    <button class="btn btn-success" type="submit">Search</button>
                                </form>
                            </br>
                                <table class="table table-dark">
                                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Toko</th>
                                        <th>ID Toko</th>
                                        <th>Nama Pemilik Toko</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php

                               
                                for($i=1;$i<=20;$i++){?>
                                    <tr>
                                        <td><?php echo($i); ?> </td>
                                        <td>Toko Busana Aqila</td>
                                        <td>000011</td>
                                        <td>Aqila</td>
                                        <td> Detail | Hapus Toko </td>
                                    </tr>
                                <?php } ?>
                                    </tbody>
                                </table>
                    </div>
                    <!-- /panel toko -->

                     <!-- panel iklan -->
                     <div id="iklan" class="tab-pane container-fluid fade mt-5">
                     </br>
                        <h1>Iklan</h1><hr/>
                        <div class="card">
                            <div class="container-fluid"><br/><h3>Edit Iklan Slideshow untuk User</h3><hr/></div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-4">
                                        <img src="assets/images/iklanH1.jpg" width="100%" height="100px">
                                            <center>
                                                <form action="#">
                                                            <div class="form-group">
                                                            <input type="file" class="form-control" id="ssUser1">
                                                            </div>
                                                </form>
                                                <a href="/Profilemerchants"> <button type="" class="btn bg-primary btn-block "><font color="white">Ganti</font></button> </a>
                                            </center>
                                        </div>
                                        <div class="col-4">
                                        <img src="assets/images/iklanH2.jpg" width="100%" height="100px">
                                            <center>
                                                <form action="#">
                                                            <div class="form-group">
                                                            <input type="file" class="form-control" id="ssUser2">
                                                            </div>
                                                </form>
                                                <a href="/Profilemerchants"> <button type="" class="btn bg-primary btn-block "><font color="white">Ganti</font></button> </a>
                                            </center>
                                        </div>
                                        <div class="col-4">
                                        <img src="assets/images/iklanH3.jpg" width="100%" height="100px">
                                            <center>
                                                <form action="#">
                                                            <div class="form-group">
                                                            <input type="file" class="form-control" id="ssUser3">
                                                            </div>
                                                </form>
                                                <a href="/Profilemerchants"> <button type="" class="btn bg-primary btn-block "><font color="white">Ganti</font></button> </a>
                                            </center>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </br>

                        <div class="card">
                            <div class="container-fluid"><br/><h3>Edit Iklan Slideshow untuk Admin</h3><hr/></div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-4">
                                        <img src="assets/images/iklanH1.jpg" width="100%" height="100px">
                                            <center>
                                                <form action="#">
                                                            <div class="form-group">
                                                            <input type="file" class="form-control" id="ssUser1">
                                                            </div>
                                                </form>
                                                <a href="/Profilemerchants"> <button type="" class="btn bg-primary btn-block "><font color="white">Ganti</font></button> </a>
                                            </center>
                                        </div>
                                        <div class="col-4">
                                        <img src="assets/images/iklanH2.jpg" width="100%" height="100px">
                                            <center>
                                                <form action="#">
                                                            <div class="form-group">
                                                            <input type="file" class="form-control" id="ssUser2">
                                                            </div>
                                                </form>
                                                <a href="/Profilemerchants"> <button type="" class="btn bg-primary btn-block "><font color="white">Ganti</font></button> </a>
                                            </center>
                                        </div>
                                        <div class="col-4">
                                        <img src="assets/images/iklanH3.jpg" width="100%" height="100px">
                                            <center>
                                                <form action="#">
                                                            <div class="form-group">
                                                            <input type="file" class="form-control" id="ssUser3">
                                                            </div>
                                                </form>
                                                <a href="/Profilemerchants"> <button type="" class="btn bg-primary btn-block "><font color="white">Ganti</font></button> </a>
                                            </center>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </br>

                        <div class="card">
                            <div class="container-fluid"><br/><h3>Edit Iklan Untuk Anggota</h3><hr/></div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-4">
                                        <img src="assets/images/iklanH1.jpg" width="100%" height="100px">
                                            <center>
                                                <form action="#">
                                                            <div class="form-group">
                                                            <input type="file" class="form-control" id="ssUser1">
                                                            </div>
                                                </form>
                                                <a href="/Profilemerchants"> <button type="" class="btn bg-primary btn-block "><font color="white">Ganti</font></button> </a>
                                            </center>
                                        </div>
                                        <div class="col-4">
                                        <img src="assets/images/iklanH2.jpg" width="100%" height="100px">
                                            <center>
                                                <form action="#">
                                                            <div class="form-group">
                                                            <input type="file" class="form-control" id="ssUser2">
                                                            </div>
                                                </form>
                                                <a href="/Profilemerchants"> <button type="" class="btn bg-primary btn-block "><font color="white">Ganti</font></button> </a>
                                            </center>
                                        </div>
                                        <div class="col-4">
                                        <img src="assets/images/iklanH3.jpg" width="100%" height="100px">
                                            <center>
                                                <form action="#">
                                                            <div class="form-group">
                                                            <input type="file" class="form-control" id="ssUser3">
                                                            </div>
                                                </form>
                                                <a href="/Profilemerchants"> <button type="" class="btn bg-primary btn-block "><font color="white">Ganti</font></button> </a>
                                            </center>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </br>
                        <div class="card">
                            <div class="container-fluid"><br/><h3>Edit Iklan Home User</h3><hr/></div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-4">
                                        <img src="assets/images/iklanH1.jpg" width="100%" height="100px">
                                            <center>
                                                <form action="#">
                                                            <div class="form-group">
                                                            <input type="file" class="form-control" id="ssUser1">
                                                            </div>
                                                </form>
                                                <a href="/Profilemerchants"> <button type="" class="btn bg-primary btn-block "><font color="white">Ganti</font></button> </a>
                                            </center>
                                        </div>
                                        <div class="col-4">
                                        <img src="assets/images/iklanH2.jpg" width="100%" height="100px">
                                            <center>
                                                <form action="#">
                                                            <div class="form-group">
                                                            <input type="file" class="form-control" id="ssUser2">
                                                            </div>
                                                </form>
                                                <a href="/Profilemerchants"> <button type="" class="btn bg-primary btn-block "><font color="white">Ganti</font></button> </a>
                                            </center>
                                        </div>
                                        <div class="col-4">
                                        <img src="assets/images/iklanH3.jpg" width="100%" height="100px">
                                            <center>
                                                <form action="#">
                                                            <div class="form-group">
                                                            <input type="file" class="form-control" id="ssUser3">
                                                            </div>
                                                </form>
                                                <a href="/Profilemerchants"> <button type="" class="btn bg-primary btn-block "><font color="white">Ganti</font></button> </a>
                                            </center>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </br>
                    </div>
                    <!-- /panel iklan -->

                     <!-- panel pengaturan -->
                     <div id="pengaturan" class="tab-pane container-fluid fade mt-5">
                     </br>
                        <h1>Pengaturan Lainnya</h1><hr/>

                            <div class="card">
                            <div class="container-fluid"><br/><h3> Edit Banner User</h3><hr/></div>
                                <div class="card-body">
                                    <div class="row">
                                    <div class="col-4">
                                        <img src="assets/images/bgheader1.jpg" width="100%" height="100px">
                                            <center>
                                                <form action="#">
                                                            <div class="form-group">
                                                            <input type="file" class="form-control" id="ssUser1">
                                                            </div>
                                                </form>
                                                <a href="/Profilemerchants"> <button type="" class="btn bg-primary btn-block "><font color="white">Ganti</font></button> </a>
                                            </center>
                                        </div>
                                        <div class="col-4">
                                        <img src="assets/images/bgheader2.jpg" width="100%" height="100px">
                                            <center>
                                                <form action="#">
                                                            <div class="form-group">
                                                            <input type="file" class="form-control" id="ssUser2">
                                                            </div>
                                                </form>
                                                <a href="/Profilemerchants"> <button type="" class="btn bg-primary btn-block "><font color="white">Ganti</font></button> </a>
                                            </center>
                                        </div>
                                        <div class="col-4">
                                        <img src="assets/images/bgheader3.jpg"" width="100%" height="100px">
                                            <center>
                                                <form action="#">
                                                            <div class="form-group">
                                                            <input type="file" class="form-control" id="ssUser3">
                                                            </div>
                                                </form>
                                                <a href="/Profilemerchants"> <button type="" class="btn bg-primary btn-block "><font color="white">Ganti</font></button> </a>
                                            </center>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </br>

                        <div class="card">
                            <div class="container-fluid"><br/><h3> Edit Profile Admin</h3><hr/></div>
                                <div class="card-body">
                                    <div class="container-fluid">
                                        <form action="#">
                                            <div class="form-group">
                                                <label>Foto Profile : </label></br>
                                                <img src='assets/images/admin.png' width="100px" height="100px"> <input type="file" class="form-control-file border mr-2" id="fotoProfile">
                                            </div>
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Username" id="email">
                                            </div>
                                            <div class="form-group">
                                                <input type="password" class="form-control" placeholder="Password" id="pwd">
                                            </div>
                                            </div>
                                            <button type="submit" class="btn btn-primary btn-block">Submit</button></br>
                                        </form>

                                    </div>
                                </div>
                            </div>
                        </br>

                    </div>
                    <!-- /panel pengaturan -->
                </div>



</br>



  <center><p>Kisah Kreatif | Copyright 2020 © All Rights Reserved</p></center>
</body>
</html>


