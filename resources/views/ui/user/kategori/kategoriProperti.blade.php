@extends('/ui/user/layout/baseUser')
@section('title', 'KATEGORI | Properti | Traddee.com')
@section('content')


    <div class="boxKategoriProperti">
      <div class="textKategori">
        Properti
      </div>
    </div>  

</br>
</br>
<div class="boxmainkt">
</br>
</br>

  <!-- kategori lainnya -->
  <div class="container-fluid">
      <h3>Kategori Lainnya</h3>
    <div class="card">
      <div class="card-body">
      <div class="container-fluid">
            <div class="row">
              
            <div class="col-3">
              <center>
                <a href="Makanan" >
                <img src = 'assets/icon/013-shop.png' class="imgKT"></br>
                <div class="txtKT">Makanan</div>
                </a>
              </center>
              </div>

              <div class="col-3">
              <center>
              <a href="Produsen" >
                <img src = 'assets/icon/013-shop.png' class="imgKT"></br>
                <div class="txtKT">Produsen</div>
                </a>
              </center>
              </div>

              <div class="col-3">
              <center>
              <a href="Store" >
                <img src = 'assets/icon/013-shop.png' class="imgKT"></br>
                <div class="txtKT">Store</div>
                </a>
              </center>
              </div>

              <div class="col-3">
              <center>
              <a href="Wisata" >
                <img src = 'assets/icon/013-shop.png' class="imgKT"></br>
                <div class="txtKT">Wisata dan Inap</div>
                </a>
              </center>
              </div>

              </br>
              </br>

              <div class="col-3">
              <center>
              <a href="Barang" >
                <img src = 'assets/icon/013-shop.png' class="imgKT"></br>
                <div class="txtKT">Barang</div>
                </a>
              </center>
              </div>

              <div class="col-3">
              <center>
              <a href="Jasa" >
                <img src = 'assets/icon/013-shop.png' class="imgKT"></br>
                <div class="txtKT">Jasa</div>
                </a>
              </center>
              </div>

              <div class="col-3">
              <center>
              <a href="Properti" >
                <img src = 'assets/icon/013-shop.png' class="imgKT"></br>
                <div class="txtKT"><strong>Properti</strong></div>
                </a>
              </center>
              </div>

              <div class="col-3">
              <center>
              <a href="Hasilbumi" >
                <img src = 'assets/icon/013-shop.png' class="imgKT"></br>
                <div class="txtKT">Hasil Bumi</div>
                </a>
              </center>
              </div>

            </div>
          </div>
      </div>
    </div>
    </div>
  <!-- / ketogori lainnya --> 
</br>
</br>
<!-- sub katergori -->

   <!-- Nav sub kategori -->
  <div class="container-fluid">
  <h2>Sub Kategori</h2>

  <br>
 
  
    <ul class="nav nav-pills">
      
      

        <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#perumahan"><div class="txtSKT" >Perumahan</div></a>
          
        </li>
        
         
     
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#kavling"><div class="txtSKT" >Kavling Tanah</div></a>
          </li>
      
         
       
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#jualtanah"><div class="txtSKT" >Jual Tanah</div></a>
          </li>
       
          
      
        <li class="nav-item">
              <a class="nav-link" data-toggle="tab" href="#jualkebun"><div class="txtSKT" >Jual Kebun</div></a>
            </li>
       
          
      
    </ul>
    <hr />
    <!-- panel sub kategori -->
    <div class="tab-content">
      <!-- perumahan -->
      <div id="perumahan" class="container-fluid tab-pane active"><br>
      <div class="row">
          <div class="col-3">
          <h3 class="srtKTU"><strong>Sorting</strong></h3><hr/>
 

              <div class="srtKTSU" ><strong>Lokasi</strong></div>
              <form><label class="srtKT">Provinsi</label>
                <select name="provinsi" class="custom-select">
                  <option selected class="srtSKT"> Jawa Barat</option>
                  <option class="srtSKT">Jawa Tengah</option>
                  <option class="srtSKT">Jawa Timur</option>
                </select>
            </form>

            <form><label class="srtKT">Kota</label>
                <select name="kota" class="custom-select">
                  <option selected class="srtSKT">Kota Bekasi</option>
                  <option class="srtSKT">Kab Bekasi</option>
                  <option class="srtSKT">Kota Bandung</option>
                </select>
            </form>

              <form><label class="srtKT">Kecamatan</label>
                <select name="kecamatan" class="custom-select">
                  <option selected class="srtSKT">Bekasi Timur</option>
                  <option class="srtSKT">Mustika Jaya</option>
                  <option class="srtSKT">Pekayon</option>
                </select>
            </form>
</br>

              <div class="srtKT" ><strong>Berdasarkan Huruf</strong></div>
                  <div class="form-check">
                    <label class="form-check-label">
                      <input type="radio" class="form-check-input" value="">A-H
                    </label>
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">
                      <input type="radio" class="form-check-input" value="">I-Z
                    </label>
                  </div>
              </div>
          <div class="col-9">

                <div class="row">
                    <div class="col-3">
                    <center>
                      <img src = 'assets/images/iklan1.jpg'  id="imgTokoKT" class="btn btn-light" data-toggle="modal" data-target="#myModalbarang"  ></br>
                      <p class="txtTokoKT"> Toko A</p>
                    </center>
                    </div>

                    <div class="col-3">
                    <center>
                      <img src = 'assets/images/iklan1.jpg' id="imgTokoKT" class="btn btn-light" data-toggle="modal" data-target="#myModalbarang" ></br>
                      <p class="txtTokoKT"> Toko A</p>
                    </center>
                    </div>

                    <div class="col-3">
                    <center>
                      <img src = 'assets/images/iklan1.jpg'  id="imgTokoKT" class="btn btn-light" data-toggle="modal" data-target="#myModalbarang" ></br>
                      <p class="txtTokoKT"> Toko A</p>
                    </center>
                    </div>

                    <div class="col-3">
                    <center>
                      <img src = 'assets/images/iklan1.jpg'  id="imgTokoKT" class="btn btn-light" data-toggle="modal" data-target="#myModalbarang" ></br>
                      <p class="txtTokoKT"> Toko A</p>
                    </center>
                    </div>
                </div>
          </div>
        </div>
      </div>
      <!-- /peruamahan-->

      <!-- kavling -->
      <div id="kavling" class="container-fluid tab-pane fade"><br>
      <div class="row">
          <div class="col-3">
          <h3 class="srtKTU"><strong>Sorting</strong></h3><hr/>
 

              <div class="srtKTSU" ><strong>Lokasi</strong></div>
              <form><label class="srtKT">Provinsi</label>
                <select name="provinsi" class="custom-select">
                  <option selected class="srtSKT"> Jawa Barat</option>
                  <option class="srtSKT">Jawa Tengah</option>
                  <option class="srtSKT">Jawa Timur</option>
                </select>
            </form>

            <form><label class="srtKT">Kota</label>
                <select name="kota" class="custom-select">
                  <option selected class="srtSKT">Kota Bekasi</option>
                  <option class="srtSKT">Kab Bekasi</option>
                  <option class="srtSKT">Kota Bandung</option>
                </select>
            </form>

              <form><label class="srtKT">Kecamatan</label>
                <select name="kecamatan" class="custom-select">
                  <option selected class="srtSKT">Bekasi Timur</option>
                  <option class="srtSKT">Mustika Jaya</option>
                  <option class="srtSKT">Pekayon</option>
                </select>
            </form>
</br>

              <div class="srtKT" ><strong>Berdasarkan Huruf</strong></div>
                  <div class="form-check">
                    <label class="form-check-label">
                      <input type="radio" class="form-check-input" value="">A-H
                    </label>
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">
                      <input type="radio" class="form-check-input" value="">I-Z
                    </label>
                  </div>
              </div>
          <div class="col-9">

                <div class="row">
                    <div class="col-3">
                    <center>
                      <img src = 'assets/images/iklan1.jpg'  id="imgTokoKT" class="btn btn-light" data-toggle="modal" data-target="#myModalbarang"  ></br>
                      <p class="txtTokoKT"> Toko A</p>
                    </center>
                    </div>

                    <div class="col-3">
                    <center>
                      <img src = 'assets/images/iklan1.jpg' id="imgTokoKT" class="btn btn-light" data-toggle="modal" data-target="#myModalbarang" ></br>
                      <p class="txtTokoKT"> Toko A</p>
                    </center>
                    </div>

                    <div class="col-3">
                    <center>
                      <img src = 'assets/images/iklan1.jpg'  id="imgTokoKT" class="btn btn-light" data-toggle="modal" data-target="#myModalbarang" ></br>
                      <p class="txtTokoKT"> Toko A</p>
                    </center>
                    </div>

                    <div class="col-3">
                    <center>
                      <img src = 'assets/images/iklan1.jpg'  id="imgTokoKT" class="btn btn-light" data-toggle="modal" data-target="#myModalbarang" ></br>
                      <p class="txtTokoKT"> Toko A</p>
                    </center>
                    </div>
                </div>
          </div>
        </div>
      </div>
      <!-- /kavling -->

      <!-- Jual Tanah-->
      <div id="jualtanah" class="container-fluid tab-pane fade"><br>
      <div class="row">
          <div class="col-3">
          <h3 class="srtKTU"><strong>Sorting</strong></h3><hr/>
 

              <div class="srtKTSU" ><strong>Lokasi</strong></div>
              <form><label class="srtKT">Provinsi</label>
                <select name="provinsi" class="custom-select">
                  <option selected class="srtSKT"> Jawa Barat</option>
                  <option class="srtSKT">Jawa Tengah</option>
                  <option class="srtSKT">Jawa Timur</option>
                </select>
            </form>

            <form><label class="srtKT">Kota</label>
                <select name="kota" class="custom-select">
                  <option selected class="srtSKT">Kota Bekasi</option>
                  <option class="srtSKT">Kab Bekasi</option>
                  <option class="srtSKT">Kota Bandung</option>
                </select>
            </form>

              <form><label class="srtKT">Kecamatan</label>
                <select name="kecamatan" class="custom-select">
                  <option selected class="srtSKT">Bekasi Timur</option>
                  <option class="srtSKT">Mustika Jaya</option>
                  <option class="srtSKT">Pekayon</option>
                </select>
            </form>
</br>

              <div class="srtKT" ><strong>Berdasarkan Huruf</strong></div>
                  <div class="form-check">
                    <label class="form-check-label">
                      <input type="radio" class="form-check-input" value="">A-H
                    </label>
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">
                      <input type="radio" class="form-check-input" value="">I-Z
                    </label>
                  </div>
              </div>
          <div class="col-9">

                <div class="row">
                    <div class="col-3">
                    <center>
                      <img src = 'assets/images/iklan1.jpg'  id="imgTokoKT" class="btn btn-light" data-toggle="modal" data-target="#myModalbarang"  ></br>
                      <p class="txtTokoKT"> Toko A</p>
                    </center>
                    </div>

                    <div class="col-3">
                    <center>
                      <img src = 'assets/images/iklan1.jpg' id="imgTokoKT" class="btn btn-light" data-toggle="modal" data-target="#myModalbarang" ></br>
                      <p class="txtTokoKT"> Toko A</p>
                    </center>
                    </div>

                    <div class="col-3">
                    <center>
                      <img src = 'assets/images/iklan1.jpg'  id="imgTokoKT" class="btn btn-light" data-toggle="modal" data-target="#myModalbarang" ></br>
                      <p class="txtTokoKT"> Toko A</p>
                    </center>
                    </div>

                    <div class="col-3">
                    <center>
                      <img src = 'assets/images/iklan1.jpg'  id="imgTokoKT" class="btn btn-light" data-toggle="modal" data-target="#myModalbarang" ></br>
                      <p class="txtTokoKT"> Toko A</p>
                    </center>
                    </div>
                </div>
          </div>
        </div>
      </div>
      <!-- /jual tanah -->

      <!-- jual kebun-->
      <div id="jualkebun" class="container-fluid tab-pane fade"><br>
      <div class="row">
          <div class="col-3">
          <h3 class="srtKTU"><strong>Sorting</strong></h3><hr/>
 

              <div class="srtKTSU" ><strong>Lokasi</strong></div>
              <form><label class="srtKT">Provinsi</label>
                <select name="provinsi" class="custom-select">
                  <option selected class="srtSKT"> Jawa Barat</option>
                  <option class="srtSKT">Jawa Tengah</option>
                  <option class="srtSKT">Jawa Timur</option>
                </select>
            </form>

            <form><label class="srtKT">Kota</label>
                <select name="kota" class="custom-select">
                  <option selected class="srtSKT">Kota Bekasi</option>
                  <option class="srtSKT">Kab Bekasi</option>
                  <option class="srtSKT">Kota Bandung</option>
                </select>
            </form>

              <form><label class="srtKT">Kecamatan</label>
                <select name="kecamatan" class="custom-select">
                  <option selected class="srtSKT">Bekasi Timur</option>
                  <option class="srtSKT">Mustika Jaya</option>
                  <option class="srtSKT">Pekayon</option>
                </select>
            </form>
</br>

              <div class="srtKT" ><strong>Berdasarkan Huruf</strong></div>
                  <div class="form-check">
                    <label class="form-check-label">
                      <input type="radio" class="form-check-input" value="">A-H
                    </label>
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">
                      <input type="radio" class="form-check-input" value="">I-Z
                    </label>
                  </div>
              </div>
          <div class="col-9">

                <div class="row">
                    <div class="col-3">
                    <center>
                      <img src = 'assets/images/iklan1.jpg'  id="imgTokoKT" class="btn btn-light" data-toggle="modal" data-target="#myModalbarang"  ></br>
                      <p class="txtTokoKT"> Toko A</p>
                    </center>
                    </div>

                    <div class="col-3">
                    <center>
                      <img src = 'assets/images/iklan1.jpg' id="imgTokoKT" class="btn btn-light" data-toggle="modal" data-target="#myModalbarang" ></br>
                      <p class="txtTokoKT"> Toko A</p>
                    </center>
                    </div>

                    <div class="col-3">
                    <center>
                      <img src = 'assets/images/iklan1.jpg'  id="imgTokoKT" class="btn btn-light" data-toggle="modal" data-target="#myModalbarang" ></br>
                      <p class="txtTokoKT"> Toko A</p>
                    </center>
                    </div>

                    <div class="col-3">
                    <center>
                      <img src = 'assets/images/iklan1.jpg'  id="imgTokoKT" class="btn btn-light" data-toggle="modal" data-target="#myModalbarang" ></br>
                      <p class="txtTokoKT"> Toko A</p>
                    </center>
                    </div>
                </div>
          </div>
        </div>
      </div>
      <!-- /jual kebun-->



    </div>
    <!-- panel sub kategori -->
</div>
<!-- / nav sub kategori -->
<!-- / sub kategori -->
</br>
</br>
</div>
</br>
</br>
</br>
</br>



@endsection