@extends('/ui/user/layout/baseUser')
@section('title', 'KATEGORI | Makanan | Traddee.com')
@section('content')


    <div class="boxKategoriMakanan">
      <div class="textKategori">
        MAKANAN
      </div>
    </div>  

</br>
</br>
<div class="boxmainkt">
</br>
</br>

  <!-- kategori lainnya -->
  <div class="container-fluid">
      <h3 class="txtKTU">Kategori Lainnya</h3></br>  
    <div class="card">
      <div class="card-body">
      <div class="container-fluid">
            <div class="row">
              
            <div class="col-3">
              <center>
                <a href="Makanan" >
                <img src = 'assets/icon/013-shop.png' class="imgKT"></br>
                <div class="txtKT"><strong>Makanan</strong></div>
                </a>
              </center>
              </div>

              <div class="col-3">
              <center>
              <a href="Produsen" >
                <img src = 'assets/icon/013-shop.png' class="imgKT"></br>
                <div class="txtKT">Produsen</div>
                </a>
              </center>
              </div>

              <div class="col-3">
              <center>
              <a href="Store" >
                <img src = 'assets/icon/013-shop.png' class="imgKT"></br>
                <div class="txtKT">Store</div>
                </a>
              </center>
              </div>

              <div class="col-3">
              <center>
              <a href="Wisata" >
                <img src = 'assets/icon/013-shop.png' class="imgKT"></br>
                <div class="txtKT">Wisata dan Inap</div>
                </a>
              </center>
              </div>

              </br>
              </br>

              <div class="col-3">
              <center>
              <a href="Barang" >
                <img src = 'assets/icon/013-shop.png' class="imgKT"></br>
                <div class="txtKT">Barang</div>
                </a>
              </center>
              </div>

              <div class="col-3">
              <center>
              <a href="Jasa" >
                <img src = 'assets/icon/013-shop.png' class="imgKT"></br>
                <div class="txtKT">Jasa</div>
                </a>
              </center>
              </div>

              <div class="col-3">
              <center>
              <a href="Properti" >
                <img src = 'assets/icon/013-shop.png' class="imgKT"></br>
                <div class="txtKT">Properti</div>
                </a>
              </center>
              </div>

              <div class="col-3">
              <center>
              <a href="Hasilbumi" >
                <img src = 'assets/icon/013-shop.png' class="imgKT"></br>
                <div class="txtKT">Hasil Bumi</div>
                </a>
              </center>
              </div>

            </div>
          </div>
      </div>
    </div>
    </div>
  <!-- / ketogori lainnya --> 
</br>
<!-- sub katergori -->

   <!-- Nav sub kategori -->
  <div class="container-fluid">
  <h2 class="txtKTU">Sub Kategori</h2>

  <br>
 
  
    <ul class="nav nav-pills">
      
      

        <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#rumahmakan"><div class="txtSKT" >Rumah Makan</div></a>
          
        </li>
        
         
     
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#restoran"><div class="txtSKT" >Restoran</div></a>
        </li>
      
         
       
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#warteg"><div class="txtSKT" >Warteg</div></a>
        </li>
       
          
      
        <li class="nav-item">
              <a class="nav-link" data-toggle="tab" href="#jajanankeliling"><div class="txtSKT" >Jajanan Keliling</div></a>
        </li>
       
         
     
        <li class="nav-item">
              <a class="nav-link" data-toggle="tab" href="#oleh"><div class="txtSKT" >Oleh-Oleh</div></a>
        </li>
    
        
      
        <li class="nav-item">
              <a class="nav-link" data-toggle="tab" href="#jajananpasar"><div class="txtSKT" >Jajanan Pasar</div></a>
        </li>
        
        <li class="nav-item">
              <a class="nav-link" data-toggle="tab" href="#angkringan"><div class="txtSKT" >Angkringan</div></a>
        </li>
    
          
      
    </ul>
    <hr />
    <!-- panel sub kategori -->
    <div class="tab-content">
      <!-- rumah makan -->
      <div id="rumahmakan" class="container-fluid tab-pane active"><br>
        <div class="row">
          <div class="col-3">
          <h3 class="srtKTU"><strong>Sorting</strong></h3><hr/>
 

              <div class="srtKTSU" ><strong>Lokasi</strong></div>
              <form><label class="srtKT">Provinsi</label>
                <select name="provinsi" class="custom-select">
                  <option selected class="srtSKT"> Jawa Barat</option>
                  <option class="srtSKT">Jawa Tengah</option>
                  <option class="srtSKT">Jawa Timur</option>
                </select>
            </form>

            <form><label class="srtKT">Kota</label>
                <select name="kota" class="custom-select">
                  <option selected class="srtSKT">Kota Bekasi</option>
                  <option class="srtSKT">Kab Bekasi</option>
                  <option class="srtSKT">Kota Bandung</option>
                </select>
            </form>

              <form><label class="srtKT">Kecamatan</label>
                <select name="kecamatan" class="custom-select">
                  <option selected class="srtSKT">Bekasi Timur</option>
                  <option class="srtSKT">Mustika Jaya</option>
                  <option class="srtSKT">Pekayon</option>
                </select>
            </form>
</br>

              <div class="srtKT" ><strong>Berdasarkan Huruf</strong></div>
                  <div class="form-check">
                    <label class="form-check-label">
                      <input type="radio" class="form-check-input" value="">A-H
                    </label>
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">
                      <input type="radio" class="form-check-input" value="">I-Z
                    </label>
                  </div>
              </div>
          <div class="col-9">

                <div class="row">
                    <div class="col-3">
                    <center>
                      <img src = 'assets/images/iklan1.jpg'  id="imgTokoKT" class="btn btn-light" data-toggle="modal" data-target="#myModalbarang"  ></br>
                      <p class="txtTokoKT"> Toko A</p>
                    </center>
                    </div>

                    <div class="col-3">
                    <center>
                      <img src = 'assets/images/iklan1.jpg' id="imgTokoKT" class="btn btn-light" data-toggle="modal" data-target="#myModalbarang" ></br>
                      <p class="txtTokoKT"> Toko A</p>
                    </center>
                    </div>

                    <div class="col-3">
                    <center>
                      <img src = 'assets/images/iklan1.jpg'  id="imgTokoKT" class="btn btn-light" data-toggle="modal" data-target="#myModalbarang" ></br>
                      <p class="txtTokoKT"> Toko A</p>
                    </center>
                    </div>

                    <div class="col-3">
                    <center>
                      <img src = 'assets/images/iklan1.jpg'  id="imgTokoKT" class="btn btn-light" data-toggle="modal" data-target="#myModalbarang" ></br>
                      <p class="txtTokoKT"> Toko A</p>
                    </center>
                    </div>
                </div>
          </div>
        </div>
      </div>
      <!-- /rumah makan -->

      <!-- restoran -->
      <div id="restoran" class="container-fluid tab-pane fade"><br>
      <div class="row">
          <div class="col-3">
          <h3 class="srtKTU"><strong>Sorting</strong></h3><hr/>
 

              <div class="srtKTSU" ><strong>Lokasi</strong></div>
              <form><label class="srtKT">Provinsi</label>
                <select name="provinsi" class="custom-select">
                  <option selected class="srtSKT"> Jawa Barat</option>
                  <option class="srtSKT">Jawa Tengah</option>
                  <option class="srtSKT">Jawa Timur</option>
                </select>
            </form>

            <form><label class="srtKT">Kota</label>
                <select name="kota" class="custom-select">
                  <option selected class="srtSKT">Kota Bekasi</option>
                  <option class="srtSKT">Kab Bekasi</option>
                  <option class="srtSKT">Kota Bandung</option>
                </select>
            </form>

              <form><label class="srtKT">Kecamatan</label>
                <select name="kecamatan" class="custom-select">
                  <option selected class="srtSKT">Bekasi Timur</option>
                  <option class="srtSKT">Mustika Jaya</option>
                  <option class="srtSKT">Pekayon</option>
                </select>
            </form>
</br>

              <div class="srtKT" ><strong>Berdasarkan Huruf</strong></div>
                  <div class="form-check">
                    <label class="form-check-label">
                      <input type="radio" class="form-check-input" value="">A-H
                    </label>
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">
                      <input type="radio" class="form-check-input" value="">I-Z
                    </label>
                  </div>
              </div>
          <div class="col-9">

                <div class="row">
                    <div class="col-3">
                    <center>
                      <img src = 'assets/images/iklan1.jpg'  id="imgTokoKT" class="btn btn-light" data-toggle="modal" data-target="#myModalbarang"  ></br>
                      <p class="txtTokoKT"> Toko A</p>
                    </center>
                    </div>

                    <div class="col-3">
                    <center>
                      <img src = 'assets/images/iklan1.jpg' id="imgTokoKT" class="btn btn-light" data-toggle="modal" data-target="#myModalbarang" ></br>
                      <p class="txtTokoKT"> Toko A</p>
                    </center>
                    </div>

                    <div class="col-3">
                    <center>
                      <img src = 'assets/images/iklan1.jpg'  id="imgTokoKT" class="btn btn-light" data-toggle="modal" data-target="#myModalbarang" ></br>
                      <p class="txtTokoKT"> Toko A</p>
                    </center>
                    </div>

                    <div class="col-3">
                    <center>
                      <img src = 'assets/images/iklan1.jpg'  id="imgTokoKT" class="btn btn-light" data-toggle="modal" data-target="#myModalbarang" ></br>
                      <p class="txtTokoKT"> Toko A</p>
                    </center>
                    </div>
                </div>
          </div>
        </div>
      </div>
      <!-- /restoran -->

      <!-- warteg -->
      <div id="warteg" class="container-fluid tab-pane fade"><br>
      <div class="row">
          <div class="col-3">
          <h3 class="srtKTU"><strong>Sorting</strong></h3><hr/>
 

              <div class="srtKTSU" ><strong>Lokasi</strong></div>
              <form><label class="srtKT">Provinsi</label>
                <select name="provinsi" class="custom-select">
                  <option selected class="srtSKT"> Jawa Barat</option>
                  <option class="srtSKT">Jawa Tengah</option>
                  <option class="srtSKT">Jawa Timur</option>
                </select>
            </form>

            <form><label class="srtKT">Kota</label>
                <select name="kota" class="custom-select">
                  <option selected class="srtSKT">Kota Bekasi</option>
                  <option class="srtSKT">Kab Bekasi</option>
                  <option class="srtSKT">Kota Bandung</option>
                </select>
            </form>

              <form><label class="srtKT">Kecamatan</label>
                <select name="kecamatan" class="custom-select">
                  <option selected class="srtSKT">Bekasi Timur</option>
                  <option class="srtSKT">Mustika Jaya</option>
                  <option class="srtSKT">Pekayon</option>
                </select>
            </form>
</br>

              <div class="srtKT" ><strong>Berdasarkan Huruf</strong></div>
                  <div class="form-check">
                    <label class="form-check-label">
                      <input type="radio" class="form-check-input" value="">A-H
                    </label>
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">
                      <input type="radio" class="form-check-input" value="">I-Z
                    </label>
                  </div>
              </div>
          <div class="col-9">

                <div class="row">
                    <div class="col-3">
                    <center>
                      <img src = 'assets/images/iklan1.jpg'  id="imgTokoKT" class="btn btn-light" data-toggle="modal" data-target="#myModalbarang"  ></br>
                      <p class="txtTokoKT"> Toko A</p>
                    </center>
                    </div>

                    <div class="col-3">
                    <center>
                      <img src = 'assets/images/iklan1.jpg' id="imgTokoKT" class="btn btn-light" data-toggle="modal" data-target="#myModalbarang" ></br>
                      <p class="txtTokoKT"> Toko A</p>
                    </center>
                    </div>

                    <div class="col-3">
                    <center>
                      <img src = 'assets/images/iklan1.jpg'  id="imgTokoKT" class="btn btn-light" data-toggle="modal" data-target="#myModalbarang" ></br>
                      <p class="txtTokoKT"> Toko A</p>
                    </center>
                    </div>

                    <div class="col-3">
                    <center>
                      <img src = 'assets/images/iklan1.jpg'  id="imgTokoKT" class="btn btn-light" data-toggle="modal" data-target="#myModalbarang" ></br>
                      <p class="txtTokoKT"> Toko A</p>
                    </center>
                    </div>
                </div>
          </div>
        </div>
      </div>
      <!-- /warteg -->

      <!-- jajanan keliling -->
      <div id="jajanankeliling" class="container-fluid tab-pane fade"><br>
      <div class="row">
          <div class="col-3">
          <h3 class="srtKTU"><strong>Sorting</strong></h3><hr/>
 

              <div class="srtKTSU" ><strong>Lokasi</strong></div>
              <form><label class="srtKT">Provinsi</label>
                <select name="provinsi" class="custom-select">
                  <option selected class="srtSKT"> Jawa Barat</option>
                  <option class="srtSKT">Jawa Tengah</option>
                  <option class="srtSKT">Jawa Timur</option>
                </select>
            </form>

            <form><label class="srtKT">Kota</label>
                <select name="kota" class="custom-select">
                  <option selected class="srtSKT">Kota Bekasi</option>
                  <option class="srtSKT">Kab Bekasi</option>
                  <option class="srtSKT">Kota Bandung</option>
                </select>
            </form>

              <form><label class="srtKT">Kecamatan</label>
                <select name="kecamatan" class="custom-select">
                  <option selected class="srtSKT">Bekasi Timur</option>
                  <option class="srtSKT">Mustika Jaya</option>
                  <option class="srtSKT">Pekayon</option>
                </select>
            </form>
</br>

              <div class="srtKT" ><strong>Berdasarkan Huruf</strong></div>
                  <div class="form-check">
                    <label class="form-check-label">
                      <input type="radio" class="form-check-input" value="">A-H
                    </label>
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">
                      <input type="radio" class="form-check-input" value="">I-Z
                    </label>
                  </div>
              </div>
          <div class="col-9">

                <div class="row">
                    <div class="col-3">
                    <center>
                      <img src = 'assets/images/iklan1.jpg'  id="imgTokoKT" class="btn btn-light" data-toggle="modal" data-target="#myModalbarang"  ></br>
                      <p class="txtTokoKT"> Toko A</p>
                    </center>
                    </div>

                    <div class="col-3">
                    <center>
                      <img src = 'assets/images/iklan1.jpg' id="imgTokoKT" class="btn btn-light" data-toggle="modal" data-target="#myModalbarang" ></br>
                      <p class="txtTokoKT"> Toko A</p>
                    </center>
                    </div>

                    <div class="col-3">
                    <center>
                      <img src = 'assets/images/iklan1.jpg'  id="imgTokoKT" class="btn btn-light" data-toggle="modal" data-target="#myModalbarang" ></br>
                      <p class="txtTokoKT"> Toko A</p>
                    </center>
                    </div>

                    <div class="col-3">
                    <center>
                      <img src = 'assets/images/iklan1.jpg'  id="imgTokoKT" class="btn btn-light" data-toggle="modal" data-target="#myModalbarang" ></br>
                      <p class="txtTokoKT"> Toko A</p>
                    </center>
                    </div>
                </div>
          </div>
        </div>
      </div>
      <!-- /jajanan keliling -->

      
      <!-- angkringan -->
      <div id="angkringan" class="container-fluid tab-pane fade"><br>
      <div class="row">
          <div class="col-3">
          <h3 class="srtKTU"><strong>Sorting</strong></h3><hr/>
 

              <div class="srtKTSU" ><strong>Lokasi</strong></div>
              <form><label class="srtKT">Provinsi</label>
                <select name="provinsi" class="custom-select">
                  <option selected class="srtSKT"> Jawa Barat</option>
                  <option class="srtSKT">Jawa Tengah</option>
                  <option class="srtSKT">Jawa Timur</option>
                </select>
            </form>

            <form><label class="srtKT">Kota</label>
                <select name="kota" class="custom-select">
                  <option selected class="srtSKT">Kota Bekasi</option>
                  <option class="srtSKT">Kab Bekasi</option>
                  <option class="srtSKT">Kota Bandung</option>
                </select>
            </form>

              <form><label class="srtKT">Kecamatan</label>
                <select name="kecamatan" class="custom-select">
                  <option selected class="srtSKT">Bekasi Timur</option>
                  <option class="srtSKT">Mustika Jaya</option>
                  <option class="srtSKT">Pekayon</option>
                </select>
            </form>
</br>

              <div class="srtKT" ><strong>Berdasarkan Huruf</strong></div>
                  <div class="form-check">
                    <label class="form-check-label">
                      <input type="radio" class="form-check-input" value="">A-H
                    </label>
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">
                      <input type="radio" class="form-check-input" value="">I-Z
                    </label>
                  </div>
              </div>
          <div class="col-9">

                <div class="row">
                    <div class="col-3">
                    <center>
                      <img src = 'assets/images/iklan1.jpg'  id="imgTokoKT" class="btn btn-light" data-toggle="modal" data-target="#myModalbarang"  ></br>
                      <p class="txtTokoKT"> Toko A</p>
                    </center>
                    </div>

                    <div class="col-3">
                    <center>
                      <img src = 'assets/images/iklan1.jpg' id="imgTokoKT" class="btn btn-light" data-toggle="modal" data-target="#myModalbarang" ></br>
                      <p class="txtTokoKT"> Toko A</p>
                    </center>
                    </div>

                    <div class="col-3">
                    <center>
                      <img src = 'assets/images/iklan1.jpg'  id="imgTokoKT" class="btn btn-light" data-toggle="modal" data-target="#myModalbarang" ></br>
                      <p class="txtTokoKT"> Toko A</p>
                    </center>
                    </div>

                    <div class="col-3">
                    <center>
                      <img src = 'assets/images/iklan1.jpg'  id="imgTokoKT" class="btn btn-light" data-toggle="modal" data-target="#myModalbarang" ></br>
                      <p class="txtTokoKT"> Toko A</p>
                    </center>
                    </div>
                </div>
          </div>
        </div>
      </div>
      <!-- /angkringan-->

      <!-- oleh oleh -->
      <div id="oleh" class="container-fluid tab-pane fade"><br>
      <div class="row">
          <div class="col-3">
          <h3 class="srtKTU"><strong>Sorting</strong></h3><hr/>
 

              <div class="srtKTSU" ><strong>Lokasi</strong></div>
              <form><label class="srtKT">Provinsi</label>
                <select name="provinsi" class="custom-select">
                  <option selected class="srtSKT"> Jawa Barat</option>
                  <option class="srtSKT">Jawa Tengah</option>
                  <option class="srtSKT">Jawa Timur</option>
                </select>
            </form>

            <form><label class="srtKT">Kota</label>
                <select name="kota" class="custom-select">
                  <option selected class="srtSKT">Kota Bekasi</option>
                  <option class="srtSKT">Kab Bekasi</option>
                  <option class="srtSKT">Kota Bandung</option>
                </select>
            </form>

              <form><label class="srtKT">Kecamatan</label>
                <select name="kecamatan" class="custom-select">
                  <option selected class="srtSKT">Bekasi Timur</option>
                  <option class="srtSKT">Mustika Jaya</option>
                  <option class="srtSKT">Pekayon</option>
                </select>
            </form>
</br>

              <div class="srtKT" ><strong>Berdasarkan Huruf</strong></div>
                  <div class="form-check">
                    <label class="form-check-label">
                      <input type="radio" class="form-check-input" value="">A-H
                    </label>
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">
                      <input type="radio" class="form-check-input" value="">I-Z
                    </label>
                  </div>
              </div>
          <div class="col-9">

                <div class="row">
                    <div class="col-3">
                    <center>
                      <img src = 'assets/images/iklan1.jpg'  id="imgTokoKT" class="btn btn-light" data-toggle="modal" data-target="#myModalbarang"  ></br>
                      <p class="txtTokoKT"> Toko A</p>
                    </center>
                    </div>

                    <div class="col-3">
                    <center>
                      <img src = 'assets/images/iklan1.jpg' id="imgTokoKT" class="btn btn-light" data-toggle="modal" data-target="#myModalbarang" ></br>
                      <p class="txtTokoKT"> Toko A</p>
                    </center>
                    </div>

                    <div class="col-3">
                    <center>
                      <img src = 'assets/images/iklan1.jpg'  id="imgTokoKT" class="btn btn-light" data-toggle="modal" data-target="#myModalbarang" ></br>
                      <p class="txtTokoKT"> Toko A</p>
                    </center>
                    </div>

                    <div class="col-3">
                    <center>
                      <img src = 'assets/images/iklan1.jpg'  id="imgTokoKT" class="btn btn-light" data-toggle="modal" data-target="#myModalbarang" ></br>
                      <p class="txtTokoKT"> Toko A</p>
                    </center>
                    </div>
                </div>
          </div>
        </div>
      </div>
      <!-- /oleh oleh -->

      <!-- jajanan pasar -->
      <div id="jajananpasar" class="container-fluid tab-pane fade"><br>
      <div class="row">
          <div class="col-3">
          <h3 class="srtKTU"><strong>Sorting</strong></h3><hr/>
 

              <div class="srtKTSU" ><strong>Lokasi</strong></div>
              <form><label class="srtKT">Provinsi</label>
                <select name="provinsi" class="custom-select">
                  <option selected class="srtSKT"> Jawa Barat</option>
                  <option class="srtSKT">Jawa Tengah</option>
                  <option class="srtSKT">Jawa Timur</option>
                </select>
            </form>

            <form><label class="srtKT">Kota</label>
                <select name="kota" class="custom-select">
                  <option selected class="srtSKT">Kota Bekasi</option>
                  <option class="srtSKT">Kab Bekasi</option>
                  <option class="srtSKT">Kota Bandung</option>
                </select>
            </form>

              <form><label class="srtKT">Kecamatan</label>
                <select name="kecamatan" class="custom-select">
                  <option selected class="srtSKT">Bekasi Timur</option>
                  <option class="srtSKT">Mustika Jaya</option>
                  <option class="srtSKT">Pekayon</option>
                </select>
            </form>
</br>

              <div class="srtKT" ><strong>Berdasarkan Huruf</strong></div>
                  <div class="form-check">
                    <label class="form-check-label">
                      <input type="radio" class="form-check-input" value="">A-H
                    </label>
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">
                      <input type="radio" class="form-check-input" value="">I-Z
                    </label>
                  </div>
              </div>
          <div class="col-9">

                <div class="row">
                    <div class="col-3">
                    <center>
                      <img src = 'assets/images/iklan1.jpg'  id="imgTokoKT" class="btn btn-light" data-toggle="modal" data-target="#myModalbarang"  ></br>
                      <p class="txtTokoKT"> Toko A</p>
                    </center>
                    </div>

                    <div class="col-3">
                    <center>
                      <img src = 'assets/images/iklan1.jpg' id="imgTokoKT" class="btn btn-light" data-toggle="modal" data-target="#myModalbarang" ></br>
                      <p class="txtTokoKT"> Toko A</p>
                    </center>
                    </div>

                    <div class="col-3">
                    <center>
                      <img src = 'assets/images/iklan1.jpg'  id="imgTokoKT" class="btn btn-light" data-toggle="modal" data-target="#myModalbarang" ></br>
                      <p class="txtTokoKT"> Toko A</p>
                    </center>
                    </div>

                    <div class="col-3">
                    <center>
                      <img src = 'assets/images/iklan1.jpg'  id="imgTokoKT" class="btn btn-light" data-toggle="modal" data-target="#myModalbarang" ></br>
                      <p class="txtTokoKT"> Toko A</p>
                    </center>
                    </div>
                </div>
          </div>
        </div>
      </div>
      <!-- /jajanan pasar -->

    </div>
    <!-- panel sub kategori -->
</div>
<!-- / nav sub kategori -->
<!-- / sub kategori -->
</br>
</br>
</div>
</br>
</br>
</br>
</br>



@endsection