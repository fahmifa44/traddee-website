@extends('/ui/user/layout/baseUser')
@section('title', 'KATEGORI | Minuman | Traddee.com')
@section('content')


    <div class="boxKategoriMinuman">
      <div class="textKategori">
        PRODUSEN
      </div>
    </div>  

</br>
</br>
<div class="boxmainkt">
</br>
</br>

  <!-- kategori lainnya -->
  <div class="container-fluid">
      <h3>Kategori Lainnya</h3>
    <div class="card">
      <div class="card-body">
      <div class="container-fluid">
            <div class="row">
              
            <div class="col-3">
              <center>
                <a href="Makanan" >
                <img src = 'assets/icon/013-shop.png' class="imgKT"></br>
                <div class="txtKT">Makanan</div>
                </a>
              </center>
              </div>

              <div class="col-3">
              <center>
              <a href="Produsen" >
                <img src = 'assets/icon/013-shop.png' class="imgKT"></br>
                <div class="txtKT"><strong>Produsen</strong></div>
                </a>
              </center>
              </div>

              <div class="col-3">
              <center>
              <a href="Store" >
                <img src = 'assets/icon/013-shop.png' class="imgKT"></br>
                <div class="txtKT">Store</div>
                </a>
              </center>
              </div>

              <div class="col-3">
              <center>
              <a href="Wisata" >
                <img src = 'assets/icon/013-shop.png' class="imgKT"></br>
                <div class="txtKT">Wisata dan Inap</div>
                </a>
              </center>
              </div>

              </br>
              </br>

              <div class="col-3">
              <center>
              <a href="Barang" >
                <img src = 'assets/icon/013-shop.png' class="imgKT"></br>
                <div class="txtKT">Barang</div>
                </a>
              </center>
              </div>

              <div class="col-3">
              <center>
              <a href="Jasa" >
                <img src = 'assets/icon/013-shop.png' class="imgKT"></br>
                <div class="txtKT">Jasa</div>
                </a>
              </center>
              </div>

              <div class="col-3">
              <center>
              <a href="Properti" >
                <img src = 'assets/icon/013-shop.png' class="imgKT"></br>
                <div class="txtKT">Properti</div>
                </a>
              </center>
              </div>

              <div class="col-3">
              <center>
              <a href="Hasilbumi" >
                <img src = 'assets/icon/013-shop.png' class="imgKT"></br>
                <div class="txtKT">Hasil Bumi</div>
                </a>
              </center>
              </div>

            </div>
          </div>
      </div>
    </div>
    </div>
  <!-- / ketogori lainnya --> 
</br>
</br>
<!-- sub katergori -->

   <!-- Nav sub kategori -->
  <div class="container-fluid">
  <h2>Sub Kategori</h2>

  <br>
 
  
    <ul class="nav nav-pills">
      
      

        <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#hc"><div class="txtSKT" >HandyCraft</div></a>
          
        </li>
        
         
     
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#umkm"><div class="txtSKT" >UMKM</div></a>
          </li>
      
         
       
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#lainnya"><div class="txtSKT" >Lainnya</div></a>
          </li>
       
    </ul>
    <hr />
    <!-- panel sub kategori -->
    <div class="tab-content">
      <!-- Jamu-->
      <div id="hc" class="container-fluid tab-pane active"><br>
      <div class="row">
          <div class="col-3">
          <h3 class="srtKTU"><strong>Sorting</strong></h3><hr/>
 

              <div class="srtKTSU" ><strong>Lokasi</strong></div>
              <form><label class="srtKT">Provinsi</label>
                <select name="provinsi" class="custom-select">
                  <option selected class="srtSKT"> Jawa Barat</option>
                  <option class="srtSKT">Jawa Tengah</option>
                  <option class="srtSKT">Jawa Timur</option>
                </select>
            </form>

            <form><label class="srtKT">Kota</label>
                <select name="kota" class="custom-select">
                  <option selected class="srtSKT">Kota Bekasi</option>
                  <option class="srtSKT">Kab Bekasi</option>
                  <option class="srtSKT">Kota Bandung</option>
                </select>
            </form>

              <form><label class="srtKT">Kecamatan</label>
                <select name="kecamatan" class="custom-select">
                  <option selected class="srtSKT">Bekasi Timur</option>
                  <option class="srtSKT">Mustika Jaya</option>
                  <option class="srtSKT">Pekayon</option>
                </select>
            </form>
</br>

              <div class="srtKT" ><strong>Berdasarkan Huruf</strong></div>
                  <div class="form-check">
                    <label class="form-check-label">
                      <input type="radio" class="form-check-input" value="">A-H
                    </label>
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">
                      <input type="radio" class="form-check-input" value="">I-Z
                    </label>
                  </div>
              </div>
          <div class="col-9">

                <div class="row">
                    <div class="col-3">
                    <center>
                      <img src = 'assets/images/iklan1.jpg'  id="imgTokoKT" class="btn btn-light" data-toggle="modal" data-target="#myModalbarang"  ></br>
                      <p class="txtTokoKT"> Toko A</p>
                    </center>
                    </div>

                    <div class="col-3">
                    <center>
                      <img src = 'assets/images/iklan1.jpg' id="imgTokoKT" class="btn btn-light" data-toggle="modal" data-target="#myModalbarang" ></br>
                      <p class="txtTokoKT"> Toko A</p>
                    </center>
                    </div>

                    <div class="col-3">
                    <center>
                      <img src = 'assets/images/iklan1.jpg'  id="imgTokoKT" class="btn btn-light" data-toggle="modal" data-target="#myModalbarang" ></br>
                      <p class="txtTokoKT"> Toko A</p>
                    </center>
                    </div>

                    <div class="col-3">
                    <center>
                      <img src = 'assets/images/iklan1.jpg'  id="imgTokoKT" class="btn btn-light" data-toggle="modal" data-target="#myModalbarang" ></br>
                      <p class="txtTokoKT"> Toko A</p>
                    </center>
                    </div>
                </div>
          </div>
        </div>
      </div>
      <!-- /jamu -->

      <!-- Kedai kopi -->
      <div id="umkm" class="container-fluid tab-pane fade"><br>
      <div class="row">
          <div class="col-3">
          <h3 class="srtKTU"><strong>Sorting</strong></h3><hr/>
 

              <div class="srtKTSU" ><strong>Lokasi</strong></div>
              <form><label class="srtKT">Provinsi</label>
                <select name="provinsi" class="custom-select">
                  <option selected class="srtSKT"> Jawa Barat</option>
                  <option class="srtSKT">Jawa Tengah</option>
                  <option class="srtSKT">Jawa Timur</option>
                </select>
            </form>

            <form><label class="srtKT">Kota</label>
                <select name="kota" class="custom-select">
                  <option selected class="srtSKT">Kota Bekasi</option>
                  <option class="srtSKT">Kab Bekasi</option>
                  <option class="srtSKT">Kota Bandung</option>
                </select>
            </form>

              <form><label class="srtKT">Kecamatan</label>
                <select name="kecamatan" class="custom-select">
                  <option selected class="srtSKT">Bekasi Timur</option>
                  <option class="srtSKT">Mustika Jaya</option>
                  <option class="srtSKT">Pekayon</option>
                </select>
            </form>
</br>

              <div class="srtKT" ><strong>Berdasarkan Huruf</strong></div>
                  <div class="form-check">
                    <label class="form-check-label">
                      <input type="radio" class="form-check-input" value="">A-H
                    </label>
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">
                      <input type="radio" class="form-check-input" value="">I-Z
                    </label>
                  </div>
              </div>
          <div class="col-9">

                <div class="row">
                    <div class="col-3">
                    <center>
                      <img src = 'assets/images/iklan1.jpg'  id="imgTokoKT" class="btn btn-light" data-toggle="modal" data-target="#myModalbarang"  ></br>
                      <p class="txtTokoKT"> Toko A</p>
                    </center>
                    </div>

                    <div class="col-3">
                    <center>
                      <img src = 'assets/images/iklan1.jpg' id="imgTokoKT" class="btn btn-light" data-toggle="modal" data-target="#myModalbarang" ></br>
                      <p class="txtTokoKT"> Toko A</p>
                    </center>
                    </div>

                    <div class="col-3">
                    <center>
                      <img src = 'assets/images/iklan1.jpg'  id="imgTokoKT" class="btn btn-light" data-toggle="modal" data-target="#myModalbarang" ></br>
                      <p class="txtTokoKT"> Toko A</p>
                    </center>
                    </div>

                    <div class="col-3">
                    <center>
                      <img src = 'assets/images/iklan1.jpg'  id="imgTokoKT" class="btn btn-light" data-toggle="modal" data-target="#myModalbarang" ></br>
                      <p class="txtTokoKT"> Toko A</p>
                    </center>
                    </div>
                </div>
          </div>
        </div>
      </div>
      <!-- /Kedai kopi -->

      <!-- warung kopi -->
      <div id="lainnya" class="container-fluid tab-pane fade"><br>
      <div class="row">
          <div class="col-3">
          <h3 class="srtKTU"><strong>Sorting</strong></h3><hr/>
 

              <div class="srtKTSU" ><strong>Lokasi</strong></div>
              <form><label class="srtKT">Provinsi</label>
                <select name="provinsi" class="custom-select">
                  <option selected class="srtSKT"> Jawa Barat</option>
                  <option class="srtSKT">Jawa Tengah</option>
                  <option class="srtSKT">Jawa Timur</option>
                </select>
            </form>

            <form><label class="srtKT">Kota</label>
                <select name="kota" class="custom-select">
                  <option selected class="srtSKT">Kota Bekasi</option>
                  <option class="srtSKT">Kab Bekasi</option>
                  <option class="srtSKT">Kota Bandung</option>
                </select>
            </form>

              <form><label class="srtKT">Kecamatan</label>
                <select name="kecamatan" class="custom-select">
                  <option selected class="srtSKT">Bekasi Timur</option>
                  <option class="srtSKT">Mustika Jaya</option>
                  <option class="srtSKT">Pekayon</option>
                </select>
            </form>
</br>

              <div class="srtKT" ><strong>Berdasarkan Huruf</strong></div>
                  <div class="form-check">
                    <label class="form-check-label">
                      <input type="radio" class="form-check-input" value="">A-H
                    </label>
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">
                      <input type="radio" class="form-check-input" value="">I-Z
                    </label>
                  </div>
              </div>
          <div class="col-9">

                <div class="row">
                    <div class="col-3">
                    <center>
                      <img src = 'assets/images/iklan1.jpg'  id="imgTokoKT" class="btn btn-light" data-toggle="modal" data-target="#myModalbarang"  ></br>
                      <p class="txtTokoKT"> Toko A</p>
                    </center>
                    </div>

                    <div class="col-3">
                    <center>
                      <img src = 'assets/images/iklan1.jpg' id="imgTokoKT" class="btn btn-light" data-toggle="modal" data-target="#myModalbarang" ></br>
                      <p class="txtTokoKT"> Toko A</p>
                    </center>
                    </div>

                    <div class="col-3">
                    <center>
                      <img src = 'assets/images/iklan1.jpg'  id="imgTokoKT" class="btn btn-light" data-toggle="modal" data-target="#myModalbarang" ></br>
                      <p class="txtTokoKT"> Toko A</p>
                    </center>
                    </div>

                    <div class="col-3">
                    <center>
                      <img src = 'assets/images/iklan1.jpg'  id="imgTokoKT" class="btn btn-light" data-toggle="modal" data-target="#myModalbarang" ></br>
                      <p class="txtTokoKT"> Toko A</p>
                    </center>
                    </div>
                </div>
          </div>
        </div>
      </div>
      <!-- /warrung kopi-->
    </div>
    <!-- panel sub kategori -->
</div>
<!-- / nav sub kategori -->
<!-- / sub kategori -->
</br>
</br>
</div>
</br>
</br>
</br>
</br>



@endsection