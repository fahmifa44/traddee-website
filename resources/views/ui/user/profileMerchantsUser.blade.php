@extends('/ui/user/layout/baseUser')
@section('title', 'MERCHANTS | Aqila Busana | Traddee.com')
@section('content')

<div class="boxHeaderProfileMerc">
            
        <!-- iklan slide show -->
        <div id="demo" class="carousel slide" data-ride="carousel">

          <!-- The slideshow -->
          <div class="carousel-inner">
                <div class="carousel-item active">
                  <img src = 'assets/images/aqilabg.jpg' class="imgHeaderProfileMerc">
                </div>
          </div>

        <!-- tutup iklan slide show -->

      <!-- txt Slide Show -->
     
      <div class="container-fluid">
       <div class="ProfileHeader">
         <div class="row">
           <div class="col-3">
              <img src = 'assets/images/user.png' class="imgProfileMerc">
           </div>

           <div class="col-9">
              <p class="txtProfileMerch1">TOKO BUSANAN AQILA</p>
              <p class="txtProfileMerch2">ID : 000011</p>
           </div>
         </div>
       </div>
     
      </div>
      <!-- / txt Slide Show -->
</div>  

<div class="boxMainProfileMerc">
  <div class="container-fluid">
    <div class="row">
      <div class="col-6">
          <div class="boxIklanToko">
          Iklan dari Toko
          </div>
      </div>

      <div class="col-6">
          <div class="boxIklanToko">
          Iklan dari Toko
          </div>
      </div>

    </div>
</br>
                <ul class="nav nav-pills" role="tablist">
                    <li class="nav-item">
                      <a class="nav-link active" data-toggle="tab" href="#profil">Profil Toko</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" data-toggle="tab" href="#produk">Produk</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" data-toggle="tab" href="#chat">Chat</a>
                    </li>
                  </ul>

                  <div class="tab-content">
                    <div id="profil" class="container tab-pane active"><br>
                    <p>Nama Toko : Aqila Busana</p>
                    <p>Id Toko : 000011</p>
                    <p>Alamat Toko : </p>
                    <p>Nama Pemilik Toko : Aqila</p>
                    <p>Link Iframe : </p>
                        <h3 class="txtLokMerch">Lokasi</h3>
                          <div class="container-fluid">
                            <center>
                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15862.18072672806!2d107.0185125958462!3d-6.323309853560492!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69918ff08d6a23%3A0x43afe6f00e8b0288!2sAqela%20Busana!5e0!3m2!1sen!2sid!4v1605552677186!5m2!1sen!2sid" width="100%" height="400" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                            </center>
                          </div>
                    </div>

                    <div id="produk" class="container tab-pane fade"><br>

                    <form class="form" action="/action_page.php">
                        <div class="input-group mb-3" id="Src"> 
                            <input type="text" class="form-control" placeholder="Cari di toko ini">
                            <div class="input-group-append">
                                <span class="btn bg-warning">Search</span>
                            </div>
                        </div>
                    </form>
                
                        <div class="row">
                                  
                        <div class="col-3">
                        <center>
                          <img src = 'assets/images/iklan1.jpg' id="imgTokoMerch" class="img-thumbnail btn btn-light" data-toggle="modal" data-target="#myModalbarang"  ></br>
                          <p class="txtTokoMerc"> Toko A</p>
                        </center>
                        </div>

                        <div class="col-3">
                        <center>
                          <img src = 'assets/images/iklan1.jpg' id="imgTokoMerch" class="img-thumbnail btn btn-light" data-toggle="modal" data-target="#myModalbarang" ></br>
                          <p class="txtTokoMerc"> Toko A</p>
                        </center>
                        </div>

                        <div class="col-3">
                        <center>
                          <img src = 'assets/images/iklan1.jpg'  id="imgTokoMerch" class="img-thumbnail btn btn-light" data-toggle="modal" data-target="#myModalbarang" ></br>
                          <p class="txtTokoMerc"> Toko A</p>
                        </center>
                        </div>

                        <div class="col-3">
                        <center>
                          <img src = 'assets/images/iklan1.jpg'  id="imgTokoMerch" class="img-thumbnail btn btn-light" data-toggle="modal" data-target="#myModalbarang" ></br>
                          <p class="txtTokoMerc"> Toko A</p>
                        </center>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-3">
                        <center>
                        <img src = 'assets/images/iklan1.jpg' id="imgTokoMerch" class="img-thumbnail btn btn-light" data-toggle="modal" data-target="#myModalbarang" ></br>
                        <p class="txtTokoMerc"> Toko A</p>
                        </center>
                        </div>

                        <div class="col-3">
                        <center>
                          <img src = 'assets/images/iklan1.jpg'  id="imgTokoMerch" class="img-thumbnail btn btn-light" data-toggle="modal" data-target="#myModalbarang" ></br>
                          <p class="txtTokoMerc"> Toko A</p>
                        </center>
                        </div>

                        <div class="col-3">
                        <center>
                          <img src = 'assets/images/iklan1.jpg' id="imgTokoMerch" class="img-thumbnail btn btn-light" data-toggle="modal" data-target="#myModalbarang" ></br>
                          <p class="txtTokoMerc"> Toko A</p>
                        </center>
                        </div>

                        <div class="col-3">
                        <center>
                          <img src = 'assets/images/iklan1.jpg' id="imgTokoMerch" class="img-thumbnail btn btn-light" data-toggle="modal" data-target="#myModalbarang" ></br>
                          <p class="txtTokoMerc"> Toko A</p>
                        </center>
                        </div>

                        </div>

              
                    </div>
                    
                    <div id="chat" class="container tab-pane fade"><br>
                    
                          <div class="row">
                            <div class="col-6">
                                  <h1 class="txtChatMerch">Chat Via Wahtsapp</h1>
                                  <button type="button" class="btn btn-success">Chat</button>
                            </div>
                            <div class="col-6">
                                <h1  class="txtChatMerch">Chat disini</h1>
                                <form action="#">
                                  <div class="form-group">
                                  <input type="text" class="form-control" id="namaUser" placeholder="Nama">
                                  </div>
                                  <div class="form-group">
                                  <input type="text" class="form-control" id="noUser" placeholder="No Tlp">
                                  </div>
                                  <button type="submit" class="btn btn-success" data-toggle="modal" data-target="#modalChat">Chat</button>
                                </form>
                            </div>
                          </div>
              
                  </div>


    </div>
  </div>
</div>


@endsection