<!DOCTYPE html>
<html lang="en">
<head>
  <title>Treddee.com | Bantuan</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
  <link rel="stylesheet" type="text/css" href="css/userStyle.css">
</head>
<body>

<div class="boxBannerBantuan">
    <div class="container">
    <center><h1 class="hBtn"><font color="white">Hai, Apa yang bisa kamu bantu?</font></h1></center></br>
    <form class="form" action="/action_page.php">
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" placeholder="Cari Bantuan">
                        <div class="input-group-append">
                            <span class="btn btn bg-warning">Search</span>
                        </div>
                    </div>
                </form>
    </div>
</div>

<div class="container">
  <br>
  <nav class="navbar navbar-expand-md bg-dark navbar-dark">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="collapsibleNavbar">
      <ul class="nav nav-pills" role="tablist">
        <li class="nav-item">
          <a class="nav-link active" data-toggle="pill" href="#bantuan">Pusat Bantuan</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-toggle="pill" href="#faq">FAQ</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-toggle="pill" href="#kontak">Hubungi Kami</a>
        </li>
      </ul>
    </div>
  </nav>

 
  <div class="tab-content">
    <div id="bantuan" class="container tab-pane active"><br>
        <div class="card">
            <div class="card-body">
                Isi Bantuan 
            </div>
        </div>
    </div>

    <div id="faq" class="container tab-pane fade"><br>
        <div class="card">
            <div class="card-body">
                Isi FAQ
            </div>
        </div>
    </div>

    <div id="kontak" class="container tab-pane fade"><br>
        <div class="card">
            <div class="card-body">
                Isi Kontak
            </div>
        </div>
    </div>
    
  </div>
</div>





</br>
</br>
<footer>
  <center><p>Kisah Kreatif | Copyright 2020 © All Rights Reserved</p></center>
</footer>

</body>
</html>



