<!DOCTYPE html>
<html lang="en">
<head>
  <title>Treddee.com</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
  <link rel="stylesheet" type="text/css" href="css/userStyle.css">
</head>
<body>


 <!-- The Modal Main -->
 <div class="modal fade" id="login">
    <div class="modal-dialog ">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
        <h1>Login</h1>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
        <form action="#">
          <div class="form-group">
            <label for="email">ID :</label>
            <input type="text" class="form-control" placeholder="Enter ID" name="id">
          </div>
          <div class="form-group">
            <label for="pwd">Password:</label>
            <input type="password" class="form-control" placeholder="Enter password" name="pwd">
          </div>
          <button type="" class="btn btn-primary"><a href="ProfileMerchants" class="txtLdNav">Login</a></button> 
          <a href="Regist" class="txtLdNav">Buka Toko</a>
        </form>
        </div>
        
    
        
      </div>
    </div>
  </div>

<section>
<!-- Header Text -->
<div class="boxLanding">
    
    <div class="textLanding1">
        <center><font face="Tahoma" color="white" >Selamat datang di</font></center>
    </div>
    <div class="textLanding2">
        <center><font face="Tahoma" color="white" >Traddee.com</font></center>
        
    </div>
    <div class="container">
   
      <div class="start">
        <button type="button" class="btn btn-link">
          <a href="HomePagesUser">
            <center><font face="Tahoma" color="white" size="5px" class="ljt">Lanjutkan</font></center>
          </a>
        </button>
      </div>
    </div>
</div>
<!-- tutup Header Text -->

<!-- Card -->
<div class="container">
    <div class="container-fluid">
        <div class="row">
          <div class="col-3">
          <div class="txtNavLand">
          <center><a href="#">
            <img src = 'assets/icon/013-shop.png' class="imgNavLand btn btn-light" data-toggle="modal" data-target="#login" ></br>
          <p class="txtLdNav">Login Merchants</p></a>
         
          </center>
          </div>
          </div>

          <div class="col-3">
          <div class="txtNavLand">
          <center>
            <img src = 'assets/icon/023-online store.png' class="imgNavLand  btn btn-light"></br>
            Download Apps
          </center>
          </div>
          </div>

          <div class="col-3">
          <div class="txtNavLand">
          <center>
            <img src = 'assets/icon/045-website.png'class="imgNavLand  btn btn-light"></br>
            Tentang Kami
          </center>
          </div>
          </div>

          <div class="col-3">
          <div class="txtNavLand">
          <center>
          <a href="Bantuan">
            <img src = 'assets/icon/041-customer service.png' class="imgNavLand  btn btn-light"></br>
           <p class="txtLdNav"> Bantuan</p></a>
          </center>
          </div>
          </div>
        </div>
</div>
</div>
<!-- Tutup Card -->

</section>
<footer>
  <center><p>Kisah Kreatif | Copyright 2020 © All Rights Reserved</p></center>
</footer>

</body>
</html>



