<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ketersediaan extends Model
{
    protected $table = 'ketersediaans';
    protected $fillable = ['id','kode_penjualan','produk_id','ukm_id','banyak','total'];

    public function keranjang()
    {
      return $this->hasOne('App\keranjang');
    }
}
