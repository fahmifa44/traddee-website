<?php

namespace App\Http\Controllers;

use App\Ukm;
use App\User;
use Illuminate\Http\Request;

class ZController extends Controller
{
    public function index()
    {
        $ukm = Ukm::All();
        $data['data'] = User::select('*','foto_produk.file','users.name as nama_ukm','users.username as no_wanya')
          ->join('ukm','ukm.user_id','=','users.id')
          ->join('produk','produk.ukm_id','=','ukm.id')
          ->join('foto_produk','foto_produk.produk_id','=','produk.id')
          ->where('foto_produk.status','1')
          ->where('users.role','!=','admin')
          ->get();
        return response($data);
    }
}
