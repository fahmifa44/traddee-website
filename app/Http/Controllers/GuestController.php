<?php

namespace App\Http\Controllers;

use Auth;
use App\Ukm;
use App\Produk;
use App\User;
use Illuminate\Http\Request;

class GuestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = Ukm::All();
        return view('welcome', compact('list'));
    }

    public function resetPassword(Request $request)
    {
        if ($request->username != 'adminadmin') {
          $noAdmin = 6282257851837;
          $cek = User::where('username',$request->username)->first();
          if ($cek == null) {
              return redirect()->back()->with('fails','Nomor HP '.$request->username.' yang anda masukkan tidak terdaftar dalam Sitem');
          }else{
              return redirect('https://api.whatsapp.com/send?phone='.$noAdmin.'&text=Hallo Admin, tolong resetkan password UKM '. $cek->name. ' dengam nomor HP '. $cek->username);
          }
        } else {
          return redirect()->back()->with('fails','Nomor HP '.$request->username.' yang anda masukkan tidak terdaftar dalam Sitem');
        }


    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $profil = Ukm::where('slug', $slug)->firstOrFail();
        return view('profile', compact('profil'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function product($slug, $slug_produk)
    {
      $identify = Produk::select('*','ukm.id as ukmid','produk.id as produkid')
        ->join('ukm','ukm.id','=','produk.ukm_id')
        ->where('ukm.slug',$slug)
        ->where('produk.slug_produk',$slug_produk)
        ->first();
      if (empty($identify)) {
        $arr['judul'] = "Tidak Ditemukan";
        $arr['pesan'] = "Barang atau UKM dengan slug UKM ".$slug. " dan slug Produk " .$slug_produk. " tidak ditemukan";
        $arr['backlink'] = "/";
        return view('admin.gaada-ukm', compact('arr'));
      }else{
      $ukm = ukm::where('slug', $slug)->get();
      // $barang = $ukm->produk->where('slug_produk', $slug_produk)-firstOrFail();
      $barang = Produk::whereId($identify->produkid)->first();
      // dd($barang);
      $produk = Produk::All();

      return view('produk', compact('produk','barang'));
    }
    }
}
