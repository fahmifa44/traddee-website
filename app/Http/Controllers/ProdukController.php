<?php

namespace App\Http\Controllers;
use Auth;
use Image;
use App\User;
use App\Ukm;
use App\Produk;
use App\Foto_Produk;
use Illuminate\Http\Request;
use Redirect,Response;

class ProdukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userid = Auth::user()->id;
        $ukm = Ukm::where('user_id',$userid)->first();
        $produk = $ukm->produk()->where('status',1)->paginate(6);
        return view('ukm.produk',compact('ukm','produk'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('ukm.create-produk');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      foreach ($request->field_name as $xz) {
        $gbr = $xz;
        $nama_Gbr = $gbr->getClientOriginalName();

        $a1 = $nama_Gbr;
          if ((strpos($a1, "JPG") || strpos($a1, "PNG") || strpos($a1, "jpg") || strpos($a1, "png") || strpos($a1, "jpeg") || strpos($a1, "JPEG")) == false) {
              return redirect()->back()->with('gagal','Silahkan upload bukti berupa PNG, JPG, atau JPEG');
          }
      }

      if (count($request->field_name) >= 6) {
        return redirect('/ukm/product-create')->with('gagal','Maksimal Foto Yang Dapat Diunggah adalah 5');
      }else{
        $tempatfile = ('foto_produk');
        $userid = Auth::user()->id;

        $ukm = Ukm::select('*')
                ->where('user_id',$userid)
                ->first();

        $z = Produk::create([
            'ukm_id' => $ukm->id,
            'nama_produk' => $request->nama_produk,
            'slug_produk' => str_slug($request->nama_produk, '-')."-".str_slug($request->rasa_produk, '-')."-".$request->berat_produk,
            'deskripsi' => $request->deskripsi_produk,
            'berat' => $request->berat_produk,
            'rasa' => $request->rasa_produk,
            'harga' => $request->harga_produk,
            'status' => 1,
            'tersedia' =>1,
        ]);
        if (count($request->field_name)==1) {
          $i = 0;
          foreach ($request->field_name as $p) {
          $i++;
          $gbr = $p;
          $nama_Gbr = uniqid() . '.' .$gbr->getClientOriginalExtension();
          // $gbr->move($tempatfile, $nama_Gbr);

          Image::make($p)->save(public_path('foto_produk') . '/' . $nama_Gbr);
              $canvas = Image::canvas(100, 100);
              $resizeImage  = Image::make($p)->resize(100, 100, function($constraint) {
                  $constraint->aspectRatio();
              });
              $canvas->insert($resizeImage, 'center');

          if ($i == 1) {
            $a = new Foto_Produk;
            $a->produk_id = $z->id;
            $a->file = $nama_Gbr;
            $a->status = 1;
            $a->save();
          }else{
            $a = new Foto_Produk;
            $a->produk_id = $z->id;
            $a->file = $nama_Gbr;

            $a->save();
          }

        }
        return redirect('/ukm/product');
      }else{
        $i = 0;
        foreach ($request->field_name as $p) {
        $i++;
        $gbr = $p;
        $nama_Gbr = uniqid() . '_' .$gbr->getClientOriginalExtension();
        // $gbr->move($tempatfile, $nama_Gbr);

        Image::make($p)->save(public_path('foto_produk') . '/' . $nama_Gbr);
            $canvas = Image::canvas(100, 100);
            $resizeImage  = Image::make($p)->resize(100, 100, function($constraint) {
                $constraint->aspectRatio();
            });
            $canvas->insert($resizeImage, 'center');


        if ($i == 1) {
          $a = new Foto_Produk;
          $a->produk_id = $z->id;
          $a->file = $nama_Gbr;
          $a->status = 1;
          $a->save();
        }else{
          $a = new Foto_Produk;
          $a->produk_id = $z->id;
          $a->file = $nama_Gbr;

          $a->save();
        }

      }
      return redirect('/ukm/product-create/'.$z->id.'/select-profile')->with('baruBuat','a');
    }
  }}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */



    public function show($id)
    {
        $userid = Auth::user()->id;
        $produk = Produk::whereId($id)->first();
        $ukm = Ukm::where('user_id',$userid)->first();
        // dd($ukm->id,$produk->ukm_id);
        // dd();

        if (empty($produk)) {
          $arr['judul'] = "Produk Tidak Ditemukan !!";
          $arr['pesan'] = "Produk dengan id ".$id. " belum ada atau bukan milik anda . . . ";
          $arr['backlink'] = "/ukm/product";
          return view('admin.gaada-ukm', compact('arr'));
        }else if ($produk->ukm_id == $ukm->id && !empty($produk)) {
          return view('ukm.detail-produk', compact('produk'));
        }else{
          $arr['judul'] = "Produk Tidak Ditemukan !!";
          $arr['pesan'] = "Produk dengan id ".$id. " belum ada atau bukan milik anda . . . ";
          $arr['backlink'] = "/ukm/product";
          return view('admin.gaada-ukm', compact('arr'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $userid = Auth::user()->id;
      $id_ukm = Ukm::where('user_id',$userid)->first();
      $produk = Produk::where('slug_produk',$id)->where('ukm_id',$id_ukm->id)->first();
      $ukm = Ukm::where('user_id',$userid)->first();
      // dd($ukm->id,$produk->ukm_id);
      // dd($produk);

      if (empty($produk)) {
        $arr['judul'] = "Produk Tidak Ditemukan !!";
        $arr['pesan'] = "Produk dengan id ".$id. " belum ada atau bukan milik anda . . . ";
        $arr['backlink'] = "/ukm/product";
        return view('admin.gaada-ukm', compact('arr'));
      }else if ($produk->status == 0) {
        $arr['judul'] = "Produk Tidak Ditemukan !!";
        $arr['pesan'] = "Produk dengan id ".$id. " sudah tidak aktif dan di nonaktifkan . . . ";
        $arr['backlink'] = "/ukm/product";
        return view('admin.gaada-ukm', compact('arr'));
      }else if ($produk->ukm_id == $ukm->id && !empty($produk)) {
        return view('ukm.edit-produk', compact('produk'));
      }else if($produk->ukm_id !== $ukm->id && !empty($produk)){
        $arr['judul'] = "Produk Tidak Ditemukan !!";
        $arr['pesan'] = "Produk dengan id ".$id. " belum ada atau bukan milik anda . . . ";
        $arr['backlink'] = "/ukm/product";
        return view('admin.gaada-ukm', compact('arr'));
      }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
      $tempatfile = ('foto_produk');
      if (empty($request->file('field_name'))) {
        $ops = Produk::whereId($request->produk_id)
            ->update([
                'nama_produk' => $request->nama_produk,
                'slug_produk' => str_slug($request->nama_produk, '-')."-".str_slug($request->rasa_produk, '-')."-".$request->berat_produk,
                'deskripsi' => $request->deskripsi_produk,
                'berat' => $request->berat_produk,
                'rasa' => $request->rasa_produk,
                'harga' => $request->harga_produk,
            ]);
        return redirect('ukm/edit-product/'.str_slug($request->nama_produk, '-')."-".str_slug($request->rasa_produk, '-')."-".$request->berat_produk)->with('sukses','Berhasil mengubah data');
      }
      else if(!empty($request->file('field_name'))){
        $produk = Produk::whereId($request->produk_id)->first();
        $foto = Foto_Produk::where('produk_id',$request->produk_id)->get();
        $sisa = (5 - $foto->count());
        // dd($sisa);
        //Validate Count()//
        if ($foto->count() >= $sisa) {
          return redirect()->back()->with('gagal','Anda Telah Memiliki '. $foto->count() .' Foto Untuk Produk '.$produk->nama_produk.' , Batas Upload Foto Saat Ini Adalah ' . $sisa);
        }

        // Validate
        foreach ($request->field_name as $xz) {
          $gbr = $xz;
          $nama_Gbr = $gbr->getClientOriginalName();

          $a1 = $nama_Gbr;
            if ((strpos($a1, "JPG") || strpos($a1, "PNG") || strpos($a1, "jpg") || strpos($a1, "png") || strpos($a1, "jpeg") || strpos($a1, "JPEG")) == false) {
                return redirect()->back()->with('gagal','Silahkan upload foto berupa PNG, JPG, atau JPEG');
            }
        }

        Produk::whereId($request->produk_id)
            ->update([
                'nama_produk' => $request->nama_produk,
                'slug_produk' => str_slug($request->nama_produk, '-')."-".str_slug($request->rasa_produk, '-')."-".$request->berat_produk,
                'deskripsi' => $request->deskripsi_produk,
                'berat' => $request->berat_produk,
                'rasa' => $request->rasa_produk,
                'harga' => $request->harga_produk,
            ]);

        foreach ($request->field_name as $p) {

        $gbr = $p;
        $nama_Gbr = uniqid() . '.' .$gbr->getClientOriginalExtension();
        // $gbr->move($tempatfile, $nama_Gbr);

        Image::make($p)->save(public_path('foto_produk') . '/' . $nama_Gbr);
            $canvas = Image::canvas(100, 100);
            $resizeImage  = Image::make($p)->resize(100, 100, function($constraint) {
                $constraint->aspectRatio();
            });
            $canvas->insert($resizeImage, 'center');

        $a = new Foto_Produk;
        $a->produk_id = $request->produk_id;
        $a->file = $nama_Gbr;

        $a->save();
        }
        return redirect('ukm/edit-product/'.str_slug($request->nama_produk, '-')."-".str_slug($request->rasa_produk, '-')."-".$request->berat_produk)->with('sukses','Berhasil melakukan update barang');
      }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyFoto($id)
    {
        $userid = Auth::user()->id;

        $ukm = Ukm::where('user_id',$userid)->first();
        $foto = Foto_Produk::whereId($id)->first();
        if (empty($foto)) {
          $arr['judul'] = "Foto tidak ditemukan";
          $arr['pesan'] = "Produk dengan id ".$id. " tidak ditemukan atau bukan milik anda . . . ";
          $arr['backlink'] = "/ukm/edit-product/";
          return view('admin.gaada-ukm', compact('arr'));
        }
        // Jika Foto ADA
        elseif (!empty($foto)) {
          $produk = Produk::whereId($foto->produk_id)->first();
          if ($ukm->id == $produk->ukm_id) {
            Foto_Produk::whereId($id)->delete();
            return redirect()->back()->with('sukses','Berhasil menghapus foto');
          }else{
            $arr['judul'] = "Foto Tidak Ditemukan";
            $arr['pesan'] = "Anda tidak memiliki foto dengan ID ".$id;
            $arr['backlink'] = "/ukm/product/";
            return view('admin.gaada-ukm', compact('arr'));
          }
        }



    }
    public function destroy(Request $request)
    {
      Produk::whereId($request->id_produk)
        ->update([
          'status' => 0
        ]);
      return redirect()->back()->with('sukses','Berhasil menghapus produk');
    }
    public function ubahTersedia($id){
        Produk::where('id', $id)->update([
            'tersedia'=>1,
        ]);
        return redirect()->back();
    }
    public function ubahHabis($id){
        Produk::where('id', $id)->update([
            'tersedia'=>0,
        ]);
        return redirect()->back();
    }
}
