<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Auth;
use App\User;
use App\Ukm;
use Illuminate\Http\Request;

class PrivasiController extends Controller
{
    public function index()
    {
        $ukm = Ukm::All();
        return view('admin.reset-password', compact('ukm'));
    }

    public function resetPassword(Request $request)
    {
      $newPwsd = substr(md5(uniqid(mt_rand(), true)), 8, 8);
      User::whereId($request->ukm_id)
        ->update([
            'password' => Hash::make($newPwsd)
        ]);
      $user = User::whereId($request->ukm_id)->first();
      return redirect('https://api.whatsapp.com/send?phone='.$user->username.'&text=Hallo UKM '.$user->name. ', Terimakasih Telah Melakukan Reset Password. Password Sementara UKM anda adalah '. $newPwsd. '. Segera Login dan Rubah Password Sementara anda, Terimakasih.');
    }

    public function editPassword()
    {
        return view('ukm.edit-password');
    }
    public function permissionPassword(Request $request)
    {
        $current_password = Auth::user()->password;
        if (Hash::check($request->password_lama, $current_password)) {
          return redirect()->back()->with('changePassword','a');
        }else{
          return redirect()->back()->with('gagal','Password yang anda masukkan tidak sesuai dengan password saat ini');
        }
    }
    public function updatePassword(Request $request)
    {
        $userid = Auth::user()->id;
        $current_password = Auth::user()->password;
        // dd(bcrypt($request->password_lama));
          if ($request->password_baru == $request->ulangi_password_baru) {
            User::whereId($userid)
              ->update([
                  'password' => Hash::make($request->ulangi_password_baru),
              ]);
            return redirect()->back()->with('sukses','Berhasil Mengubah Password');
          }elseif ($request->password_baru !== $request->ulangi_password_baru) {
            return redirect()->back()->with('gagal','Password baru anda tidak sesuai');
          }
    }
    public function editNomor(Request $request)
    {
        return view('ukm.edit-nomor');
    }
    public function permissionNomor(Request $request)
    {
      $current_password = Auth::user()->password;
      if (Hash::check($request->password_lama, $current_password)) {
        return redirect()->back()->with('changeNohp','a');
      }else{
        return redirect()->back()->with('gagal','Password yang anda masukkan tidak sesuai dengan password saat ini');
      }
    }
    public function updateNohp(Request $request)
    {
      $userid = Auth::user()->id;
      User::whereId($userid)
        ->update([
            'username' => $request->nohp_baru,
        ]);
      return redirect()->back()->with('sukses','Berhasil Mengubah Nomor HP / Username');
    }
}
