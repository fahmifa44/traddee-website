<?php

namespace App\Http\Controllers;

use App\Ukm;
use App\penjualan;
use App\keranjang;
use App\Produk;
use Carbon\Carbon;
use Auth;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (Auth::user()->role=="ukm") {
            return redirect('/ukm');
        }else if (Auth::user()->role=="admin") {
            return redirect('/super-admin');
        }
    }
    public function indexUKM()
    {
        $now = Carbon::today();
        $user = Auth::user();
        $ukm = Ukm::where('user_id', $user->id)->first();

        $penjualan = null;
        if (empty(penjualan::where('ukm_id',$ukm->id)->get())){
            $penjualan = penjualan::where('ukm_id',$ukm->id)->get();
        }
        // dd($penjualan);

        $keranjang = 0;
        if (keranjang::where('ukm_id', $ukm->id)->get()){
            $keranjang = keranjang::where('ukm_id', $ukm->id)->get();

        }

//        Hitung lama penggunaan apps
        $createuser = $user->created_at;
        $hari = $now->diffInDays($createuser);
        $tanggal = $now->diffInMonths($createuser) ;
        $tahun = $now->diffInMonths($createuser) ;
        $lamapenggunaan = $hari + ($tanggal*30);

//        Terjual Hari ini


//        dd(date('Y-m-d', strtotime($penjualan->created_at)));
//        $banyakterjual = $keranjang->sum('banyak');
      $now = Carbon::now();
      $hariini = date('Y-m-d', strtotime($now));

      $penjualan = penjualan::where('ukm_id',$ukm->id)
        ->where('created_at','like','%'.$hariini.'%')
        ->get();

      // penjualan unit hari ini
        $total = 0;

        foreach ($penjualan as $penjualans){

            $kode = $penjualans->kode_penjualan;
            $ranjang = keranjang::where('kode_penjualan', $kode)->get();

            foreach ($ranjang as $ranjangs){
               $total += $ranjangs->banyak;

            }
        }

//        income hari ini
        $income = $penjualan->sum('total_bayar');


//        paling diminati
        $keranjanguser = Auth::user()->ukm->keranjang;
        $totalbanyak = $keranjanguser->sum('banyak');
        $produk = Produk::where('ukm_id', $ukm->id)->get();
//        dd($totalbanyak);


       return view('ukm.index', compact('user','ukm'))
//            ->with('banyakterjual', $banyakterjual)

           ->with('lamapenggunaan', $lamapenggunaan)
           ->with('keranjang', $keranjang)
           ->with('tahun', $tahun)
           ->with('income', $income)
           ->with('total', $total)
           ->with('totalbanyak', $totalbanyak)
           ->with('produk', $produk);

//        dd($now, $banyakterjual, $lamapenggunaan);
    }
}
