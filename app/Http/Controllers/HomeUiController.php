<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeUiController extends Controller
{
    public function LandingPages()
    {
        return view('ui/user/landingPage/landingPage');
    }
    public function Bantuan()
    {
        return view('ui/user/landingPage/bantuan');
    }
    public function Index()
    {
        return view('ui/user/HomePagesUser');
    }
    public function ProfileMerchantsUser()
    {
        return view('ui/user/profileMerchantsUser');
    }
}
