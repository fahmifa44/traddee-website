<?php

namespace App\Http\Controllers;
use Auth;
use App\User;
use App\Ukm;
use App\Produk;
use App\Foto_Produk;
use Illuminate\Http\Request;

class FotoProdukController extends Controller
{
  public function setThumbnail(Request $request)
  {
      Foto_Produk::whereId($request->id_foto)
        ->update([
          'status' => 1,
        ]);
      return redirect()->back();
  }
  public function showFotoById($id)
  {
    $userid = Auth::user()->id;
    $ukm = Ukm::where('user_id',$userid)->first();
    $produk = Produk::whereId($id)->first();
      if (empty($produk)) {
        $arr['judul'] = "Produk TIdak Ditemukan! ";
        $arr['pesan'] = "Produk dengan id ".$id. " tidak ditemukan . . . ";
        $arr['backlink'] = "/ukm/product";
        return view('admin.gaada-ukm', compact('arr'));
      }
      else if ($produk->ukm_id !== $ukm->id) {
        $arr['judul'] = "Invalid Hak Akses! ";
        $arr['pesan'] = "Produk dengan id ".$id. " bukan milik anda . . . ";
        $arr['backlink'] = "/ukm/product";
        return view('admin.gaada-ukm', compact('arr'));
      }else if($produk->ukm_id == $ukm->id){
        return view('ukm.create-second-part',compact('produk'));
      }
  }
  public function unsetThumbnail(Request $request)
  {
    Foto_Produk::whereId($request->id_foto)
      ->update([
        'status' => null,
      ]);
    return redirect()->back();
  }
  public function updateThumbnail(Request $request)
  {
    Foto_Produk::whereId($request->id_foto)
      ->update([
        'status' => 1,
      ]);
    Foto_Produk::whereId($request->id_foto_lawas)
      ->update([
        'status' => null,
      ]);
      return redirect()->back()->with('sukses','Berhasil merubah thumbnail Foto Produk');
  }
}
