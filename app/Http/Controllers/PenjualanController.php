<?php

namespace App\Http\Controllers;

use App\keranjang;
use App\Ukm;
use App\Produk;
use App\penjualan;
use Auth;
use PDF;
use Illuminate\Http\Request;

class PenjualanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function cariHarga(Request $request)
    {
        $id_produk = $request->produk_id;
        $harga = Produk::where('id','=', $id_produk)->get();
        return response()->json($harga);
    }

    public function createPDF($id){
        $user = Auth::User();
        $ukm = Ukm::where('user_id', $user->id)->first();
        // $penjualan = penjualan::where('id_penjual', $ukm->id)
        // ->where('id', $id)->first();
        $penjualan = penjualan::whereId($id)->first();
        // CEK
        if (empty($penjualan)) {
            $arr['judul'] = "Nota Tidak Ditemukan ";
            $arr['pesan'] = "Produk dengan id ".$id. " tidak ditemukan . . . ";
            $arr['backlink'] = "/ukm/kasir";
            return view('admin.gaada-ukm', compact('arr'));
        }elseif ($penjualan->ukm_id == $ukm->id) {
            $nota = keranjang::where('kode_penjualan', $penjualan->kode_penjualan)->get();

            $pdf = PDF::loadView('ukm/PDF', compact('penjualan', 'nota', 'user'));
            return $pdf->download('nota.pdf');
//            return view('ukm/PDF')
//                ->with('penjualan',$penjualan)
//                ->with('nota', $nota)
//                ->with('user', $user);

        }else if ($penjualan->id_penjual !== $ukm->id) {
            $arr['judul'] = "Invalid Hak Akses! ";
            $arr['pesan'] = "Penjualan dengan id ".$id. " bukan milik anda . . . ";
            $arr['backlink'] = "/ukm/kasir";
            return view('admin.gaada-ukm', compact('arr'));
        }
    }



    public function index()
    {
        $id = Auth::User()->id;

        $product = keranjang::where('$id');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        global $template;
        settype($template, "string");

        $template = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        /* this line can include numbers or not */

        settype($length, "integer");
        settype($rndstring, "string");
        settype($a, "integer");
        settype($b, "integer");

        for ($a = 0; $a <= 13; $a++) {
            $b = rand(0, strlen($template) - 1);
            $rndstring .= $template[$b];
        }

        $userid = Auth::user()->id;
        $ukm = Ukm::where('user_id', $userid)->first();
        $keranjang = keranjang::where('ukm_id', $ukm->id)->where('kode_penjualan', null)->get();
        $total = $keranjang->sum('total');
//         dd($keranjang);
        if ($keranjang->count()==0 ){
            return redirect()->back()->with('gagal','proses penjualan gagal, keranjang tidak boleh kosong!');
        }else{
            $z = penjualan::create([
                'kode_penjualan' => $rndstring,
                'ukm_id' => $ukm->id,
                'total_bayar' => $total,
            ]);
            foreach ($keranjang as $x) {
                keranjang::whereId($x->id)
                    ->update([
                        'kode_penjualan' => $rndstring,
                    ]);
            }
            return redirect()->back()->with('sukses', 'Pembelian telah dilakukan');
//        }else{
//            return redirect()->back()->with('status', 'error!!');
        }


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = Auth::User()->id;
        $ukm_id = Ukm::where('user_id', $id)->first();
        $itemint = $request->input('produk_id');
        $item = (int)$itemint;
        $produk = Produk::where('id', $item)->first();
        $jumlah = $request->banyak * $produk->harga;
        $tot = (int)$request->banyak;
//        dd($item, $ukm_id->id,$tot, $jumlah);
        keranjang::create([
            'produk_id' => $item,
            'ukm_id' => $ukm_id->id,
            'banyak' => $tot,
            'total' => $jumlah,
        ]);
        return redirect()->back()->with('sukses', 'data berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $user = Auth::User();
        $ukm = Ukm::where('user_id', $user->id)->first();
        // $penjualan = penjualan::where('id_penjual', $ukm->id)
        // ->where('id', $id)->first();
        $penjualan = penjualan::whereId($id)->first();
        // CEK
        if (empty($penjualan)) {
          $arr['judul'] = "Nota Tidak Ditemukan ";
          $arr['pesan'] = "Produk dengan id ".$id. " tidak ditemukan . . . ";
          $arr['backlink'] = "/ukm/kasir";
          return view('admin.gaada-ukm', compact('arr'));
        }elseif ($penjualan->ukm_id == $ukm->id) {
          $nota = keranjang::where('kode_penjualan', $penjualan->kode_penjualan)->get();
          return view('ukm/detail-nota')
          ->with('penjualan',$penjualan)
          ->with('nota', $nota)
          ->with('user', $user);

        }else if ($penjualan->id_penjual !== $ukm->id) {
          $arr['judul'] = "Invalid Hak Akses! ";
          $arr['pesan'] = "Penjualan dengan id ".$id. " bukan milik anda . . . ";
          $arr['backlink'] = "/ukm/kasir";
          return view('admin.gaada-ukm', compact('arr'));
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $krgj = keranjang::find($id);
        $krgj->delete();
        return redirect('/ukm/kasir');
    }
}
