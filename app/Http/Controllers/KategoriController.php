<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class KategoriController extends Controller
{
    public function Makanan()
    {
        return view('ui/user/kategori/kategoriMakanan');

    }
    public function Produsen()
    {
        return view('ui/user/kategori/kategoriProdusen');

    }
    public function Store()
    {
        return view('ui/user/kategori/kategoriStore');

    }
    public function Wisata()
    {
        return view('ui/user/kategori/kategoriWisata');

    }
    public function Barang()
    {
        return view('ui/user/kategori/kategoriBarang');

    }
    public function Jasa()
    {
        return view('ui/user/kategori/kategoriJasa');

    }
    public function Properti()
    {
        return view('ui/user/kategori/kategoriProperti');

    }
    public function Hasilbumi()
    {
        return view('ui/user/kategori/kategoriHasilbumi');

    }

}
