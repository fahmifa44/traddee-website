<?php

namespace App\Http\Controllers;

use Auth;
use Image;
use App\User;
use App\Ukm;
use Validator;
use Illuminate\Http\Request;
use App\keranjang;
use App\penjualan;

class UkmController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ukm = Ukm::All();
        return view('admin.index', compact('ukm'));
    }

    public function kasir()
    {
        $userid = Auth::user()->id;
        $ukm = Ukm::where('user_id', $userid)->first();
        $keranjang = keranjang::select('*')
            ->where('ukm_id', $ukm->id)
            ->where('kode_penjualan', null)
            ->get();
        $total = $keranjang->sum('total');


//        return view('ukm.kasir')
//                ->with('ukm', $ukm)
//                ->with('keranjang', $keranjang);
        if ($keranjang == null) {
            return view('ukm.kasir')
                ->with('ukm', $ukm);
        } else {
            return view('ukm.kasir')
                ->with('ukm', $ukm)
                ->with('keranjang', $keranjang)
                ->with('total', $total);
        }

    }

    public function updateData(Request $request){
        $user = Auth::User()->id;
        // $tempatfile = ('foto_produk');
        if(!empty($request->file('foto_profil'))){
          $gbr = $request->file('foto_profil');
          $nama_Gbr = $gbr->getClientOriginalName();
          // VALIDATE
          $a1 = $nama_Gbr;
            if ((strpos($a1, "JPG") || strpos($a1, "PNG") || strpos($a1, "jpg") || strpos($a1, "png") || strpos($a1, "jpeg") || strpos($a1, "JPEG")) == false) {
                return redirect()->back()->with('gagal','Silahkan upload foto profil berupa PNG, JPG, atau JPEG');
            }else{
              $gbr = $request->file('foto_profil');
              $nama_Gbr = $gbr->getClientOriginalName();
              // $tempatfile = ('foto_profil');
              $nama_Gbr = uniqid() . '.' .$gbr->getClientOriginalExtension();
              // $gbr->move($tempatfile, $nama_Gbr);

              Image::make($gbr)->save(public_path('foto_profil') . '/' . $nama_Gbr);
                  $canvas = Image::canvas(100, 100);
                  $resizeImage  = Image::make($gbr)->resize(100, 100, function($constraint) {
                      $constraint->aspectRatio();
                  });
                  $canvas->insert($resizeImage, 'center');

              Ukm::where('user_id', $user)->update([
                  'nama_pemilik' => $request->nama_pemilik,
                  'alamat' => $request->alamat,
                  'deskripsi' => $request->deskripsi,
                  'foto_profil' => $nama_Gbr,
              ]);
              return redirect()->back()
              ->with('sukses','Profil sukses di edit');
            }
        }else if (empty($request->file('foto_profil'))) {
        Ukm::where('user_id', $user)->update([
            'nama_pemilik'=>$request->nama_pemilik,
            'alamat'=>$request->alamat,
            'deskripsi'=>$request->deskripsi,
        ]);
        return redirect()->back()
        ->with('sukses','Profil sukses di edit');
      }
//        dd($user);
    }

    public function nota()
    {
        $userid = Auth::user()->id;
        $ukm = Ukm::where('user_id', $userid)->first();
        $nota = penjualan::where('ukm_id', $ukm->id)->get();

        return view('ukm.nota')->with('nota', $nota);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $cek = User::where('username', $request->username)
            ->first();

        if (!empty($cek)) {
            return redirect()->back()->with('gagal', 'Username telah digunakan');

        } else if (!preg_match('/^[a-zA-Z ]*$/', $request->nama_pemilik)) {
            return redirect()->back()->with('gagal', 'Nama pemilik harus berupa HURUF');
        } else if ($request->password !== $request->password_confirmation) {
            return redirect()->back()->with('gagal', 'Passwrod tidak sesuai');
        } else {

            $u = User::create([
                'role' => 'ukm',
                'name' => $request->name,
                'username' => $request->username,
                'password' => bcrypt($request->password),

            ]);
            Ukm::create([
                'user_id' => $u->id,
                'nama_pemilik' => $request->nama_pemilik,
                'slug' => "ukm-".str_slug($request->name, '-'),
                'alamat' => $request->alamat,
                'iframe' => $request->iframe,
            ]);
            return redirect()->back()->with('sukses', 'Berhasil membuat UKM');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ukm = Ukm::whereId($id)->first();
        if (empty($ukm)) {
            $arr['judul'] = "UKM Tidak Ditemukan !!";
            $arr['pesan'] = "UKM dengan id " . $id . " belum terdaftar . . . ";
            $arr['backlink'] = "/super-admin";
            return view('admin.gaada-ukm', compact('arr'));
        } else {
            return view('admin.detail-ukm', compact('ukm'));
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $ukm = Auth::User()->Ukm;
        return view('ukm.edit-profile')
            ->with('ukm', $ukm);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $cek = User::where('username', $request->username)
            ->first();

        if (!empty($cek) && $request->username !== $request->last_username) {
            return redirect()->back()->with('gagal', 'Username telah digunakan');

        } else if (!preg_match('/^[a-zA-Z ]*$/', $request->nama_pemilik)) {
            return redirect()->back()->with('gagal', 'Nama pemilik harus berupa HURUF');
        } else {
            Ukm::where('id', $request->id_ukm)
                ->update([
                    'nama_pemilik' => $request->nama_pemilik,
                    'alamat' => $request->alamat,
                    'iframe' => $request->iframe,
                    'slug' => "ukm-".str_slug($request->name, '-'),
                ]);
            User::where('id', $request->id_user)
                ->update([
                    'name' => $request->name,
                    'username' => $request->username,
                ]);
            return redirect()->back()->with('sukses', 'Berhasil mengubah data UKM');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        Ukm::whereId($request->id_ukm)->delete();
        User::whereId($request->id_user)->delete();

        return redirect()->back()->with('sukses', 'Berhasil menghapus UKM ' . $request->nama_ukm);
    }

    public function pengaturan()
    {
        $ukm = Auth::User()->Ukm;
        return view('ukm.pengaturan', compact('ukm'));
    }

}
