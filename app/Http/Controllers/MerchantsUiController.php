<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MerchantsUiController extends Controller
{
    public function Index()
    {
        return view('ui/user/merchants/profilemerchants');

    }

    public function Regist()
    {
        return view('ui/user/merchants/registrasiToko');

    }

    public function AlertRegist()
    {
        return view('ui/user/merchants/alertRegister');

    }
}
