<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Foto_Produk extends Model
{
    protected $table = 'foto_produk';
    protected $fillable = ['id','produk_id','file','status'];

    public function produk()
    {
      return $this->belongsTo('App\Produk');
    }
}
