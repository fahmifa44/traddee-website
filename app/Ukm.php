<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ukm extends Model
{
    protected $table = 'ukm';
    protected $guarded=[];

    //=== relation model ===//

    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public function produk()
    {
      return $this->hasMany('App\Produk', 'ukm_id');
    }
    public function keranjang()
    {
      return $this->hasMany('App\Keranjang');
    }
    public function penjualan()
    {
      return $this->hasMany('App\penjualan');
    }
}
