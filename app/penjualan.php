<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class penjualan extends Model
{
    protected $table = 'penjualans';
    protected $fillable = ['id','kode_penjualan','ukm_id','total_bayar'];

    public function ukm()
    {
      return $this->belongsTo('App\Ukm');
    }

    public function keranjang(){
        return $this->hasMany('App\keranjang', 'kode_penjualan');
    }
}
