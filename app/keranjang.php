<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class keranjang extends Model
{
    protected $table = 'keranjangs';
    protected $fillable = ['id','kode_penjualan','produk_id','ukm_id','banyak','total'];



    public function ketersediaan()
    {
      return $this->belongsTo('App\ketersediaan');
    }

    public function produk(){
        return $this->belongsTo('App\Produk');
    }

    public function ukm()
    {
        return $this->belongsTo('App\Ukm');
    }

    public function penjualan()
    {
        return $this->belongsTo('App\penjualan');
    }
}
