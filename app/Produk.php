<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
  protected $table = 'produk';
  protected $guarded=[];

  //=== relation model ===//

  public function ukm()
  {
    return $this->belongsTo('App\Ukm');
  }

  public function foto_produk()
  {
    return $this->hasMany('App\Foto_Produk', 'produk_id');
  }
  public function keranjang(){
      return $this->hasMany('App\keranjang');
  }

  public function category(){
      return $this->belongsTo('App\Category', 'category_id');
  }
}
