@extends('layouts.navbar')

@section('title','Beranda')

@section('content')

    <div class="container-fluid page-body-wrapper">
        <!-- partial:partials/_settings-panel.html -->
        <div id="right-sidebar" class="settings-panel">
            <i class="settings-close ti-close"></i>
            <ul class="nav nav-tabs border-top" id="setting-panel" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="todo-tab" data-toggle="tab" href="index.html#todo-section" role="tab"
                       aria-controls="todo-section" aria-expanded="true">TO DO LIST</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="chats-tab" data-toggle="tab" href="index.html#chats-section" role="tab"
                       aria-controls="chats-section">CHATS</a>
                </li>
            </ul>
            <div class="tab-content" id="setting-content">
                <div class="tab-pane fade show active scroll-wrapper" id="todo-section" role="tabpanel"
                     aria-labelledby="todo-section">
                    <div class="add-items d-flex px-3 mb-0">
                        <form class="form w-100">
                            <div class="form-group d-flex">
                                <input type="text" class="form-control todo-list-input" placeholder="Add To-do">
                                <button type="submit" class="add btn btn-primary todo-list-add-btn" id="add-task">Add
                                </button>
                            </div>
                        </form>
                    </div>
                    <div class="list-wrapper px-3">
                        <ul class="d-flex flex-column-reverse todo-list">
                            <li>
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="checkbox" type="checkbox">
                                        Team review meeting at 3.00 PM
                                    </label>
                                </div>
                                <i class="remove ti-close"></i>
                            </li>
                            <li>
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="checkbox" type="checkbox">
                                        Prepare for presentation
                                    </label>
                                </div>
                                <i class="remove ti-close"></i>
                            </li>
                            <li>
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="checkbox" type="checkbox">
                                        Resolve all the low priority tickets due today
                                    </label>
                                </div>
                                <i class="remove ti-close"></i>
                            </li>
                            <li class="completed">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="checkbox" type="checkbox" checked>
                                        Schedule meeting for next week
                                    </label>
                                </div>
                                <i class="remove ti-close"></i>
                            </li>
                            <li class="completed">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="checkbox" type="checkbox" checked>
                                        Project review
                                    </label>
                                </div>
                                <i class="remove ti-close"></i>
                            </li>
                        </ul>
                    </div>
                    <h4 class="px-3 text-muted mt-5 font-weight-light mb-0">Events</h4>
                    <div class="events pt-4 px-3">
                        <div class="wrapper d-flex mb-2">
                            <i class="ti-control-record text-primary mr-2"></i>
                            <span>Feb 11 2018</span>
                        </div>
                        <p class="mb-0 font-weight-thin text-gray">Creating component page build a js</p>
                        <p class="text-gray mb-0">build a js based app</p>
                    </div>
                    <div class="events pt-4 px-3">
                        <div class="wrapper d-flex mb-2">
                            <i class="ti-control-record text-primary mr-2"></i>
                            <span>Feb 7 2018</span>
                        </div>
                        <p class="mb-0 font-weight-thin text-gray">Meeting with Alisa</p>
                        <p class="text-gray mb-0 ">Call Sarah Graves</p>
                    </div>
                </div>
                <!-- To do section tab ends -->
                <div class="tab-pane fade" id="chats-section" role="tabpanel" aria-labelledby="chats-section">
                    <div class="d-flex align-items-center justify-content-between border-bottom">
                        <p class="settings-heading border-top-0 mb-3 pl-3 pt-0 border-bottom-0 pb-0">Friends</p>
                        <small
                            class="settings-heading border-top-0 mb-3 pt-0 border-bottom-0 pb-0 pr-3 font-weight-normal">
                            See All
                        </small>
                    </div>
                    <ul class="chat-list">
                        <li class="list active">
                            <div class="profile"><img src="../../images/faces/face1.jpg" alt="image"><span
                                    class="online"></span></div>
                            <div class="info">
                                <p>Thomas Douglas</p>
                                <p>Available</p>
                            </div>
                            <small class="text-muted my-auto">19 min</small>
                        </li>
                        <li class="list">
                            <div class="profile"><img src="../../images/faces/face2.jpg" alt="image"><span
                                    class="offline"></span></div>
                            <div class="info">
                                <div class="wrapper d-flex">
                                    <p>Catherine</p>
                                </div>
                                <p>Away</p>
                            </div>
                            <div class="badge badge-success badge-pill my-auto mx-2">4</div>
                            <small class="text-muted my-auto">23 min</small>
                        </li>
                        <li class="list">
                            <div class="profile"><img src="../../images/faces/face3.jpg" alt="image"><span
                                    class="online"></span></div>
                            <div class="info">
                                <p>Daniel Russell</p>
                                <p>Available</p>
                            </div>
                            <small class="text-muted my-auto">14 min</small>
                        </li>
                        <li class="list">
                            <div class="profile"><img src="../../images/faces/face4.jpg" alt="image"><span
                                    class="offline"></span></div>
                            <div class="info">
                                <p>James Richardson</p>
                                <p>Away</p>
                            </div>
                            <small class="text-muted my-auto">2 min</small>
                        </li>
                        <li class="list">
                            <div class="profile"><img src="../../images/faces/face5.jpg" alt="image"><span
                                    class="online"></span></div>
                            <div class="info">
                                <p>Madeline Kennedy</p>
                                <p>Available</p>
                            </div>
                            <small class="text-muted my-auto">5 min</small>
                        </li>
                        <li class="list">
                            <div class="profile"><img src="../../images/faces/face6.jpg" alt="image"><span
                                    class="online"></span></div>
                            <div class="info">
                                <p>Sarah Graves</p>
                                <p>Available</p>
                            </div>
                            <small class="text-muted my-auto">47 min</small>
                        </li>
                    </ul>
                </div>
                <!-- chat tab ends -->
            </div>
        </div>
        <!-- partial -->
        <!-- partial:partials/_sidebar.html -->
        <nav class="sidebar sidebar-offcanvas" id="sidebar">
            <ul class="nav">
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('/super-admin/') }}">
                       <i class="ti-home menu-icon"></i>
                        <span class="menu-title">Beranda</span>
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="{{ url('/super-admin/buat-ukm') }}">
                       <i class="ti-view-list menu-icon"></i>
                        <span class="menu-title">Buat UKM</span>
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" data-toggle="collapse" href="{{ url('/super-admin/buat-ukm')}}" aria-expanded="false"
                       aria-controls="e-commerce">
                        <i class="ti-shopping-cart menu-icon"></i>
                        <span class="menu-title">Kasir</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"
                       href="http://www.urbanui.com/justdo/template/demo/vertical-default-dark/pages/widgets/widgets.html">
                        <i class="ti-settings menu-icon"></i>
                        <span class="menu-title">Pengaturan</span>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- partial -->
        <div class="main-panel">
            <div class="content-wrapper">
              <!-- Notifikasi -->
              @if(session('sukses'))
              <div class="alert alert-success" >
              {{session('sukses')}}
              </div>
              @endif
              @if(session('gagal'))
              <div class="alert alert-danger" >
              {{session('gagal')}}
              <!-- End-Notifikasi -->
              </div>
              @endif
              <div class="row">
                @foreach($ukm as $ukm)

                    <div class="col-md-4 stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <p class="card-title">UKM <strong>{{$ukm->user->name}}</strong></p>
                                <p>Nama Pemilik : <strong>{{$ukm->nama_pemilik}}</strong></p>
                                <p>Alamat : <strong>{{$ukm->alamat}}</strong></p>
                                <p>Username : <strong>{{$ukm->user->username}}</strong></p>
                                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#ukm{{$ukm->id}}">
                                  <i class="ti-pencil menu-icon"></i>
                                   <!-- <span class="menu-title">Edit UKM</span> -->
                                </button>
                                <a href="{{ url('/super-admin/detail-ukm') }}/{{$ukm->id}}" class="btn btn-success btn-sm">
                                  <i class="ti-eye menu-icon"></i>
                                   <!-- <span class="menu-title">Detail UKM</span> -->
                                </a>
                                <button type="button" class="btn btn-danger btn-sm float-right" data-toggle="modal" data-target="#delete{{$ukm->id}}">
                                  <i class="ti-trash menu-icon"></i>
                                   <!-- <span class="menu-title">Edit UKM</span> -->
                                </button>

                                <!-- MODAL DELETE -->

                                <div class="modal fade" id="delete{{$ukm->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                  <div class="modal-dialog modal-dialog" role="document">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLongTitle">Peringatan</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body">
                                        Anda yakin ingin menghapus UKM <strong>{{$ukm->user->name}}</strong> ?
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                        <form action="{{ url('/super-admin/delete-ukm') }}" method="post">
                                        @csrf
                                          <input type="hidden" name="id_ukm" value="{{$ukm->id}}">
                                          <input type="hidden" name="nama_ukm" value="{{$ukm->user->name}}">
                                          <input type="hidden" name="id_user" value="{{$ukm->user->id}}">
                                          <button type="submit" class="btn btn-success">Konfirmasi</button>
                                      </form>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <!-- END MODAL DELETE -->

                              <!-- Modal -->
                              <div class="modal fade" id="ukm{{$ukm->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h5 class="modal-title" id="exampleModalLongTitle">{{$ukm->user->name}}</h5>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <div class="modal-body">
                                      <form method="POST" action="{{ url('/update-ukm') }}">
                                          @csrf
                                          <input type="hidden" name="id_ukm" value="{{$ukm->id}}">
                                          <input type="hidden" name="id_user" value="{{$ukm->user->id}}">
                                          <div class="form-group row">
                                              <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nama UKM') }}</label>

                                              <div class="col-md-6">
                                                  <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $ukm->user->name }}" required autocomplete="name" autofocus>
                                              </div>
                                          </div>

                                          <div class="form-group row">
                                              <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nama Pemilik') }}</label>

                                              <div class="col-md-6">
                                                  <input id="name" type="text" class="form-control" name="nama_pemilik" value="{{ $ukm->nama_pemilik }}" required autocomplete="name" autofocus>
                                              </div>
                                          </div>

                                          <div class="form-group row">
                                              <label for="username" class="col-md-4 col-form-label text-md-right">{{ __('Nomor WA') }}</label>
                                              <div class="col-md-6">
                                                  <input id="username" type="number" class="form-control" name="username" value="{{ $ukm->user->username }}" required autocomplete="username">
                                                  <input type="hidden" name="last_username" value="{{ $ukm->user->username }}">
                                              </div>
                                          </div>

                                          <div class="form-group row">
                                              <label for="alamat" class="col-md-4 col-form-label text-md-right">{{ __('Alamat') }}</label>
                                              <div class="col-md-6">
                                                  <input id="alamat" type="text" class="form-control" name="alamat" value="{{ $ukm->alamat }}" required autocomplete="alamat">
                                              </div>
                                          </div>

                                          <div class="form-group row">
                                              <label for="iframe" class="col-md-4 col-form-label text-md-right">{{ __('Link Iframe') }}</label>
                                              <div class="col-md-6">
                                                <textarea class="form-control" name="iframe" rows="8" cols="80">{{$ukm->iframe}}</textarea>
                                                  <!-- <input id="iframe" value="" type="text" class="form-control" name="iframe" required autocomplete="iframe"> -->
                                              </div>
                                          </div>


                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
                                      <button type="submit" class="btn btn-primary btn-sm">Save changes</button>
                                    </div>
                                  </div>
                                  </form>
                                </div>
                              </div>
                            </div>
                        </div>
                    </div>

                @endforeach
              </div>
            <br>

            </div>
            <!-- content-wrapper ends -->
            <!-- partial:partials/_footer.html -->
            <footer class="footer">
                <div class="d-sm-flex justify-content-center justify-content-sm-between">
                    <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2020 <a
                            href="https://www.fahmifa.website/"
                            target="_blank">Sekarepwes</a>. All rights reserved.</span>
                </div>
            </footer>
            <!-- partial -->
        </div>
        <!-- main-panel ends -->
    </div>
@endsection
