@extends('layouts.navbar')

@section('title','Beranda')

@section('content')
    <div class="container-fluid page-body-wrapper">
        <!-- partial:partials/_settings-panel.html -->
        <div id="right-sidebar" class="settings-panel">
            <i class="settings-close ti-close"></i>
            <ul class="nav nav-tabs border-top" id="setting-panel" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="todo-tab" data-toggle="tab" href="index.html#todo-section" role="tab"
                       aria-controls="todo-section" aria-expanded="true">TO DO LIST</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="chats-tab" data-toggle="tab" href="index.html#chats-section" role="tab"
                       aria-controls="chats-section">CHATS</a>
                </li>
            </ul>
            <div class="tab-content" id="setting-content">
                <div class="tab-pane fade show active scroll-wrapper" id="todo-section" role="tabpanel"
                     aria-labelledby="todo-section">
                    <div class="add-items d-flex px-3 mb-0">
                        <form class="form w-100">
                            <div class="form-group d-flex">
                                <input type="text" class="form-control todo-list-input" placeholder="Add To-do">
                                <button type="submit" class="add btn btn-primary todo-list-add-btn" id="add-task">Add
                                </button>
                            </div>
                        </form>
                    </div>
                    <div class="list-wrapper px-3">
                        <ul class="d-flex flex-column-reverse todo-list">
                            <li>
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="checkbox" type="checkbox">
                                        Team review meeting at 3.00 PM
                                    </label>
                                </div>
                                <i class="remove ti-close"></i>
                            </li>
                            <li>
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="checkbox" type="checkbox">
                                        Prepare for presentation
                                    </label>
                                </div>
                                <i class="remove ti-close"></i>
                            </li>
                            <li>
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="checkbox" type="checkbox">
                                        Resolve all the low priority tickets due today
                                    </label>
                                </div>
                                <i class="remove ti-close"></i>
                            </li>
                            <li class="completed">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="checkbox" type="checkbox" checked>
                                        Schedule meeting for next week
                                    </label>
                                </div>
                                <i class="remove ti-close"></i>
                            </li>
                            <li class="completed">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="checkbox" type="checkbox" checked>
                                        Project review
                                    </label>
                                </div>
                                <i class="remove ti-close"></i>
                            </li>
                        </ul>
                    </div>
                    <h4 class="px-3 text-muted mt-5 font-weight-light mb-0">Events</h4>
                    <div class="events pt-4 px-3">
                        <div class="wrapper d-flex mb-2">
                            <i class="ti-control-record text-primary mr-2"></i>
                            <span>Feb 11 2018</span>
                        </div>
                        <p class="mb-0 font-weight-thin text-gray">Creating component page build a js</p>
                        <p class="text-gray mb-0">build a js based app</p>
                    </div>
                    <div class="events pt-4 px-3">
                        <div class="wrapper d-flex mb-2">
                            <i class="ti-control-record text-primary mr-2"></i>
                            <span>Feb 7 2018</span>
                        </div>
                        <p class="mb-0 font-weight-thin text-gray">Meeting with Alisa</p>
                        <p class="text-gray mb-0 ">Call Sarah Graves</p>
                    </div>
                </div>
                <!-- To do section tab ends -->
                <div class="tab-pane fade" id="chats-section" role="tabpanel" aria-labelledby="chats-section">
                    <div class="d-flex align-items-center justify-content-between border-bottom">
                        <p class="settings-heading border-top-0 mb-3 pl-3 pt-0 border-bottom-0 pb-0">Friends</p>
                        <small
                            class="settings-heading border-top-0 mb-3 pt-0 border-bottom-0 pb-0 pr-3 font-weight-normal">
                            See All
                        </small>
                    </div>
                    <ul class="chat-list">
                        <li class="list active">
                            <div class="profile"><img src="../../images/faces/face1.jpg" alt="image"><span
                                    class="online"></span></div>
                            <div class="info">
                                <p>Thomas Douglas</p>
                                <p>Available</p>
                            </div>
                            <small class="text-muted my-auto">19 min</small>
                        </li>
                        <li class="list">
                            <div class="profile"><img src="../../images/faces/face2.jpg" alt="image"><span
                                    class="offline"></span></div>
                            <div class="info">
                                <div class="wrapper d-flex">
                                    <p>Catherine</p>
                                </div>
                                <p>Away</p>
                            </div>
                            <div class="badge badge-success badge-pill my-auto mx-2">4</div>
                            <small class="text-muted my-auto">23 min</small>
                        </li>
                        <li class="list">
                            <div class="profile"><img src="../../images/faces/face3.jpg" alt="image"><span
                                    class="online"></span></div>
                            <div class="info">
                                <p>Daniel Russell</p>
                                <p>Available</p>
                            </div>
                            <small class="text-muted my-auto">14 min</small>
                        </li>
                        <li class="list">
                            <div class="profile"><img src="../../images/faces/face4.jpg" alt="image"><span
                                    class="offline"></span></div>
                            <div class="info">
                                <p>James Richardson</p>
                                <p>Away</p>
                            </div>
                            <small class="text-muted my-auto">2 min</small>
                        </li>
                        <li class="list">
                            <div class="profile"><img src="../../images/faces/face5.jpg" alt="image"><span
                                    class="online"></span></div>
                            <div class="info">
                                <p>Madeline Kennedy</p>
                                <p>Available</p>
                            </div>
                            <small class="text-muted my-auto">5 min</small>
                        </li>
                        <li class="list">
                            <div class="profile"><img src="../../images/faces/face6.jpg" alt="image"><span
                                    class="online"></span></div>
                            <div class="info">
                                <p>Sarah Graves</p>
                                <p>Available</p>
                            </div>
                            <small class="text-muted my-auto">47 min</small>
                        </li>
                    </ul>
                </div>
                <!-- chat tab ends -->
            </div>
        </div>
        <!-- partial -->
        <!-- partial:partials/_sidebar.html -->
        <nav class="sidebar sidebar-offcanvas" id="sidebar">
            <ul class="nav">
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('/super-admin/') }}">
                       <i class="ti-home menu-icon"></i>
                        <span class="menu-title">Beranda</span>
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="{{ url('/super-admin/buat-ukm') }}">
                       <i class="ti-view-list menu-icon"></i>
                        <span class="menu-title">Buat UKM</span>
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" data-toggle="collapse" href="{{ url('/super-admin/buat-ukm')}}" aria-expanded="false"
                       aria-controls="e-commerce">
                        <i class="ti-shopping-cart menu-icon"></i>
                        <span class="menu-title">Kasir</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"
                       href="http://www.urbanui.com/justdo/template/demo/vertical-default-dark/pages/widgets/widgets.html">
                        <i class="ti-settings menu-icon"></i>
                        <span class="menu-title">Pengaturan</span>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper">
            <div class="row">
              <div class="col-12">
                <div class="card">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-lg-4">
                        <div class="border-bottom text-center pb-4">
                          <img src="{{asset('admin/images/faces/face2.jpg')}}" class="img-lg rounded-circle mb-3"/>
                          <div class="mb-3">
                            <h3>{{$ukm->user->name}}</h3>
                          </div>
                          <p class="w-75 mx-auto mb-3">Nomor WA : <strong>{{$ukm->user->username}}</strong></p>
                          <div class="d-flex justify-content-center">
                            <button class="btn btn-success mr-1">Hire Me</button>
                            <button class="btn btn-success">Follow</button>
                          </div>
                        </div>
                        <div class="border-bottom py-4">
                          <p>Skills</p>
                          <div>
                            <label class="badge badge-outline-light">Chalk</label>
                            <label class="badge badge-outline-light">Hand lettering</label>
                            <label class="badge badge-outline-light">Information Design</label>
                            <label class="badge badge-outline-light">Graphic Design</label>
                            <label class="badge badge-outline-light">Web Design</label>
                          </div>
                        </div>
                        <div class="border-bottom py-4">
                          <div class="d-flex mb-3">
                            <div class="progress progress-md flex-grow">
                              <div class="progress-bar bg-primary" role="progressbar" aria-valuenow="55" style="width: 55%" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                          </div>
                          <div class="d-flex">
                            <div class="progress progress-md flex-grow">
                              <div class="progress-bar bg-success" role="progressbar" aria-valuenow="75" style="width: 75%" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                          </div>
                        </div>
                        <div class="py-4">
                          <p class="clearfix">
                            <span class="float-left">
                              Status
                            </span>
                            <span class="float-right text-muted">
                              Active
                            </span>
                          </p>
                          <p class="clearfix">
                            <span class="float-left">
                              Phone
                            </span>
                            <span class="float-right text-muted">
                              006 3435 22
                            </span>
                          </p>
                          <p class="clearfix">
                            <span class="float-left">
                              Mail
                            </span>
                            <span class="float-right text-muted">
                              Jacod@testmail.com
                            </span>
                          </p>
                          <p class="clearfix">
                            <span class="float-left">
                              Facebook
                            </span>
                            <span class="float-right text-muted">
                              <a href="#">David Grey</a>
                            </span>
                          </p>
                          <p class="clearfix">
                            <span class="float-left">
                              Twitter
                            </span>
                            <span class="float-right text-muted">
                              <a href="#">@davidgrey</a>
                            </span>
                          </p>
                        </div>
                        <button class="btn btn-primary btn-block mb-2">Preview</button>
                      </div>
                      <div class="col-lg-8">
                        <div class="d-flex justify-content-between">
                          <div>
                            <button class="btn btn-outline-primary">Message</button>
                            <button class="btn btn-primary">Request</button>
                          </div>
                        </div>
                        <div class="mt-4 py-2 border-top border-bottom">
                          <ul class="nav profile-navbar">
                            <li class="nav-item">
                              <a class="nav-link" href="#">
                                <i class="ti-user"></i>
                                Info
                              </a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link active" href="#">
                                <i class="ti-receipt"></i>
                                Feed
                              </a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" href="#">
                                <i class="ti-calendar"></i>
                                Agenda
                              </a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" href="#">
                                <i class="ti-clip"></i>
                                Resume
                              </a>
                            </li>
                          </ul>
                        </div>
                        <div class="profile-feed">
                          <div class="d-flex align-items-start profile-feed-item">
                            <img src="../../../../images/faces/face13.jpg" alt="profile" class="img-sm rounded-circle"/>
                            <div class="ml-4">
                              <h6>
                                Mason Beck
                                <small class="ml-4 text-muted"><i class="ti-time mr-1"></i>10 hours</small>
                              </h6>
                              <p>
                                There is no better advertisement campaign that is low cost and also successful at the same time.
                              </p>
                              <p class="small text-muted mt-2 mb-0">
                                <span>
                                  <i class="ti-star mr-1"></i>4
                                </span>
                                <span class="ml-2">
                                  <i class="ti-comment mr-1"></i>11
                                </span>
                                <span class="ml-2">
                                  <i class="ti-share"></i>
                                </span>
                              </p>
                            </div>
                          </div>
                          <div class="d-flex align-items-start profile-feed-item">
                            <img src="../../../../images/faces/face16.jpg" alt="profile" class="img-sm rounded-circle"/>
                            <div class="ml-4">
                              <h6>
                                Willie Stanley
                                <small class="ml-4 text-muted"><i class="ti-time mr-1"></i>10 hours</small>
                              </h6>
                              <img src="../../../../images/samples/1280x768/12.jpg" alt="sample" class="rounded mw-100"/>
                              <p class="small text-muted mt-2 mb-0">
                                <span>
                                  <i class="ti-star mr-1"></i>4
                                </span>
                                <span class="ml-2">
                                  <i class="ti-comment mr-1"></i>11
                                </span>
                                <span class="ml-2">
                                  <i class="ti-share"></i>
                                </span>
                              </p>
                            </div>
                          </div>
                          <div class="d-flex align-items-start profile-feed-item">
                            <img src="../../../../images/faces/face19.jpg" alt="profile" class="img-sm rounded-circle"/>
                            <div class="ml-4">
                              <h6>
                                Dylan Silva
                                <small class="ml-4 text-muted"><i class="ti-time mr-1"></i>10 hours</small>
                              </h6>
                              <p>
                                When I first got into the online advertising business, I was looking for the magical combination
                                that would put my website into the top search engine rankings
                              </p>
                              <img src="../../../../images/samples/1280x768/5.jpg" alt="sample" class="rounded mw-100"/>
                              <p class="small text-muted mt-2 mb-0">
                                <span>
                                  <i class="ti-star mr-1"></i>4
                                </span>
                                <span class="ml-2">
                                  <i class="ti-comment mr-1"></i>11
                                </span>
                                <span class="ml-2">
                                  <i class="ti-share"></i>
                                </span>
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
            <!-- partial:partials/_footer.html -->
            <footer class="footer">
                <div class="d-sm-flex justify-content-center justify-content-sm-between">
                    <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2020 <a
                            href="https://www.fahmifa.website/"
                            target="_blank">Sekarepwes</a>. All rights reserved.</span>
                </div>
            </footer>
            <!-- partial -->
        </div>
        <!-- main-panel ends -->
    </div>
@endsection
